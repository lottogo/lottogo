﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LottoGO.Database;
using LottoGO.Database.Entities;
using LottoGO.Database.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LottoGO.AdminPanel
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<LottoGoDbContext>();

			services.AddIdentity<UserEntity, IdentityRole>((o) =>
				{
					o.Password.RequireDigit = false;
					o.Password.RequireLowercase = false;
					o.Password.RequireUppercase = false;
					o.Password.RequireNonAlphanumeric = false;
					o.Password.RequiredLength = 6;
					o.User.RequireUniqueEmail = false;
					o.User.AllowedUserNameCharacters = null;
				})
				.AddEntityFrameworkStores<LottoGoDbContext>()
				.AddDefaultTokenProviders();

			services.AddMvc()
				.AddDataAnnotationsLocalization()
				.AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);
			services.Configure<AppConfig>(Configuration);
			services.AddLocalization(opt => opt.ResourcesPath = "Resources");
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				app.UseHsts();
			}

			app.UseStaticFiles();
			app.UseCookiePolicy();
			app.UseAuthentication();

			var supportedCultures = new[] { new CultureInfo("pl-PL") };
			app.UseRequestLocalization(new RequestLocalizationOptions
			{
				DefaultRequestCulture = new RequestCulture("pl-PL"),
				SupportedCultures = supportedCultures,
				SupportedUICultures = supportedCultures,
				RequestCultureProviders = new List<IRequestCultureProvider>()
			});

			loggerFactory.AddFile("Logs/cards_admin_log.txt");

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Users}/{action=Index}/{id?}");
			});

			using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
			{
				using (var db = serviceScope.ServiceProvider.GetService<LottoGoDbContext>())
				{
					db.Database.Migrate();

					//create admin account if not exists ONLY FOR HACKATON PURPOSE
					var adminAccount = db.Users.FirstOrDefault(u => u.UserName == "admin");
					if (adminAccount == null)
					{
						adminAccount = new UserEntity()
						{
							UserName = "admin",
							Email = "admin@lottogo.pl"
						};

						using (var userManager = serviceScope.ServiceProvider.GetService<UserManager<UserEntity>>())
						{
							userManager.CreateAsync(adminAccount, "lottogo").GetAwaiter().GetResult();

							using (var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>())
							{
								if (!roleManager.RoleExistsAsync("admin").GetAwaiter().GetResult())
								{
									roleManager.CreateAsync(new IdentityRole("admin")).GetAwaiter().GetResult();
								}
							}

							adminAccount = userManager.FindByNameAsync("admin").GetAwaiter().GetResult();
							userManager.AddToRoleAsync(adminAccount, "admin").GetAwaiter().GetResult();
						}
					}
				}
			}
		}
	}
}