﻿using System.Net;
using LottoGO.AdminPanel.Helpers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LottoGO.AdminPanel.Controllers
{
	public class BaseController : Controller
	{
		protected string UserId => HttpContext.User.Identity.Name;

		[ApiExplorerSettings(IgnoreApi = true)]
		protected ContentResult CreateServerResponse(object content, HttpStatusCode statusCode, bool serialize = true)
		{
			return new ContentResult()
			{
				Content = serialize ? LowercaseJsonConverter.Serialize(content) : content.ToString(),
				ContentType = serialize ? "application/json;charset=utf-8" : "text/html; charset=utf-8",
				StatusCode = (int)statusCode
			};
		}
	}
}
