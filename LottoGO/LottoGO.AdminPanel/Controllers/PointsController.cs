﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LottoGO.AdminPanel.Models;
using LottoGO.AdminPanel.Models.DataTables;
using LottoGO.Database;
using LottoGO.Database.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace LottoGO.AdminPanel.Controllers
{
    public class PointsController : BaseController
    {
	    private readonly LottoGoDbContext _context;
	    private readonly ILogger<PointsController> _logger;
	    private readonly IStringLocalizer<PointsController> _stringLocalizer;

	    public PointsController(LottoGoDbContext context, ILogger<PointsController> logger, IStringLocalizer<PointsController> stringLocalizer)
	    {
		    _context = context;
		    _logger = logger;
		    _stringLocalizer = stringLocalizer;
	    }

        public IActionResult Index()
        {
            return View();
        }

	    public async Task<IActionResult> Edit(int mapPointId)
	    {
		    var mapPoint = await  _context.MapPoints.FindAsync(mapPointId);
		    return View("Add", new PointViewModel()
		    {
				ExtraPrize = mapPoint.ExtraPrize,
				GameType = (GameType)mapPoint.GameType,
				StandardPrize = mapPoint.StandardPrize,
				Latitude = mapPoint.Latitude,
				Longitude = mapPoint.Longitude,
				MarkerId = mapPoint.MarkerId,
				Name = mapPoint.Name,
				GameCost = mapPoint.SingleGameCost,
				Id = mapPoint.Id,
				MarkersIds = mapPoint.MultipleMarkers
		    });
	    }

		public async Task<IActionResult> GetPoints(int draw, int start, int length)
	    {
		    var pointsCount = await _context.MapPoints.CountAsync(p=>p.IsActive);
		    var points = await _context.MapPoints.Where(p=>p.IsActive).Skip(start).Take(length).ToListAsync();

		    return CreateServerResponse(new PointsTableModel()
		    {
			    Data = points.Select(u => new PointTableModel()
			    {
				    Id = u.Id,
					Longitude = u.Longitude,
					Name = u.Name,
					Latitude = u.Latitude,
					GameCost = $"{u.SingleGameCost} PLN",
					StandardPrize = $"{u.StandardPrize} PLN",
				    ExtraPrize = $"{u.ExtraPrize} PLN",
					GameType = _stringLocalizer[((GameType)u.GameType).ToString()],
					MarkerId = u.MarkerId
				}).ToList(),
			    Draw = draw,
			    RecordsFiltered = pointsCount,
			    RecordsTotal = pointsCount
		    }, HttpStatusCode.OK);
	    }

		[HttpPost]
	    public async Task<IActionResult> Add(PointViewModel viewModel)
	    {
		    try
		    {
			    if (ModelState.IsValid)
			    {
				    var mapPointEntity = new MapPoints
				    {
					    Latitude = viewModel.Latitude.Value,
					    Longitude = viewModel.Longitude.Value,
					    GameType = (int) viewModel.GameType,
					    SingleGameCost = viewModel.GameCost,
					    MarkerId = viewModel.MarkerId,
					    Name = viewModel.Name,
					    StandardPrize = viewModel.StandardPrize,
						MultipleMarkers = viewModel.MarkersIds,
						IsActive = true
				    };
				    _context.Entry(mapPointEntity).State = EntityState.Added;
				    await _context.SaveChangesAsync();
				    return RedirectToAction("Index");
			    }

			    return View(viewModel);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return View(viewModel);
		    }
	    }

	    [HttpPost]
	    public async Task<IActionResult> Edit(PointViewModel viewModel)
	    {
		    try
		    {
			    if (ModelState.IsValid)
			    {
				    var mapPoint = await _context.MapPoints.FindAsync(viewModel.Id);

				    mapPoint.Latitude = viewModel.Latitude.Value;
				    mapPoint.Longitude = viewModel.Longitude.Value;
				    mapPoint.GameType = (int) viewModel.GameType;
				    mapPoint.SingleGameCost = viewModel.GameCost;
				    mapPoint.MarkerId = viewModel.MarkerId;
				    mapPoint.MultipleMarkers = viewModel.MarkersIds;
				    mapPoint.Name = viewModel.Name;
				    mapPoint.StandardPrize = viewModel.StandardPrize;

				    _context.Entry(mapPoint).State = EntityState.Modified;
				    await _context.SaveChangesAsync();
				    return RedirectToAction("Index");
			    }

			    return View("Add", viewModel);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return View("Add", viewModel);
		    }
	    }


	    [HttpDelete]
	    public async Task<IActionResult> DeletePoint(int mapPointId)
	    {
		    var mapPoint = await _context.MapPoints.FindAsync(mapPointId);
		    mapPoint.IsActive = false;
		    _context.Entry(mapPoint).State = EntityState.Modified;
		    await _context.SaveChangesAsync();

		    return CreateServerResponse(string.Empty, HttpStatusCode.OK);
	    }

		public IActionResult Add()
	    {
		    return View();
	    }
    }
}