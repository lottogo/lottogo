﻿using System.Threading.Tasks;
using LottoGO.AdminPanel.Models;
using LottoGO.Database.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace LottoGO.AdminPanel.Controllers
{
	[Authorize]
	[Route("[controller]/[action]")]
	public class AccountController : BaseController
	{
		private UserManager<UserEntity> _userManager;
		private SignInManager<UserEntity> _signInManager;
		private ILogger<AccountController> _logger;
		private readonly IStringLocalizer<AccountController> _stringLocalizer;

		public AccountController(UserManager<UserEntity> userManager, SignInManager<UserEntity> signInManager, ILogger<AccountController> logger, IStringLocalizer<AccountController> stringLocalizer)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_logger = logger;
			_stringLocalizer = stringLocalizer;
		}

		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> Login(string returnUrl = null)
		{
			// Clear the existing external cookie to ensure a clean login process
			await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

			ViewData["ReturnUrl"] = returnUrl;
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
		{
			ViewData["ReturnUrl"] = returnUrl;
			if (ModelState.IsValid)
			{
				var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, false);
				if (result.Succeeded)
				{
					var user = await _userManager.FindByNameAsync(model.Username);
					var isAdmin = await _userManager.IsInRoleAsync(user, "admin");
					if (isAdmin)
					{
						_logger.LogInformation("User logged in.");
						return RedirectToLocal(returnUrl);
					}
				}

				ModelState.AddModelError(string.Empty, _stringLocalizer.GetString("InvalidLoginOrPassword"));
				return View(model);
			}

			return View(model);
		}

		[HttpGet]
		public async Task<IActionResult> Logout()
		{
			await _signInManager.SignOutAsync();
			_logger.LogInformation("User logged out.");
			return RedirectToAction("Login");
		}


		#region Helpers

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError(string.Empty, error.Description);
			}
		}

		private IActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			else
			{
				return RedirectToAction(nameof(UsersController.Index), "Users");
			}
		}

		#endregion
	}
}
