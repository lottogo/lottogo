﻿using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LottoGO.AdminPanel.Models;
using LottoGO.AdminPanel.Models.DataTables;
using LottoGO.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace LottoGO.AdminPanel.Controllers
{
	[Authorize]
	public class UsersController : BaseController
	{
		private readonly LottoGoDbContext _context;

		public UsersController(LottoGoDbContext context)
		{
			_context = context;
		}

		public IActionResult Index()
		{
			return View();
		}

		public async Task<IActionResult> GetUsers(int draw, int start, int length)
		{
			var usersCount = await _context.Users.CountAsync(u => u.UserName != "admin" && u.IsActive);
			var users = await _context.Users.Where(u => u.UserName != "admin" && u.IsActive).Skip(start).Take(length).ToListAsync();

			return CreateServerResponse(new UsersTableModel
			{
				Data = users.Select(u => new UserModel()
				{
					Id = u.Id,
					AccountBalance = $"{u.AccountBalance} PLN",
					Username = u.UserName,
					Email = u.Email
				}).ToList(),
				Draw = draw,
				RecordsFiltered = usersCount,
				RecordsTotal = usersCount
			}, HttpStatusCode.OK);
		}

		[HttpDelete]
		public async Task<IActionResult> DeleteUser(string userId)
		{
			var user = await _context.Users.FindAsync(userId);
			user.IsActive = false;
			_context.Entry(user).State = EntityState.Modified;
			await _context.SaveChangesAsync();

			return CreateServerResponse(string.Empty, HttpStatusCode.OK);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
