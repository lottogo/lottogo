﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LottoGO.AdminPanel.Models;
using LottoGO.AdminPanel.Models.DataTables;
using LottoGO.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LottoGO.AdminPanel.Controllers
{
	public class BonusesController : BaseController
	{
		private readonly LottoGoDbContext _context;

		public BonusesController(LottoGoDbContext context)
		{
			_context = context;
		}

		public IActionResult Index()
		{
			return View();
		}

		public async Task<IActionResult> Add()
		{
			var availablePoints = await _context.MapPoints.Where(p => p.ExtraPrize == decimal.Zero && p.IsActive).ToListAsync();
			var bonusVm = new BonusViewModel()
			{
				AvailableMapPoints = availablePoints.Select(p => new MapPoint()
				{
					Id = p.Id,
					Name = p.Name
				})
			};

			return View(bonusVm);
		}

		[HttpPost]
		public async Task<IActionResult> Add(BonusViewModel bonusViewModel)
		{
			if (ModelState.IsValid)
			{
				var mapPoint = await _context.MapPoints.FindAsync(bonusViewModel.SelectedMapPoint);
				mapPoint.ExtraPrize = bonusViewModel.ExtraPrize;
				_context.Entry(mapPoint).State = EntityState.Modified;
				await _context.SaveChangesAsync();
				return RedirectToAction("Index");
			}

			return View(bonusViewModel);
		}

		[HttpDelete]
		public async Task<IActionResult> DeleteBonus(int mapPointId)
		{
			var mapPoint = await _context.MapPoints.FindAsync(mapPointId);
			mapPoint.ExtraPrize = 0;
			_context.Entry(mapPoint).State = EntityState.Modified;
			await _context.SaveChangesAsync();

			return CreateServerResponse(string.Empty, HttpStatusCode.OK);
		}

		public async Task<IActionResult> GetBonuses(int draw, int start, int length)
		{
			var pointsCount = await _context.MapPoints.CountAsync(p => p.ExtraPrize != decimal.Zero);
			var points = await _context.MapPoints.Where(p => p.ExtraPrize != decimal.Zero).Skip(start).Take(length).ToListAsync();

			return CreateServerResponse(new BonusesTableModel()
			{
				Data = points.Select(u => new BonusTableModel()
				{
					Id = u.Id,
					Name = u.Name,
					ExtraPrize = $"{u.ExtraPrize} PLN"
				}).ToList(),
				Draw = draw,
				RecordsFiltered = pointsCount,
				RecordsTotal = pointsCount
			}, HttpStatusCode.OK);
		}
	}
}