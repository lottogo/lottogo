﻿using System.ComponentModel.DataAnnotations;

namespace LottoGO.AdminPanel.Models
{
	public class PointViewModel
	{
		public int Id { get; set; }
		[Required (ErrorMessage =  "LatitudeRequired")]
		public double? Latitude { get; set; }
		[Required(ErrorMessage = "LongitudeRequired")]
		public double? Longitude { get; set; }
		[Required(ErrorMessage = "NameRequired")]
		public string Name { get; set; }
		[Required(ErrorMessage = "StandardPrizeRequired")]
		public decimal StandardPrize { get; set; }
		[Required(ErrorMessage = "GameCostRequired")]
		public decimal GameCost { get; set; }
		public decimal ExtraPrize { get; set; }
		[Required(ErrorMessage = "MarkerIdRequired")]
		public string MarkerId { get; set; }
		public string MarkersIds { get; set; }
		public GameType GameType { get; set; }
	}
}
