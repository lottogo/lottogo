﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LottoGO.AdminPanel.Models
{
	public class BonusViewModel
	{
		public int SelectedMapPoint { get; set; }
		public IEnumerable<MapPoint> AvailableMapPoints { get; set; }
		[Required(ErrorMessage = "ExtraPrizeRequired")]
		public decimal ExtraPrize { get; set; }
	}
}
