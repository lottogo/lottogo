﻿using System.Collections.Generic;

namespace LottoGO.AdminPanel.Models.DataTables
{
	public class UsersTableModel
	{
		public int Draw { get; set; }
		public int RecordsTotal { get; set; }
		public int RecordsFiltered { get; set; }
		public IEnumerable<UserModel> Data { get; set; }
	}
}
