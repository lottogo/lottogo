﻿using System.Collections.Generic;

namespace LottoGO.AdminPanel.Models.DataTables
{
	public class PointsTableModel
	{
		public int Draw { get; set; }
		public int RecordsTotal { get; set; }
		public int RecordsFiltered { get; set; }
		public IEnumerable<PointTableModel> Data { get; set; }
	}
}
