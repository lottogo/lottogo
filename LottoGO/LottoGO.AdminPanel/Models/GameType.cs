﻿using System.ComponentModel.DataAnnotations;

namespace LottoGO.AdminPanel.Models
{
	public enum GameType
	{
		[Display(Name = "ThrowADice")]
		ThrowADice,
		[Display(Name = "Geo")]
		Geo
	}
}