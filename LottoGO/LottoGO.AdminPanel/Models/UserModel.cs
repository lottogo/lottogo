﻿namespace LottoGO.AdminPanel.Models
{
	public class UserModel
	{
		public string Id { get; set; }
		public string Username { get; set; }
		public string Email { get; set; }
		public string AccountBalance { get; set; }
	}
}
