﻿namespace LottoGO.AdminPanel.Models
{
	public class MapPoint
	{
		public string Name { get; set; }
		public int Id { get; set; }
	}
}
