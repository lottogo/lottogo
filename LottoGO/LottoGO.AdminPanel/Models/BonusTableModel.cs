﻿namespace LottoGO.AdminPanel.Models
{
	public class BonusTableModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string ExtraPrize { get; set; }
	}
}