﻿using System.ComponentModel.DataAnnotations;

namespace LottoGO.AdminPanel.Models
{
	public class LoginViewModel
	{
		[Required(AllowEmptyStrings = false, ErrorMessage = "UsernameEmpty")]
		public string Username { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "PasswordEmpty")]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		public bool RememberMe { get; set; }
	}
}