﻿namespace LottoGO.AdminPanel.Models
{
	public class PointTableModel
	{
		public int Id { get; set; }
		public double? Latitude { get; set; }
		public double? Longitude { get; set; }
		public string Name { get; set; }
		public string StandardPrize { get; set; }
		public string GameCost { get; set; }
		public string ExtraPrize { get; set; }
		public string MarkerId { get; set; }
		public string GameType { get; set; }
	}
}
