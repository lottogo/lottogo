using Autofac;
using LottoGO.App.Services.Contracts;
using LottoGO.App.Services.Contracts.Contracts;
using LottoGO.Services;

namespace LottoGO.Configuration
{
	internal class PlatformSpecificDependenciesModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			
			builder.RegisterType<iOSGoToLocationService>().As<IGoToLocationService>();
			builder.RegisterType<iOSLocalFilePathObtainerService>().As<ILocalFilePathObtainerService>();
		}
	}
}