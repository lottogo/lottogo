﻿using Autofac;
using LottoGO.App.Configuration;

namespace LottoGO.Configuration
{
    public static class Bootstrapper
    {
        public static IContainer Init()
        {
	        CoreBootstrapper.Init();
	        var builder = new iOSContainerDependencyInjectionConfig().RegisterModules();
	        return builder.Build();
        }
    }
}