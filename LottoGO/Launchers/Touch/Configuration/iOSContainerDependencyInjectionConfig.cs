﻿using System.Collections.Generic;
using Autofac;
using LottoGO.App.Configuration.Ioc;

namespace LottoGO.Configuration
{
	internal class iOSContainerDependencyInjectionConfig : ContainerDependencyModulesProvider
	{
		protected override IEnumerable<Module> GetPlatformSpecificModules()
		{
			yield return new PlatformSpecificDependenciesModule();
		}
	}
}