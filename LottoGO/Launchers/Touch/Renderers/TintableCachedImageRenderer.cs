﻿using System.ComponentModel;
using FFImageLoading.Forms;
using FFImageLoading.Forms.Platform;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TintableCachedImage), typeof(TintableCachedImageRenderer))]
namespace LottoGO.Renderers
{
	public class TintableCachedImageRenderer : CachedImageRenderer
	{
		public TintableCachedImage View => Element as TintableCachedImage;

		protected override void OnElementChanged(ElementChangedEventArgs<CachedImage> e)
		{
			base.OnElementChanged(e);
			TintImage();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == TintableCachedImage.TintColorProperty.PropertyName)
			{
				TintImage();
			}
		}

		private void TintImage()
		{
			if (View == null) return;
			if (View.TintColor != Color.Default)
			{
				if (Control.Image != null)
				{
					Control.Image = Control.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
					Control.TintColor = View.TintColor.ToUIColor();
				}
			}
		}
	}
}