﻿using LottoGO.App.Models;
using LottoGO.App.Views.Base;
using LottoGO.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (OrientationNavigationPage), typeof (UINavigationPageRenderer))]
namespace LottoGO.Renderers
{
	public class UINavigationPageRenderer : NavigationRenderer
	{
		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
		{
			var orientationNaviPage = Element as OrientationNavigationPage;
			switch (orientationNaviPage.Orientation)
			{
				case Orientation.Portrait:
					return UIInterfaceOrientationMask.Portrait;
				case Orientation.Landscape:
					return UIInterfaceOrientationMask.Landscape;
				case Orientation.All:
					return UIInterfaceOrientationMask.AllButUpsideDown;
			}

			return UIInterfaceOrientationMask.AllButUpsideDown;
		}

		public override UIInterfaceOrientation PreferredInterfaceOrientationForPresentation()
		{
			var orientationNaviPage = Element as OrientationNavigationPage;
			switch (orientationNaviPage.Orientation)
			{
				case Orientation.Portrait:
					return UIInterfaceOrientation.Portrait;
				case Orientation.Landscape:
					return UIInterfaceOrientation.LandscapeLeft;
				case Orientation.All:
					return UIInterfaceOrientation.Portrait;
			}

			return UIInterfaceOrientation.Portrait;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			
			var navctrl = this.ViewController.NavigationController;
			this.NavigationBarHidden = true;
			this.InteractivePopGestureRecognizer.Delegate = new UIGestureRecognizerDelegate();
			this.InteractivePopGestureRecognizer.Enabled = true;
		}
	}
}