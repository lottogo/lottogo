﻿using System.ComponentModel;
using CoreAnimation;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using DatePickerRenderer = LottoGO.Renderers.DatePickerRenderer;

[assembly: ExportRenderer(typeof(DatePicker), typeof(DatePickerRenderer))]
namespace LottoGO.Renderers
{
	public class DatePickerRenderer : Xamarin.Forms.Platform.iOS.DatePickerRenderer
	{
		private bool _styled;
		private CALayer _underlineLayer;

		protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
		{
			base.OnElementChanged(e);
			if (Element != null)
			{
				StyleEntry();
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (_underlineLayer != null)
				_underlineLayer.Frame = new CGRect(0, Control.Frame.Height + 2, Control.Frame.Width, 1);
		}

		private void StyleEntry()
		{
			if (_styled) return;
			Control.BorderStyle = UITextBorderStyle.None;
			_underlineLayer = new CALayer
			{
				Frame = new CGRect(0, Control.Frame.Height + 2, Control.Frame.Width, 1),
				BackgroundColor = UIColor.Black.CGColor
			};
			Control.Layer.AddSublayer(_underlineLayer);
			_styled = true;
		}
	}
}