﻿using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace LottoGO.Renderers
{
	public class CustomButtonRenderer : ButtonRenderer
	{
		public CustomButtonRenderer()
		{

		}

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
				Control.TitleLabel.Lines = 1;
				Control.TitleLabel.TextAlignment = UITextAlignment.Left;
			}
		}
	}
}