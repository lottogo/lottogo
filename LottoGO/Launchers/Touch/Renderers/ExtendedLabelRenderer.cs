﻿using System.ComponentModel;
using Foundation;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (ExtendedLabel), typeof (ExtendedLabelRenderer))]
namespace LottoGO.Renderers
{
    public class ExtendedLabelRenderer : LabelRenderer
    {
	    private ExtendedLabel ExtendedLabelElement => Element as ExtendedLabel;

	    protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
	    {
		    base.OnElementChanged(e);
			if (e.NewElement == null) return;
			SetLabelHtmlText();
		    SetLabelLinesCount();
	    }

	    private void SetLabelLinesCount()
	    {
		    if (ExtendedLabelElement.LinesCount != -1)
		    {
			    Control.Lines = ExtendedLabelElement.LinesCount;
		    }
	    }

	    protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
		    base.OnElementPropertyChanged(sender, e);
		    if (e.PropertyName == Label.TextProperty.PropertyName)
		    {
			    SetLabelHtmlText();
		    }

		    if (e.PropertyName == ExtendedLabel.LinesCountProperty.PropertyName)
		    {
				SetLabelLinesCount();
		    }
	    }

	    private void SetLabelHtmlText()
		{
			if (ExtendedLabelElement.ShouldRenderAsHtml)
			{
				if (string.IsNullOrEmpty(ExtendedLabelElement.Text))
				{
					Control.Text = ExtendedLabelElement.Text;
					return;
				}
				NSError error = null;
				var attributedString = new NSAttributedString(NSData.FromString(ExtendedLabelElement.Text),
					new NSAttributedStringDocumentAttributes { DocumentType = NSDocumentType.HTML, StringEncoding = NSStringEncoding.UTF8 },
					ref error);
				var mutable = new NSMutableAttributedString();
				mutable.Append(attributedString);
				SetBaseFont(mutable);
				Control.AttributedText = mutable;
			}
		}

		private void SetBaseFont(NSMutableAttributedString attributedString)
		{
			//var mediumFont = UIFont.FromName(AppDelegate.mediumFontName, Control.Font.PointSize);
			//var regularFont = UIFont.FromName(AppDelegate.regularFontName, Control.Font.PointSize);
			//var baseDescriptor = regularFont.FontDescriptor;

			//attributedString.EnumerateAttributes(new NSRange(0, attributedString.Length), NSAttributedStringEnumeration.None,
				//(NSDictionary attrs, NSRange range, ref bool stop) =>
				//{
				//	foreach (var attr in attrs)
				//	{
				//		var font = attr.Value as UIFont;
				//		if (font == null) continue;
				//		var traits = font.FontDescriptor.SymbolicTraits;
				//		var descriptor = baseDescriptor.CreateWithTraits(traits);
				//		var newFont = UIFont.FromDescriptor(descriptor, Control.Font.PointSize);

				//		attributedString.RemoveAttribute(UIStringAttributeKey.Font, range);
				//		attributedString.RemoveAttribute(UIStringAttributeKey.ForegroundColor, range);
				//		attributedString.AddAttribute(UIStringAttributeKey.Font, newFont, range);
				//		attributedString.AddAttribute(UIStringAttributeKey.ForegroundColor, Element.TextColor.ToUIColor(), range);
				//	}
				//});
		}
    }
}