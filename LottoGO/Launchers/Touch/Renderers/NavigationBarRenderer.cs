﻿using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;

[assembly: ExportRenderer (typeof (NavigationBarView), typeof (NavigationBarRenderer))]
namespace LottoGO.Renderers
{
	public class NavigationBarRenderer : BaseVisualElementRenderer<ContentView>
	{
	}
}