﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using LottoGO.App.Views;
using LottoGO.Renderers;
using LottoGO.WaveWrapper;
using UIKit;
using WaveEngine.Adapter;
using WaveEngine.Adapter.Extensions;
using WaveEngine.Common;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

[assembly: ExportRenderer(typeof(GamePage), typeof(GamePageRenderer))]
namespace LottoGO.Renderers
{
    public class GamePageRenderer : PageRenderer, IApplication
	{
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
        }

        public IGame CurrentGame;
        public bool GameInitialized;

        public void Initialize()
        {
            if (!GameInitialized)
            {
                CurrentGame?.Initialize(this);
                GameInitialized = true;
            }
        }

        public void Initialize(IGame game)
        {
            if (game != null)
            {
                CurrentGame = game;
                GameInitialized = false;
            }
        }

		public void Update(TimeSpan elapsedTime)
		{
            if (!GameInitialized)
            {
                CurrentGame?.Initialize(this);
                GameInitialized = true;
            }
            CurrentGame?.UpdateFrame(elapsedTime);
		}

		public void Draw(TimeSpan elapsedTime)
		{
			CurrentGame?.DrawFrame(elapsedTime);
		}

		public void Exit()
		{
			//throw new NotImplementedException();
		}

		private const string OldTemplateCompatibilityMessage = "The WaveEngine iOS template has changed. To continue using the old style template, you must change your Application class to extend from WaveEngine.Adapter.WaveApplicationDelegate instead of WaveEngine.Adapter.Application. Otherwise you can create a new iOS launcher project using WaveEngine Visual Editor and migrate your code to use the iOS template.";
		/// <summary>The window</summary>
		protected UIWindow window;
		/// <summary>The GLView view</summary>
        internal GLView _glView;

		/// <summary>
		/// Gets the parent adapter needed to initialize the application.
		/// </summary>
		public IAdapter Adapter => this._glView.Adapter;

		/// <summary>
		/// Gets the title of the application window, when available.
		/// </summary>
		public string WindowTitle => string.Empty;

		/// <summary>
		/// Gets or sets a value indicating whether the window is in full screen.
		/// </summary>
		/// <value>
		///   <c>true</c> if in full screen; otherwise, <c>false</c>.
		/// </value>
		public bool FullScreen
		{
			get => UIApplication.SharedApplication.StatusBarHidden;
			set => UIApplication.SharedApplication.SetStatusBarHidden(value, false);
		}

		/// <summary>Gets the width, in pixels, of the window/backbuffer.</summary>
		public int Width => this.Adapter.Width;

		/// <summary>Gets the height, in pixels, of the window/backbuffer.</summary>
		public int Height => this.Adapter.Height;

		/// <summary>
		/// Gets a value indicating whether this application is running in Visual Editor
		/// </summary>
		public virtual bool IsEditor => false;

		/// <summary>Gets the execution mode of the application</summary>
		public virtual ExecutionMode ExecutionMode => WaveEngine.Common.ExecutionMode.Standalone;

		/// <summary>ShouldAutorotateToInterfaceOrientation</summary>
		/// <param name="toInterfaceOrientation">toInterfaceOrientation</param>
		/// <returns>bool</returns>
		[Obsolete]
		public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}

		/// <summary>ViewDidLoad</summary>
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			this._glView = this.FindFirstGLView();
            (this._glView as OurGLView).SetApplication(new WaveAppWrapper(this));
//            Initialize((this._glView as OurGLView).Game);
            //this._glView.Application = new WaveAppWrapper(this);
            //this._glView.Adapter = //new Adapter(0, 0, _glView);
           // _glView.contex
		}

		private GLView FindFirstGLView()
		{
			GLView firstChild = this.NativeView.FindFirstChild<GLView>();
			if (firstChild == null)
				throw new Exception("Could not find a GLView in WaveEngine.Adapter.Application view hierarchy");
			return firstChild;
		}

		/// <summary>ViewDidAppear</summary>
		/// <param name="animated">animated</param>
		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);
			this._glView.Activate();
		}

		/// <summary>ViewWillDisappear</summary>
		/// <param name="animated">animated</param>
		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			this._glView.Deactivate();
		}


		/// <summary>Dispose</summary>
		/// <param name="disposing">disposing</param>
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			NSNotificationCenter.DefaultCenter.RemoveObserver((NSObject)this);
		}
	}
}