﻿using System.ComponentModel;
using CoreGraphics;
using LottoGO.App.Views.Base;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace LottoGO.Renderers
{
	public abstract class BaseVisualElementRenderer<TElement> : VisualElementRenderer<TElement>
		where TElement : VisualElement
	{
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (Element is IShadowElement shadowElement)
			{
				if (shadowElement.HasShadow)
				{
					Layer.MasksToBounds = false;
					Layer.ShadowRadius = 3;
					Layer.ShadowOffset = new CGSize(0, 2);
					Layer.ShadowOpacity = 1;
					Layer.ShadowColor = UIColor.Black.ColorWithAlpha(0.2f).CGColor;
				}
				else
				{
					Layer.ShadowRadius = 0;
					Layer.ShadowOffset = new CGSize(0, 0);
					Layer.ShadowOpacity = 0;
					Layer.ShadowColor = UIColor.Clear.CGColor;
				}
			}
		}
	}
}

