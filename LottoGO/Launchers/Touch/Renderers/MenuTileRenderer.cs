﻿using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;

[assembly: ExportRenderer (typeof (MenuTile), typeof (MenuTileRenderer))]
namespace LottoGO.Renderers
{
	public class MenuTileRenderer : BaseVisualElementRenderer<ContentView>
	{
	}
}