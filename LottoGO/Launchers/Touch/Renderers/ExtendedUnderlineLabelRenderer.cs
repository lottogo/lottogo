﻿using System.ComponentModel;
using Foundation;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (ExtendedUnderlineLabel), typeof (ExtendedUnderlineLabelRenderer))]
namespace LottoGO.Renderers
{
    public class ExtendedUnderlineLabelRenderer : LabelRenderer
    {
	    private ExtendedUnderlineLabel ExtendedLabelElement => Element as ExtendedUnderlineLabel;

	    protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
	    {
		    base.OnElementChanged(e);
			if (e.NewElement == null) return;
		    SetTextUnderline();
	    }


	    protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
		    base.OnElementPropertyChanged(sender, e);
		    
		    if (e.PropertyName == ExtendedUnderlineLabel.IsUnderlinedProperty.PropertyName)
		    {
			    SetTextUnderline();
		    }
	    }

	    private void SetTextUnderline()
	    {
			if (string.IsNullOrEmpty(ExtendedLabelElement.Text))
			{
				Control.Text = ExtendedLabelElement.Text;
				return;
			}

			var colorToUse = ExtendedLabelElement.IsUnderlined ? UIColor.Red : UIColor.LightGray;
			var attributedString = new NSMutableAttributedString(ExtendedLabelElement.Text, strokeColor: colorToUse, underlineStyle: NSUnderlineStyle.Single);
			attributedString.AddAttribute(UIStringAttributeKey.UnderlineColor, colorToUse, new NSRange(0, ExtendedLabelElement.Text.Length));
			Control.AttributedText = attributedString;
	    }
    }
}