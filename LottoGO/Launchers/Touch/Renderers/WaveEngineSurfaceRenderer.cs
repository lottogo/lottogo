﻿using System;
using System.ComponentModel;
using CoreGraphics;
using LottoGO.App.Controls;
using LottoGO.Renderers;
using LottoGO.WaveWrapper;
using OpenTK;
using WaveEngine.Adapter;
using WaveEngine.Common;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(WaveEngineSurface), typeof(WaveEngineSurfaceRenderer))]
namespace LottoGO.Renderers
{
	public class WaveEngineSurfaceRenderer : ViewRenderer<WaveEngineSurface, OurGLView>
	{
        private OurGLView gameView;

		protected override void OnElementChanged(ElementChangedEventArgs<WaveEngineSurface> e)

        {
			base.OnElementChanged(e);

			if (e.OldElement != null) gameView = null;

			if (e.NewElement != null)
			{
                gameView = new OurGLView(new CGRect(0,0,Element.Width,Element.Height));
				SetNativeControl(gameView);
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName.Equals(WaveEngineSurface.GameProperty.PropertyName,
				StringComparison.CurrentCultureIgnoreCase)) InitGame();
		}

		private void InitGame()
		{
            (gameView as OurGLView).Game = Element.Game;
            //gameView.I?.Initialize(Element.Game);
		}
	}
}