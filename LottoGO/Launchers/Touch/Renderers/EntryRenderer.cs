﻿using System.ComponentModel;
using CoreAnimation;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using EntryRenderer = LottoGO.Renderers.EntryRenderer;

[assembly: ExportRenderer(typeof(Entry), typeof(EntryRenderer))]
namespace LottoGO.Renderers
{
	public class EntryRenderer : Xamarin.Forms.Platform.iOS.EntryRenderer
	{
		private bool _styled;
		private CALayer _underlineLayer;

		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
			if (Element != null)
			{
				Control.BorderStyle = UITextBorderStyle.None;
				//StyleEntry();
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (_underlineLayer != null)
				_underlineLayer.Frame = new CGRect(0, Element.Height + 2, Element.Width, 1);
		}

		private void StyleEntry()
		{
			if (_styled) return;
			Control.BorderStyle = UITextBorderStyle.None;
			_underlineLayer = new CALayer
			{
				Frame = new CGRect(0, Control.Frame.Height + 2, Control.Frame.Width, 1),
				BackgroundColor = UIColor.Black.CGColor
			};
			Control.Layer.AddSublayer(_underlineLayer);
			_styled = true;
		}
	}
}