﻿using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(ExtendedStackLayout), typeof(ExtendedStackLayoutRenderer))]
namespace LottoGO.Renderers
{
	public class ExtendedStackLayoutRenderer : BaseVisualElementRenderer<ExtendedStackLayout>
	{

	}
}