﻿using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomScrollView), typeof(CustomScrollViewRenderer))]
namespace LottoGO.Renderers
{
	public class CustomScrollViewRenderer : ScrollViewRenderer
	{
		public CustomScrollViewRenderer()
		{

		}

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);
			Bounces = false;
		}
	}
}