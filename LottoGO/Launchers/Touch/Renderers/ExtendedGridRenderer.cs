﻿using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(ExtendedGrid), typeof(ExtendedGridRenderer))]
namespace LottoGO.Renderers
{
	public class ExtendedGridRenderer : BaseVisualElementRenderer<ExtendedGrid>
	{
	}
}