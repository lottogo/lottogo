﻿using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (BorderlessFrame), typeof (BorderlessFrameRenderer))]
namespace LottoGO.Renderers
{
	public class BorderlessFrameRenderer : BaseVisualElementRenderer<ContentView>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ContentView> e)
		{
			base.OnElementChanged(e);
			this.BackgroundColor = UIColor.Clear;
		}
	}
}