﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using LottoGO.App.Services;
using LottoGO.App.Views;
using LottoGO.Common.Models;
using LottoGO.Renderers;
using MapKit;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(LottoMap), typeof(LottoMapRenderer))]
namespace LottoGO.Renderers
{
    public class LottoMapRenderer : MapRenderer
    {
        private LottoMap Map => Element as LottoMap;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null) {
                var nativeMap = Control as MKMapView;
                if (nativeMap != null) {
                    nativeMap.RemoveAnnotations(nativeMap.Annotations);
                    nativeMap.GetViewForAnnotation = null;
//                    nativeMap.CalloutAccessoryControlTapped -= OnCalloutAccessoryControlTapped;
                    nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
//                    nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
                }
            }

            if (e.NewElement != null) {
                var formsMap = (LottoMap)e.NewElement;
                var nativeMap = Control as MKMapView;

                nativeMap.GetViewForAnnotation = GetViewForAnnotation;
//                nativeMap.CalloutAccessoryControlTapped += OnCalloutAccessoryControlTapped;
                nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
//                nativeMap.DidDeselectAnnotationView += OnDidDeselectAnnotationView;
            }
        }
        
        protected override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            if (annotation is MKUserLocation || annotation == null)
                return null;

            var pointAnnotation = annotation as MKPointAnnotation;

            if (pointAnnotation == null) return null;

            LottoGoPin customPin = GetCustomPin(pointAnnotation);
            if (customPin == null) 
            {
                return null;
                //throw new Exception("Custom pin not found");
            }

            annotationView = mapView.DequeueReusableAnnotation(customPin.Id.ToString());
            if (annotationView == null) {
                annotationView = new LottoAnnotationView(annotation, customPin.Id.ToString());
                annotationView.Image = GetImageFromGameType(customPin.GameType);
                annotationView.CalloutOffset = new CGPoint(0, 0);
                annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromBundle("ico_hamurger"));
                ((LottoAnnotationView)annotationView).Id = customPin.Id.ToString();
//                ((LottoAnnotationView)annotationView).Url = customPin.Url;
            }
            
            annotationView.CanShowCallout = false;

            return annotationView;
        }

        private UIImage GetImageFromGameType(GameType customPinGameType)
        {
            var ico = string.Empty;
            switch (customPinGameType)
            {
                case GameType.Scratch:
                    ico = "dice";
                    break;
                case GameType.SlotMachine:
                    ico = "slot";
                    break;
                case GameType.TableChoice:
                    ico = "ticket";
                    break;
                case GameType.Geo:
                    ico = "choose";
                    break;
            }
            
            return UIImage.FromBundle(ico);
        }

        private LottoGoPin GetCustomPin(MKPointAnnotation mkPointAnnotation)
        {
            return ((IList<LottoGoPin>)Map?.ItemsSource)?.FirstOrDefault(p => p.Position.Latitude == mkPointAnnotation.Coordinate.Latitude && p.Position.Longitude == mkPointAnnotation.Coordinate.Longitude);
        }

        void OnDidSelectAnnotationView (object sender, MKAnnotationViewEventArgs e)
        {
            var pin = ((IList<LottoGoPin>)Map?.ItemsSource)?.FirstOrDefault(p => p.Position.Latitude == e.View.Annotation.Coordinate.Latitude && p.Position.Longitude == e.View.Annotation.Coordinate.Longitude);
            Map?.PinTappedCommand.Execute(pin);
        }
    }

    public class LottoAnnotationView : MKAnnotationView
    {
        public LottoAnnotationView(IMKAnnotation annotation, string reuseIdentifier) : base(annotation, reuseIdentifier)
        {
            
        }

        public string Id { get; set; }
    }
}