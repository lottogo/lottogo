using Autofac;
using CoreGraphics;
using LottoGo.iOS.Messenger.Messages;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Views.CustomViews;
using Xamarin.Forms;

namespace LottoGO
{
    using Foundation;
    using UIKit;

    [Register("AppDelegate")]
    public class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		private const string AppCenterId = "8a60d002-4851-4906-9932-90f29ef92dcb";
		private IContainer _container;

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			_container = Configuration.Bootstrapper.Init();

			FFImageLoading.Forms.Platform.CachedImageRenderer.Init();
			
			Xamarin.FormsMaps.Init();

			//AppCenter.Start(AppCenterId, typeof(Crashes));
			
			global::Xamarin.Forms.Forms.Init();
			LoadApplication(new App.App(_container));

            IncludeAssemblies();

			return base.FinishedLaunching(app, options);
		}

        private static void IncludeAssemblies()
        {
            // Include WaveEngine Components
            var a1 = typeof(WaveEngine.Components.Cameras.FreeCamera3D);
        }

		/*public override void WillChangeStatusBarFrame(UIApplication application, CGRect newStatusBarFrame)
		{
			HasAdditionalStatusBar = newStatusBarFrame.Height == 40;
			var messenger = _container?.Resolve<IMvxMessenger>();
			messenger?.Publish(new StatusBarRectChangedMessage(this));
		}*/

		public static bool HasAdditionalStatusBar { get; private set; }


	}
}
