﻿using System.IO;
using Foundation;
using LottoGO.App.Services.Contracts.Contracts;

namespace LottoGO.Services
{
	public class iOSLocalFilePathObtainerService : ILocalFilePathObtainerService
	{
		public string ObtainLocalFilePath(string fileName)
		{
			if (fileName.Contains("http")) return fileName;
			var videoPath = Path.Combine(NSBundle.MainBundle.BundlePath, $"Video/{fileName}");
			var videoUrl = new NSUrl(videoPath, false);
			return videoUrl.AbsoluteString;
		}
	}
}