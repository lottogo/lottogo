﻿using System;
using Foundation;
using LottoGO.App.Services.Contracts;
using UIKit;

namespace LottoGO.Services
{
	public class iOSGoToLocationService : IGoToLocationService
	{
			private const string GoogleMapsName = "Google Maps";
			private const string WazeName = "Waze";
			private const string AppleMapsName = "Apple Maps";
			private const string AppleMapsPrefix = "http://maps.apple.com";
			private const string AppleMapsUrl = AppleMapsPrefix + "/?q={0}";
			private const string GoogleMapsPrefix = "comgooglemaps://";
			private const string GoogleMapsUrl = "comgooglemaps://?center={1}&q={0}";
			private const string WazePrefix = "https://waze.com/ul";
			private const string WazeUrl = "https://waze.com/ul?ll={1}&q={0}";

			public async void GoToLocation(string address, string latlng)
			{
				try
				{
					var addGoogleMaps = false;
					var addWaze = false;

					if (UIApplication.SharedApplication.CanOpenUrl(NSUrl.FromString(GoogleMapsPrefix)))
					{
						addGoogleMaps = true;
					}

					if (UIApplication.SharedApplication.CanOpenUrl(NSUrl.FromString("waze://")))
					{
						addWaze = true;
					}

					var url = string.Empty;

					if (addGoogleMaps || addWaze)
					{
						var choice = string.Empty;

						if (addGoogleMaps && addWaze)
						{
							choice = await Xamarin.Forms.Application.Current.MainPage.DisplayActionSheet("Select app", "Cancel", null,
								AppleMapsName, GoogleMapsName, WazeName);
						}
						else if (addGoogleMaps)
						{
							choice = await Xamarin.Forms.Application.Current.MainPage.DisplayActionSheet("Select app", "Cancel", null,
								AppleMapsName, GoogleMapsName);
						}
						else if (addWaze)
						{
							choice = await Xamarin.Forms.Application.Current.MainPage.DisplayActionSheet("Select app", "Cancel", null,
								AppleMapsName, WazeName);
						}

						switch (choice)
						{
							case GoogleMapsName:
								url = GoogleMapsUrl;
								break;
							case WazeName:
								url = WazeUrl;
								break;
							case AppleMapsName:
								url = AppleMapsUrl;
								break;
						}
					}
					else
					{
						url = AppleMapsUrl;
					}

					var appUrl = string.Format(url, address, latlng);
					var nsappUrl = new NSString(appUrl).CreateStringByAddingPercentEscapes(NSStringEncoding.UTF8);
					var nsurl = NSUrl.FromString(nsappUrl);
					UIApplication.SharedApplication.OpenUrl(nsurl);
				}
				catch (Exception e)
				{
					await Xamarin.Forms.Application.Current.MainPage.DisplayAlert("Ups, something went wrong", string.Empty, "OK");
				}
			}
		}
}