﻿using System;
using LottoGO.Effects;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("LottoGo")]
[assembly: ExportEffect(typeof(ScrollBarsInvisibleEffect), "ScrollBarsInvisibleEffect")]
namespace LottoGO.Effects
{
	public class ScrollBarsInvisibleEffect : PlatformEffect
	{
		protected override void OnAttached()
		{
			try
			{
				SetScrollBarsInvisible();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Cannot set property on attached control. Error: ", ex.Message);
			}
		}

		protected override void OnDetached()
		{
		}

		private void SetScrollBarsInvisible()
		{
			if (Control is UIScrollView scrollView)
			{
				scrollView.ShowsHorizontalScrollIndicator = false;
				scrollView.ShowsVerticalScrollIndicator = false;
			}
			else if (Control is UITableView listView)
			{
				listView.ShowsHorizontalScrollIndicator = false;
				listView.ShowsVerticalScrollIndicator = false;
			}
		}

		protected override void OnElementPropertyChanged(System.ComponentModel.PropertyChangedEventArgs args)
		{
			base.OnElementPropertyChanged(args);
			try
			{
				SetScrollBarsInvisible();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Cannot set property on attached control. Error: ", ex.Message);
			}
		}
	}
}