﻿using LottoGO.App.Messenger.Base;

namespace LottoGo.iOS.Messenger.Messages
{
	public class StatusBarRectChangedMessage : MvxMessage
	{
		public StatusBarRectChangedMessage(object sender) : base(sender)
		{
		}
	}
}