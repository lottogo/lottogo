﻿using System;
using System.Reflection;
using CoreGraphics;
using Foundation;
using LottoGO.Renderers;
using OpenTK;
using WaveEngine.Adapter;
using WaveEngine.Common;

namespace LottoGO.WaveWrapper
{
    public class OurGLView : GLView
    {
        private FieldInfo _paused;
        private FieldInfo _application;
        public IGame Game {  set{
                ((GamePageRenderer)_app.InnerApplication).Initialize(value);
            } }
        private WaveAppWrapper _app;

        public OurGLView(NSCoder coder) : base(coder)
        {
            InitFields();
        }

        public OurGLView(CGRect rect) : base(rect)
        {
            InitFields();
        }

        public void InitFields()
        {
            try
            {
                _paused = this.GetType().GetField("isPaused", BindingFlags.NonPublic | BindingFlags.Instance);
                _application = this.GetType().GetField("Application", BindingFlags.NonPublic | BindingFlags.Instance);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        protected bool GetPaused()
        {
            try
            {
                return (bool)_paused?.GetValue(this);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void SetApplication(WaveAppWrapper app)
        {
            try
            {
                _app = app;
                _application?.SetValue(this, app);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        public WaveAppWrapper GetAppWrapper(){
            try{
                return (WaveAppWrapper)_application.GetValue(this);
            }catch(Exception e){
                System.Diagnostics.Debug.WriteLine(e.Message);
                return null;
            }
        }
    }
}
