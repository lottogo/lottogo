﻿using System;
using LottoGO.Renderers;
using WaveEngine.Common;

namespace LottoGO.WaveWrapper
{
	public class WaveAppWrapper : WaveEngine.Adapter.Application
	{
		private readonly IApplication _app;
        public IApplication InnerApplication => _app;
        public IGame Game { set{
                ((GamePageRenderer)_app)?.Initialize(value);
            }}

		public WaveAppWrapper(IApplication app)
		{
			_app = app;
		}

		public override void Initialize()
		{
			//throw new NotImplementedException();
		}

		public override void Update(TimeSpan elapsedTime)
		{
			_app?.Update(elapsedTime);
		}

		public override void Draw(TimeSpan elapsedTime)
		{
			_app?.Draw(elapsedTime);
		}
	}
}