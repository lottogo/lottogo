﻿using System;
using LottoGO.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ScrollView = Android.Widget.ScrollView;
using ListView = Android.Widget.ListView;

[assembly: ResolutionGroupName("LottoGO")]
[assembly: ExportEffect(typeof(ScrollBarsInvisibleEffect), "ScrollBarsInvisibleEffect")]
namespace LottoGO.Effects
{
	public class ScrollBarsInvisibleEffect : PlatformEffect
	{
		protected override void OnAttached()
		{
			try
			{
				SetScrollBarsInvisible();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Cannot set property on attached control. Error: ", ex.Message);
			}
		}

		protected override void OnDetached()
		{
		}

		private void SetScrollBarsInvisible()
		{
			if (Control is ScrollView scrollView)
			{
				scrollView.HorizontalScrollBarEnabled = false;
				scrollView.VerticalScrollBarEnabled = false;
			}
			else if (Control is ListView listView)
			{
				listView.HorizontalScrollBarEnabled = false;
				listView.VerticalScrollBarEnabled = false;
			}
		}

		protected override void OnElementPropertyChanged(System.ComponentModel.PropertyChangedEventArgs args)
		{
			base.OnElementPropertyChanged(args);
			try
			{
				SetScrollBarsInvisible();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Cannot set property on attached control. Error: ", ex.Message);
			}
		}
	}
}