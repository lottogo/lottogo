﻿using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Autofac;
using FFImageLoading.Forms.Platform;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Views.CustomViews;
using LottoGO.Configuration;
using LottoGO.WaveWrapper;
using Plugin.CurrentActivity;
using Plugin.Permissions;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Permission = Plugin.Permissions.Abstractions.Permission;

namespace LottoGO
{
	[Activity(Label = "LottoGo",
		Icon = "@drawable/ic_launcher_icon",
		Theme = "@style/MainTheme",
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
		ScreenOrientation = ScreenOrientation.Portrait,
		MainLauncher = true)]
	public class MainActivity : WaveAppWrapper
	{
		private const string AppCenterId = "4bc34746-b631-4a06-b6ef-52ab74b55c74";

		private bool _fontsLoaded;
		private OrientationChangeRequestObserver _orientationObserver;

		protected override async void OnCreate(Bundle bundle)
		{
			//RequestedOrientation = ScreenOrientation.Portrait;
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			var container = Bootstrapper.Init();

			Xamarin.FormsMaps.Init(this, bundle);
			CachedImageRenderer.Init(true);
			CrossCurrentActivity.Current.Init(this, bundle);
			//AppCenter.Start(AppCenterId, typeof(Crashes));

			Forms.ViewInitialized += FormsOnViewInitialized;
			SetupObservers(container);

			Forms.Init(this, bundle);
			LoadApplication(new App.App(container));

			await RequestCamera();
		}


		private static async Task RequestCamera()
		{
			var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
		}

		private void SetupObservers(IContainer container)
		{
			var messenger = container.Resolve<IMvxMessenger>();
			_orientationObserver = new OrientationChangeRequestObserver(this);
			_orientationObserver.Start(messenger);
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions,[GeneratedEnum] Android.Content.PM.Permission[] grantResults)
		{
			PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}

		public override void OnBackPressed()
		{
			if (PopupContainer.VisiblePopup != null)
			{
				PopupContainer.VisiblePopup.ClosePopupCommand.Execute(null);
				return;
			}

			base.OnBackPressed();
		}


		private void FormsOnViewInitialized(object sender, ViewInitializedEventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(e.View.StyleId))
				e.NativeView.ContentDescription = e.View.StyleId;
		}
	}
}