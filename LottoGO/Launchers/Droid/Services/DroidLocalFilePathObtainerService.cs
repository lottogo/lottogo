﻿using LottoGO.App.Services.Contracts.Contracts;
using Plugin.CurrentActivity;

namespace LottoGO.Services
{
	public class DroidLocalFilePathObtainerService : ILocalFilePathObtainerService
	{
		public string ObtainLocalFilePath(string fileName)
		{
			var rawName = fileName.Contains(".") ? fileName.Split('.')[0] : fileName;
			var resources = CrossCurrentActivity.Current.Activity.Resources;
			var identifier = resources.GetIdentifier(rawName, "raw", CrossCurrentActivity.Current.Activity.PackageName);
			if (identifier == 0) return fileName;
			var videoUrl = "android.resource://" + CrossCurrentActivity.Current.Activity.PackageName + "/" + identifier;
			return videoUrl;
		}
	}
}