﻿using System.Net;
using Android.Content;
using Android.Net;
using LottoGO.App.Services.Contracts;
using Plugin.CurrentActivity;

namespace LottoGO.Services
{
	internal class DroidGoToLocationService : IGoToLocationService
	{
		public void GoToLocation(string address, string latlng)
		{
			var uri = $"geo:{latlng}?q={WebUtility.UrlEncode(address)}";
			var intent = new Intent(Intent.ActionView, Uri.Parse(uri));
			CrossCurrentActivity.Current.Activity.StartActivity(intent);
		}
	}
}