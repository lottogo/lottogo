﻿using System.ComponentModel;
using Android.Content;
using Android.Content.Res;
using FFImageLoading.Forms;
using FFImageLoading.Forms.Platform;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TintableCachedImage), typeof(TintableCachedImageRenderer))]
namespace LottoGO.Renderers
{
	public class TintableCachedImageRenderer : CachedImageRenderer
	{
		public TintableCachedImageRenderer(Context context) : base(context)
		{
		}

		public TintableCachedImage View => Element as TintableCachedImage;

		protected override void OnElementChanged(ElementChangedEventArgs<CachedImage> e)
		{
			base.OnElementChanged(e);
			TintImage();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (e.PropertyName == TintableCachedImage.TintColorProperty.PropertyName)
			{
				TintImage();
			}
		}

		private void TintImage()
		{
			if (View == null) return;
			if (View.TintColor != Color.Default)
			{
				Control.ImageTintList = ColorStateList.ValueOf(View.TintColor.ToAndroid());
			}
		}
	}
}