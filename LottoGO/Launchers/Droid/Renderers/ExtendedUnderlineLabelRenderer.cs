﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof (ExtendedUnderlineLabel), typeof (ExtendedUnderlineLabelRenderer))]
namespace LottoGO.Renderers
{
    public class ExtendedUnderlineLabelRenderer : LabelRenderer
    {
	    private ExtendedUnderlineLabel ExtendedLabelElement => Element as ExtendedUnderlineLabel;
	    
        public ExtendedUnderlineLabelRenderer(Context context)
            : base(context)
        {
        }

	    protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
	    {
		    base.OnElementChanged(e);
		    SetTextUnderline();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == ExtendedUnderlineLabel.IsUnderlinedProperty.PropertyName)
			{
				SetTextUnderline();
			}
		}
		
	    private void SetTextUnderline()
	    {
		    if (string.IsNullOrEmpty(ExtendedLabelElement.Text))
		    {
			    Control.Text = ExtendedLabelElement.Text;
			    return;
		    }

		    if (ExtendedLabelElement.IsUnderlined)
		    {
			    Control.PaintFlags = PaintFlags.UnderlineText;
			    Control.SetTextColor(Android.Graphics.Color.Red);
		    }
		    else
		    {
			    Control.PaintFlags = PaintFlags.UnderlineText;
				Control.SetTextColor(Android.Graphics.Color.Black);
			}
	    }
	}
}