﻿using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using EntryRenderer = LottoGO.Renderers.EntryRenderer;

[assembly: ExportRenderer(typeof(Entry), typeof(EntryRenderer))]
namespace LottoGO.Renderers
{
	public class EntryRenderer : Xamarin.Forms.Platform.Android.EntryRenderer
	{
		public EntryRenderer(Context context) : base(context)
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
			if (Element != null)
			{
				Control.Background = null;
			}
		}
	}
}