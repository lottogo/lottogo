﻿using Android.Content;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;

[assembly: ExportRenderer (typeof (ExtendedNavigationBarView), typeof (ExtendedNavigationBarRenderer))]
namespace LottoGO.Renderers
{
	public class ExtendedNavigationBarRenderer : BaseVisualElementRenderer<ContentView>
	{
		public ExtendedNavigationBarRenderer(Context context)
			: base(context)
		{
		}
	}
}