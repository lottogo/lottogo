﻿using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using LottoGO.App.Services;
using LottoGO.App.Views;
using LottoGO.Common.Models;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;

[assembly: ExportRenderer(typeof(LottoMap), typeof(LottoMapRenderer))]
namespace LottoGO.Renderers
{
    public class LottoMapRenderer : MapRenderer
    {
        private LottoMap Map => Element as LottoMap;
        
        public LottoMapRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                NativeMap.MarkerClick -= NativeMapOnMarkerClick;
            }

            if (e.NewElement != null)
            {
                Control.GetMapAsync(this);
            }
        }

        protected override void OnMapReady(GoogleMap map)
        {
            base.OnMapReady(map);

            NativeMap.MarkerClick += NativeMapOnMarkerClick;
        }

        private void NativeMapOnMarkerClick(object sender, GoogleMap.MarkerClickEventArgs e)
        {
            var pin = GetCustomPin (e.Marker);
            if (pin == null)
            {
                return;
            }

            Map.PinTappedCommand?.Execute(pin);
        }

        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var lottoPin = (LottoGoPin)pin;
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            marker.SetIcon(GetImageFromGameType(lottoPin.GameType));
            return marker;
        }
        
        private LottoGoPin GetCustomPin(Marker marker)
        {
            return ((IList<LottoGoPin>)Map?.ItemsSource)?.FirstOrDefault(p => p.Position.Latitude == marker.Position.Latitude && p.Position.Longitude == marker.Position.Longitude);
        }
        
        private BitmapDescriptor GetImageFromGameType(GameType customPinGameType)
        {
            var ico = 0;
            switch (customPinGameType)
            {
                case GameType.ThrowADice:
                    ico = Resource.Drawable.dice;
                    break;
                //case GameType.SlotMachine:
                //    ico = Resource.Drawable.slot;
                //    break;
                //case GameType.TableChoice:
                    //ico = Resource.Drawable.ticket;
                    //break;
                case GameType.Geo:
                    ico = Resource.Drawable.choose;
                    break;
            }

            return BitmapDescriptorFactory.FromResource(ico);
        }
    }
}