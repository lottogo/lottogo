﻿using System.ComponentModel;
using Android.Content;
using Android.Text;
using Android.Widget;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof (ExtendedLabel), typeof (ExtendedLabelRenderer))]
namespace LottoGO.Renderers
{
    public class ExtendedLabelRenderer : LabelRenderer
    {
	    private ExtendedLabel ExtendedLabelElement => Element as ExtendedLabel;
	    
        public ExtendedLabelRenderer(Context context)
            : base(context)
        {
        }

	    protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
	    {
		    base.OnElementChanged(e);
		    SetLabelHtmlText();
		    SetLabelLinesCount();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == Label.TextProperty.PropertyName)
			{
				SetLabelHtmlText();
			}

			if (e.PropertyName == ExtendedLabel.LinesCountProperty.PropertyName)
			{
				SetLabelLinesCount();
			}
		}

	    private void SetLabelLinesCount()
	    {
		    if (ExtendedLabelElement.LinesCount != -1)
		    {
			    Control.SetLines(ExtendedLabelElement.LinesCount);;
		    }
	    }


	    private void SetLabelHtmlText()
	    {
		    if (ExtendedLabelElement.ShouldRenderAsHtml)
		    {
			    if (string.IsNullOrEmpty(ExtendedLabelElement?.Text))
			    {
					Control.SetText(string.Empty, TextView.BufferType.Normal);
				    return;
			    }

			    Control.SetText(Html.FromHtml(ExtendedLabelElement.Text), TextView.BufferType.Normal);
			}
		}
	}
}