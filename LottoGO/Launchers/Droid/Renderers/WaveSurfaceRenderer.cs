﻿using System;
using System.ComponentModel;
using Android.App;
using Android.Content;
using LottoGO.App.Controls;
using LottoGO.Renderers;
using LottoGO.WaveWrapper;
using Plugin.CurrentActivity;
using WaveEngine.Adapter;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(WaveEngineSurface), typeof(WaveEngineSurfaceRenderer))]

namespace LottoGO.Renderers
{
	public class WaveEngineSurfaceRenderer : ViewRenderer<WaveEngineSurface, GLView>
	{
		private GLView gameView;

		public WaveEngineSurfaceRenderer(Context context) : base(context)
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<WaveEngineSurface> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				gameView = null;
			}

			if (e.NewElement != null)
			{
				gameView = new GLView(Context, null);
				var activity = Context as Activity;
				SetNativeControl(gameView);
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName.Equals(WaveEngineSurface.GameProperty.PropertyName,
				StringComparison.CurrentCultureIgnoreCase)) InitGame();
		}

		private void InitGame()
		{
			(CrossCurrentActivity.Current.Activity as MainActivity).Initialize(Element.Game);
		}
	}
}