﻿using System.ComponentModel;
using Android.Content;
using Android.Content.Res;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Java.Util;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;

[assembly: ExportRenderer (typeof (ExtendedButton), typeof (ExtendedButtonRenderer))]
namespace LottoGO.Renderers
{
    public class ExtendedButtonRenderer : ButtonRenderer
    {
	    private ExtendedButton ButtonElement => Element as ExtendedButton;
	    
        public ExtendedButtonRenderer(Context context)
            : base(context)
        {
        }

	    protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
	    {
		    base.OnElementPropertyChanged(sender, e);
		    Control.Background =
			    GetPressedColorRippleDrawable(ButtonElement.BackgroundColor.ToAndroid(), Color.Argb(90, 0, 0, 0));
	    }

	    private RippleDrawable GetPressedColorRippleDrawable(Color normalColor, Color pressedColor)
	    {
		    return new RippleDrawable(GetPressedColorSelector(pressedColor, normalColor), new ColorDrawable(normalColor), GetRippleMask(normalColor)) { Radius = ButtonElement.CornerRadius};
	    }

	    private Drawable GetRippleMask(Color color)
	    {
		    float[] outerRadii = new float[8];
		    Arrays.Fill(outerRadii, 3);
		    RoundRectShape r = new RoundRectShape(outerRadii, null, null);
		    ShapeDrawable shapeDrawable = new ShapeDrawable(r);
		    shapeDrawable.Paint.Color = color;
		    return shapeDrawable;
	    }

		private ColorStateList GetPressedColorSelector(int pressedColor, int normalColor)
	    {
		    return new ColorStateList(
			    new int[][]
			    {
				    new int[] {Android.Resource.Attribute.StatePressed},
				    new int[] {Android.Resource.Attribute.StateFocused},
				    new int[] {Android.Resource.Attribute.StateActivated},
					new int[] { }
			    },
			    new int[]
			    {
				    pressedColor,
				    pressedColor,
				    pressedColor,
				    normalColor
			    });

	    }
	}
}