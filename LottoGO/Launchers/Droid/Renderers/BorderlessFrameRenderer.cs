﻿using Android.Content;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof (BorderlessFrame), typeof (BorderlessFrameRenderer))]
namespace LottoGO.Renderers
{
	public class BorderlessFrameRenderer : BaseVisualElementRenderer<ContentView>
	{
		public BorderlessFrameRenderer(Context context) : base(context)
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<ContentView> e)
		{
			base.OnElementChanged(e);
			this.SetBackground(null);
		}
	}
}