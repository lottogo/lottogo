﻿using System.ComponentModel;
using Android.Content;
using Android.Views;
using LottoGO.App.Views.CustomViews;
using LottoGO.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ButtonRenderer = Xamarin.Forms.Platform.Android.AppCompat.ButtonRenderer;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace LottoGO.Renderers
{
	public class CustomButtonRenderer : ButtonRenderer
	{
		public new CustomButton Element => (CustomButton)base.Element;

		public CustomButtonRenderer(Context context) : base(context)
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null)
				return;

			SetTextAlignment();
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == CustomButton.HorizontalTextAlignmentProperty.PropertyName)
				SetTextAlignment();
		}

		public void SetTextAlignment()
		{
			Control.Gravity = Element.HorizontalTextAlignment.ToHorizontalGravityFlags();
		}
	}

	public static class AlignmentHelper
	{
		public static GravityFlags ToHorizontalGravityFlags(this Xamarin.Forms.TextAlignment alignment)
		{
			if (alignment == Xamarin.Forms.TextAlignment.Center)
				return GravityFlags.AxisSpecified;
			return alignment == Xamarin.Forms.TextAlignment.End ? GravityFlags.Right : GravityFlags.Left;
		}
	}
}