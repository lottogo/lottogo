﻿using System.ComponentModel;
using Android.Content;
using LottoGO.App.Views.Base;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace LottoGO.Renderers
{
	public abstract class BaseVisualElementRenderer<TElement> : VisualElementRenderer<TElement>
		where TElement : VisualElement
	{
		public BaseVisualElementRenderer(Context context)
			: base(context)
		{
		}
		
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (Element is IShadowElement shadowElement)
			{
				this.Elevation = shadowElement.HasShadow ? 3 : 0;
			}
		}
	}
}