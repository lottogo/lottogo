﻿using Android.Content.PM;
using LottoGO.App.Messenger.Messages;
using LottoGO.App.Messenger.Observers.Base;
using LottoGO.App.Models;

namespace LottoGO
{
	public class OrientationChangeRequestObserver : MessageObserver<OrientationChangeRequestMessage>
	{
		private readonly MainActivity _mainActivity;

		public OrientationChangeRequestObserver(MainActivity mainActivity)
		{
			_mainActivity = mainActivity;
		}

		protected override void OnMessageArrived(OrientationChangeRequestMessage messageToHandle)
		{
			if (messageToHandle.RequestedOrientation == Orientation.Portrait)
				_mainActivity.RequestedOrientation = ScreenOrientation.Portrait;
			else if (messageToHandle.RequestedOrientation == Orientation.Landscape)
				_mainActivity.RequestedOrientation = ScreenOrientation.Landscape;
			else if (messageToHandle.RequestedOrientation == Orientation.All)
				_mainActivity.RequestedOrientation = ScreenOrientation.Sensor;
		}
	}
}