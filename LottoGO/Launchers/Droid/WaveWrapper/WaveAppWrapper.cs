﻿using System;
using Android.App;
using Android.Views;
using Plugin.CurrentActivity;
using WaveEngine.Adapter;
using WaveEngine.Common;
using Xamarin.Forms.Platform.Android;
using Object = Java.Lang.Object;

namespace LottoGO.WaveWrapper
{
	public class WaveAppWrapper : FormsAppCompatActivity, IAndroidApplication
	{
		private bool fullScreen;

		private IGame game;
		private bool isGameInitialized;

		public int Width => Adapter.Width;

		public int Height => Adapter.Height;

		public GLView View { get; set; }

		public Activity Activity => CrossCurrentActivity.Current.Activity;
		public IAdapter Adapter => View?.Adapter ?? null;

		public void Dispose()
		{
		}

		public void Initialize()
		{
		}

		public void Update(TimeSpan elapsedTime)
		{
			if (game != null)
			{
				if (!isGameInitialized)
				{
					game.Initialize(this);
					isGameInitialized = true;
				}

				game.UpdateFrame(elapsedTime);
			}
		}

		public void Draw(TimeSpan elapsedTime)
		{
			game?.DrawFrame(elapsedTime);
		}

		public void Exit()
		{
			// ????
		}


		public string WindowTitle => "LottoGO!";

		public bool FullScreen
		{
			get => fullScreen;

			set
			{
				if (fullScreen != value)
				{
					fullScreen = value;

					if (Window != null)
					{
						if (value)
							Window.AddFlags(WindowManagerFlags.Fullscreen);
						else
							Window.ClearFlags(WindowManagerFlags.Fullscreen);
					}
				}
			}
		}

		public bool IsEditor => false;
		public ExecutionMode ExecutionMode => ExecutionMode.Standalone;

		public void Initialize(IGame theGame)
		{
			game = theGame;
			isGameInitialized = false;
		}
	}
}