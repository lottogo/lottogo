using Autofac;
using LottoGO.App.Services.Contracts;
using LottoGO.App.Services.Contracts.Contracts;
using LottoGO.Services;

namespace LottoGO.Configuration
{
	internal class PlatformSpecificDependenciesModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			builder.RegisterType<DroidGoToLocationService>().As<IGoToLocationService>();
			builder.RegisterType<DroidLocalFilePathObtainerService>().As<ILocalFilePathObtainerService>();
		}
	}
}