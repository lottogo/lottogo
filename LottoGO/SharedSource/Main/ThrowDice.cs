﻿using System;
using System.Collections.Generic;
using System.Text;
using WaveEngine.Framework;

namespace LottoGO
{
	public class ThrowDice : LottoSceneBase
	{
		public int WiningNumber = 0;


		protected override void CreateScene()
		{
			this.Load(WaveContent.Scenes.VuforiaScene);
		}

		public override void InitializeLotto(GameParams.GameParams parameters)
		{
			WiningNumber = parameters.WinningNumber;
		}
	}
}
