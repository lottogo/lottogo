﻿using System;
using System.Collections.Generic;
using System.Text;
using LottoGO.Components;
using WaveEngine.Framework;

namespace LottoGO
{
	public abstract class LottoSceneBase : Scene
	{
		public abstract void InitializeLotto(GameParams.GameParams parameters);

		protected override void Start()
		{
			
			this.AddSceneBehavior(new GameComponent(), SceneBehavior.Order.PostUpdate);
		}
	}
}
