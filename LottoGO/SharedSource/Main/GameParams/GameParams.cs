﻿namespace LottoGO.GameParams
{
	public class GameParams
	{
		public string GameType { get; set; }
		public string Marker { get; set; }
		public int WinningNumber { get; set; }
	}
}