﻿#region Using Statements
using System;
using LottoGO.Components;
using WaveEngine.Common;
using WaveEngine.Common.Graphics;
using WaveEngine.Common.Math;
using WaveEngine.Components.Cameras;
using WaveEngine.Components.Graphics2D;
using WaveEngine.Components.Graphics3D;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Resources;
using WaveEngine.Framework.Services;
#endregion

namespace LottoGO
{
	public class MarkersScene : LottoSceneBase
	{
		public string WinningMarker = "";

		
		protected override void CreateScene()
		{
			this.Load(WaveContent.Scenes.VuforiaScene);
			Start();
		}

		public override void InitializeLotto(GameParams.GameParams parameters)
		{
			WinningMarker = parameters.Marker;
		}

		protected virtual void Start()
		{
			var krk = this.EntityManager.Find("defaultCamera3D");
			krk.AddComponent(new GameInfoComponent(null,WinningMarker,0));
		}
	}
}
