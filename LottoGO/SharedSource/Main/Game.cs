#region using Statements
using System;
using WaveEngine.Common;
using WaveEngine.Common.Graphics;
using WaveEngine.Framework;
using WaveEngine.Framework.Services;
#endregion

namespace LottoGO
{
    public class Game : WaveEngine.Framework.Game
    {
	    public GameParams.GameParams GameParams;
	    public event EventHandler GameWon;
		public event EventHandler GameLost;


	    public virtual void NotifyGameWon()
	    {
			GameWon?.Invoke(this,EventArgs.Empty);
	    }

	    public virtual void NotifyGameLost()
	    {	
			GameLost?.Invoke(this,EventArgs.Empty);
	    }

	    public void LottoSetup(GameParams.GameParams game)
	    {
		    GameParams = game;
	    }

		public override void Initialize(IApplication application)
        {
            base.Initialize(application);

            this.Load(WaveContent.GameInfo);
	        LottoSceneBase sceneBase = null;
			
	        switch (GameParams.GameType)
	        {
				case "Geo":
					sceneBase = new MarkersScene();
					break;
				case "ThrowDice":
					sceneBase = new ThrowDice();
					break;
	        }
			sceneBase.InitializeLotto(GameParams);
			ScreenContext screenContext = new ScreenContext(sceneBase);	
			WaveServices.ScreenContextManager.To(screenContext);
        }
    }
}
