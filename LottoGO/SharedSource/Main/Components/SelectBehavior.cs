﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LottoGO.Components;
using WaveEngine.Common.Input;
using WaveEngine.Common.Math;
using WaveEngine.Components.AR;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics3D;
using WaveEngine.Framework.Services;

namespace LottoGO.Components
{
	[DataContract]
	public class SelectBehavior : Behavior
	{
		private float bestValue;

		// A camera required component to calculate the projections
		[RequiredComponent] public ARCameraRig Camera;

		private float? collisionResult;
		private Entity currentEntity;
		private TouchLocation currentLocation;
		private Vector3 direction;
		private Collider3D entityCollider;
		private Vector3 farPoint;
		private Scene myScene;
		private Vector3 nearPoint;
		private Ray ray;
		public string EntityChoosen;

		private GameInfoComponent gic;

		private string marker;
		// Cached variables
		private TouchPanelState touchPanelState;

		/// <summary>
		///     Resolve dependencies method
		/// </summary>
		protected override void ResolveDependencies()
		{
			base.ResolveDependencies();

			myScene = WaveServices.ScreenContextManager.CurrentContext[0];
		}

		/// <summary>
		///     Update method
		/// </summary>
		/// <param name="gameTime"></param>
		protected override void Update(TimeSpan gameTime)
		{
			touchPanelState = WaveServices.Input.TouchPanelState;
			// bestValue = float.MaxValue;
			if (touchPanelState.IsConnected && touchPanelState.Count > 0)
			{
				// Calculate the ray
				CalculateRay();

				var hitResult = myScene.PhysicsManager.Simulation3D.RayCast(ref ray, 1000);
				if (hitResult.Succeeded)
				{
					var userData = (Collider3D)hitResult.Collider.UserData;
					//Owner.AddComponent(new WinComponent());
					var beh = userData.Owner.FindComponent<HideBehavior>();
					this.EntityManager.Add(new Entity("Choosen").AddChild(new Entity(beh.trackableIdentity.StringValue)));
					this.Owner.AddComponent(new LoseComponent());
					//if (beh.trackableIdentity.StringValue == marker)
					//	Win();
					//else
					//	Lose();
					this.IsActive = false;
					beh.Owner.IsVisible = false;
				}
			}
			else
			{
				if (myScene != null)
				{
					//this.myScene.ShowPickedEntity("None");
				}
			}
		}

		private void Lose()
		{
			Owner.AddComponent(new LoseComponent());
		}

		private void Win()
		{
			Owner.AddComponent(new WinComponent());
		}

		/// <summary>
		///     Initialize method
		/// </summary>
		protected override void Initialize()
		{
			base.Initialize();

			nearPoint = new Vector3();
			farPoint = new Vector3(0f, 0f, 1f);
			ray = new Ray();
		}

		/// <summary>
		///     Calculate a ray between the camer and the selected point
		/// </summary>
		private void CalculateRay()
		{
			// Get the current touch location
			currentLocation = touchPanelState.First();

			// Apply the near point:
			nearPoint.X = currentLocation.Position.X;
			nearPoint.Y = currentLocation.Position.Y;
			nearPoint.Z = 0f;
			// And the far point
			farPoint.X = currentLocation.Position.X;
			farPoint.Y = currentLocation.Position.Y;
			farPoint.Z = 1f;

			// Unproject both to get the 2d point in a 3d screen projections. 

			nearPoint = Camera.Unproject(ref nearPoint);
			farPoint = Camera.Unproject(ref farPoint);

			// Now, we have a 3d point. We calculate the point direction
			direction = farPoint - nearPoint;
			// And normalized it.
			direction.Normalize();

			// Set the ray to the cached variable
			ray.Direction = direction;
			ray.Position = nearPoint;
		}
	}
}
