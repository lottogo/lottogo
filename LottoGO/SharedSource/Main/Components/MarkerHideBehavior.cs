﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Attributes;
using WaveEngine.Common.Math;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;
using WaveEngine.Framework.Physics3D;
using WaveEngine.Framework.Services;
using WaveEngine.Vuforia;
using System.Linq;

namespace LottoGO
{
	[DataContract]
	public class HideBehavior : Behavior
	{
		[RequiredComponent(true)]
		public ARVu​Mark​Trackable​Behavior trackableIdentity;


		protected override void Update(TimeSpan gameTime)
		{
			var provider = WaveServices.GetService<VuforiaService>();

			if (provider != null && provider.TrackableResults.Any())
			{
				var entity =
					provider.TrackableResults.FirstOrDefault(x => x.Trackable.Name == trackableIdentity.StringValue);

				if (entity!=null && entity.Status == TrackableStatus.Tracked )
				{
					Debug.WriteLine($"Marker Found {entity.Trackable.Name}");
					Owner.IsVisible = true;
				}
				else
				{
					Owner.IsVisible = false;
				}
			}
			else
			{
				Owner.IsVisible = false;
			}
		}
	}
}
