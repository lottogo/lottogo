﻿using System;
using System.Runtime.Serialization;
using LottoGO.App.ViewModels;
using WaveEngine.Framework;

namespace LottoGO.Components
{
	[DataContract]
	public class LoseComponent : Behavior
	{
		private float _timeElapsed;
		private readonly float treshold = 10;
		private bool _initialized = false;
		protected override void Update(TimeSpan gameTime)
		{
			if (!_initialized)
			{
				this.EntityManager.Find("coin").IsVisible = true;
				//play sound
				_initialized = true;
			}

			_timeElapsed += (float) gameTime.TotalSeconds;
			if (_timeElapsed > treshold)
				GameViewModel.Instance.NotifyGameLost();
		}
	}
}