﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Framework;

namespace LottoGO.Components
{
	[DataContract]
	public class GameInfoComponent : Behavior
	{
		public GameInfoComponent(Game _game, string _marker, int _number)
		{
			this.Game = _game;
			this.Marker = _marker;
			this.Number = _number;
		}

		public Game Game;
		public readonly string Marker;
		public readonly int Number;

		protected override void Update(TimeSpan gameTime)
		{
			//throw new NotImplementedException();
		}
	}
}
