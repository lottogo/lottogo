﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Math;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;

namespace LottoGO.Components
{
	[DataContract]
	public class BouncyBehavor : Behavior
	{
		[RequiredComponent]
		private Transform3D myTransform;

		[DataMember]
		public float ValueY { get; set; }

		private float _factor = 1.0f;

		[DataMember]
		public float Rotation { get; set; }

		[DataMember]
		public float Scale { get; set; }

		protected override void DefaultValues()
		{
			ValueY = 0.01f;
			Rotation = 5.0f;
			Scale = 5.0f;
		}


		protected override void Update(TimeSpan gameTime)
		{
			Vector3 rotation = this.myTransform.Rotation;
			rotation.Y += (float)gameTime.TotalSeconds * this.Rotation;
			this.myTransform.Rotation = rotation;

			/*Vector3 scale = this.myTransform.Scale;
			_scale = (float)Math.Sin(gameTime.TotalSeconds);
			scale.X += _scale;
			scale.Z += _scale;
			scale.Y += _scale;
			this.myTransform.Scale = scale;*/

			Vector3 position = this.myTransform.Position;

			if (position.Y > 2.0f)
				_factor = -1.0f;
			else
				_factor = 1.0f;
			position.Y += _factor * ValueY;
			this.myTransform.Position = position;
		}
	}
}
