﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LottoGO.App.ViewModels;
using WaveEngine.Framework;

namespace LottoGO.Components
{
	[DataContract]
	public class WinComponent : Behavior
	{
		[RequiredComponent] public GameInfoComponent GIC;
		private float _timeElapsed = 0;
		private float treshold = 10;

		private bool _initialized = false;
		protected override void Update(TimeSpan gameTime)
		{
			if (!_initialized)
			{
				this.EntityManager.Find("coin").IsVisible = true;
				
				//play sound
				_initialized = true;
			}


			_timeElapsed += (float)gameTime.TotalSeconds;
			if (_timeElapsed > treshold)
			{
				GameViewModel.Instance.NotifyGameWon();
			}
		}
	}
}
