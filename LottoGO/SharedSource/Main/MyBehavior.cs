﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using WaveEngine.Common.Math;
using WaveEngine.Framework;
using WaveEngine.Framework.Graphics;

namespace LottoGO
{
	[DataContract]
	public class MyBehavior : Behavior
	{
		[RequiredComponent]
		private Transform3D myTransform;

		protected override void Update(TimeSpan gameTime)
		{
			Vector3 rotation = this.myTransform.Rotation;
			rotation.Y += (float)gameTime.TotalSeconds;
			this.myTransform.Rotation = rotation;
		}
	}
}