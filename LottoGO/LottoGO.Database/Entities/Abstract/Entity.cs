﻿using System.ComponentModel.DataAnnotations;

namespace LottoGO.Database.Entities.Abstract
{
    public abstract class Entity<T> :  IEntity<T>
    {
        [Key]
        public T Id { get; set; }
    }
}
