﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace LottoGO.Database.Entities
{
	public class UserEntity : IdentityUser
	{
		public decimal AccountBalance { get; set; }
		public string PushToken { get; set; }
		public bool IsActive { get; set; }
		public string ReferralCode { get; set; }

		public virtual ICollection<UserGames> UserGames { get; set; }
	}
}