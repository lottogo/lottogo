﻿using LottoGO.Database.Entities.Abstract;

namespace LottoGO.Database.Entities
{
    public class MapPoints : Entity<int>
    {
		public double Longitude { get; set; }
	    public double Latitude { get; set; }
	    public decimal StandardPrize { get; set; }
		public decimal ExtraPrize { get; set; }
		public decimal SingleGameCost { get; set; }
		public string MarkerId { get; set; }
	    public string Name { get; set; }
		public int GameType { get; set; }
		public bool IsActive { get; set; }
		public string MultipleMarkers { get; set; }
    }
}