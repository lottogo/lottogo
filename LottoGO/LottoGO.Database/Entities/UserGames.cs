﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using LottoGO.Database.Entities.Abstract;

namespace LottoGO.Database.Entities
{
    public class UserGames : Entity<int>
    {
	    [ForeignKey("User")]
	    public string UserId { get; set; }
	    [ForeignKey("Point")]
	    public int PointId { get; set; }

		public bool? IsWon { get; set; }
		public DateTime? StartDateTime { get; set; }
		public DateTime? EndDateTime { get; set; }
		public decimal Prize { get; set; }
	    public int GameType { get; set; }
		public int SumToWin { get; set; }
	    public decimal GameCost { get; set; }
		public string MarkerIdToWin { get; set; }

		public virtual MapPoints Point { get; set; }
	    public virtual UserEntity User { get; set; }
	}
}
