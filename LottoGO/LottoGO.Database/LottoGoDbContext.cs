﻿using LottoGO.Database.Entities;
using LottoGO.Database.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace LottoGO.Database
{
	public class LottoGoDbContext : IdentityDbContext<UserEntity>
	{
		private readonly IOptions<AppConfig> _appConfig;

		public virtual DbSet<UserGames> UserGames { get; set; }
		public virtual DbSet<MapPoints> MapPoints { get; set; }

		public LottoGoDbContext(IOptions<AppConfig> appConfig)
		{
			_appConfig = appConfig;
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			var connectionString = _appConfig.Value.MsSqlConnectionString;
			optionsBuilder.UseSqlServer(connectionString, opt => opt.CommandTimeout(30));

			//var lf = new LoggerFactory();
			//lf.AddProvider(new EFLogger());
			//optionsBuilder.UseLoggerFactory(lf);
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<MapPoints>().Property(p => p.Longitude).HasColumnType("decimal(12,8)").IsRequired();
			builder.Entity<MapPoints>().Property(p => p.Latitude).HasColumnType("decimal(12,8)").IsRequired();
			base.OnModelCreating(builder);
		}
	}
}
