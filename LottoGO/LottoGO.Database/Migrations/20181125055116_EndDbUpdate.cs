﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LottoGO.Database.Migrations
{
    public partial class EndDbUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "GameCost",
                table: "UserGames",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "MarkerIdToWin",
                table: "UserGames",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferralCode",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GameCost",
                table: "UserGames");

            migrationBuilder.DropColumn(
                name: "MarkerIdToWin",
                table: "UserGames");

            migrationBuilder.DropColumn(
                name: "ReferralCode",
                table: "AspNetUsers");
        }
    }
}
