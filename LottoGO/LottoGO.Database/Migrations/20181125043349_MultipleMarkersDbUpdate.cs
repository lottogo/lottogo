﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LottoGO.Database.Migrations
{
    public partial class MultipleMarkersDbUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LuckyRatio",
                table: "UserGames");

            migrationBuilder.AddColumn<int>(
                name: "SumToWin",
                table: "UserGames",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "MultipleMarkers",
                table: "MapPoints",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SumToWin",
                table: "UserGames");

            migrationBuilder.DropColumn(
                name: "MultipleMarkers",
                table: "MapPoints");

            migrationBuilder.AddColumn<double>(
                name: "LuckyRatio",
                table: "UserGames",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
