﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LottoGO.Database.Migrations
{
    public partial class PointNameDbUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "MapPoints",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "MapPoints");
        }
    }
}
