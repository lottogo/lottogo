﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LottoGO.Database.Migrations
{
    public partial class PuthTokenDbUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PushToken",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PushToken",
                table: "AspNetUsers");
        }
    }
}
