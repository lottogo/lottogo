﻿namespace LottoGO.Common.Models.DTOs.Requests
{
    public class GameResultDto
    {
		public int GameId { get; set; }
		public bool Won { get; set; }
    }
}
