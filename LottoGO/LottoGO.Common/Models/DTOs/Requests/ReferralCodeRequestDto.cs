﻿namespace LottoGO.Common.Models.DTOs.Requests
{
	public class ReferralCodeRequestDto
	{
		public string ReferralCode { get; set; } 
	}
}
