﻿namespace LottoGO.Common.Models.DTOs.Responses
{
    public class LoginRequestDto
    {
	    public string Username { get; set; }
	    public string Password { get; set; }
	}
}
