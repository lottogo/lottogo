﻿namespace LottoGO.Common.Models.DTOs.Requests
{
	public class PushTokenRequestDto
	{
		public string Token { get; set; }
	}
}