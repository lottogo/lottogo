﻿namespace LottoGO.Common.Models.DTOs.Requests
{
    public class StartGameRequestDto
    {
	    public int MapPointId { get; set; }
		public string MarkerId { get; set; }
	}
}
