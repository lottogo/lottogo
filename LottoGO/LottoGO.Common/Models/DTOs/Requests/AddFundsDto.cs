﻿namespace LottoGO.Common.Models.DTOs.Requests
{
	public class AddFundsDto
	{
		public decimal Amount { get; set; }
	}
}