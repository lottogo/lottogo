﻿namespace LottoGO.Common.Models.DTOs.Requests
{
	public class GameResultRequestDto
	{
		public int GameId { get; set; }
		public bool IsWon { get; set; }
	}
}