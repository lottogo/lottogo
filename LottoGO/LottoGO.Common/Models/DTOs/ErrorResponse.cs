﻿using System.Collections.Generic;
using LottoGO.Common.Models.DTOs.Responses;

namespace LottoGO.Common.Models.DTOs
{
    public class ErrorResponse : ResponseBase
    {
		public IEnumerable<ServerErrorResponseDto> ErrorDetails { get; set; }
    }
    
    public class GenericResponse<T> : ResponseBase
    {
        public IEnumerable<ServerErrorResponseDto> ErrorDetails { get; set; }
        
        public GenericResponse()
        {
            IsSuccess = true;
        }

        public T Response { get; set; }
    }
}