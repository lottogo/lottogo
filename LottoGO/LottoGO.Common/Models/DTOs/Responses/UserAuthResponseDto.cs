﻿namespace LottoGO.Common.Models.DTOs.Responses
{
    public class UserAuthResponseDto
    {
	    public string Username { get; set; }
	    public string Email { get; set; }
		public MoneyDto AccountBalance { get; set; }
	    public string AccessToken { get; set; }
	    public int AccessTokenExpirationSeconds { get; set; }
		public string ReferralCode { get; set; }
	}
}
