﻿namespace LottoGO.Common.Models.DTOs.Responses
{
    public class MoneyDto
    {
	    public MoneyDto(decimal amount, string currency)
	    {
		    Amount = amount;
		    Currency = currency;
	    }

	    public decimal Amount { get; set; }
	    public string Currency { get; set; }
	}
}
