﻿namespace LottoGO.Common.Models.DTOs.Responses
{
	public class ServerErrorResponseDto
	{
		public ServerErrorResponseDto(string errorName, string errorDescription)
		{
			ErrorName = errorName;
			ErrorDescription = errorDescription;
		}

		public string ErrorName { get; set; }
		public string ErrorDescription { get; set; }
	}
}
