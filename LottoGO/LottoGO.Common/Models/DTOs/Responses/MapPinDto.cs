﻿namespace LottoGO.Common.Models.DTOs.Responses
{
    public class MapPinDto
    {
	    public MapPinDto(int id, double longitude, double latitude, bool visitedByUser, decimal? prize, string markerId, GameType gameType, MoneyDto singleGameCost, bool isExtraPrize)
	    {
		    Id = id;
		    Longitude = longitude;
		    Latitude = latitude;
		    VisitedByUser = visitedByUser;
		    Prize = prize;
		    MarkerId = markerId;
		    GameType = gameType;
		    SingleGameCost = singleGameCost;
		    IsExtraPrize = isExtraPrize;
	    }

		public int Id { get; set; }
		public double Longitude { get; set; }
	    public double Latitude { get; set; }
		public bool VisitedByUser { get; set; }
	    public bool IsExtraPrize { get; set; }
		public decimal? Prize { get; set; }
	    public GameType GameType { get; set; }
	    public string MarkerId { get; set; }
		public MoneyDto SingleGameCost { get; set; }
	}
}