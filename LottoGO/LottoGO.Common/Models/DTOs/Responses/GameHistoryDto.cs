﻿using System;

namespace LottoGO.Common.Models.DTOs.Responses
{
	public class GameHistoryDto
	{
		public GameHistoryDto(int gameId, bool isWon, MoneyDto prize, DateTime dateTime)
		{
			GameId = gameId;
			IsWon = isWon;
			Prize = prize;
			DateTime = dateTime;
		}

		public int GameId { get; set; }
		public bool IsWon { get; set; }
		public MoneyDto Prize { get; set; }
		public DateTime DateTime { get; set; }
	}
}
