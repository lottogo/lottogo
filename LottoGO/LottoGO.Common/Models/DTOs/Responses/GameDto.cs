﻿namespace LottoGO.Common.Models.DTOs.Responses
{
    public class GameDto
    {
	    public GameDto(int gameId, GameType gameType, decimal prize, int sumToWin)
	    {
		    GameId = gameId;
		    GameType = gameType;
		    Prize = prize;
		    SumToWin = sumToWin;
	    }

		public int GameId { get; set; }
		public GameType GameType { get; set; }
		public decimal Prize { get; set; }
		public int SumToWin { get; set; }
    }
}