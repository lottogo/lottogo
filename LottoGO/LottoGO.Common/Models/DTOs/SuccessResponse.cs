﻿namespace LottoGO.Common.Models.DTOs
{
    public class SuccessResponse<T> : ResponseBase
    {
	    public SuccessResponse()
	    {
		    IsSuccess = true;
	    }

		public T Response { get; set; }
    }
}