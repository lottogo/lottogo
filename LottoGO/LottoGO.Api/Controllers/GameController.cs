﻿using System;
using System.Net;
using System.Threading.Tasks;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace LottoGO.Api.Controllers
{
	[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
	[Route("api/[controller]")]
	public class GameController : BaseController
    {
	    private readonly ILogger<GameController> _logger;
	    private readonly IStringLocalizer<GameController> _stringLocalizer;
	    private readonly IGameService _gameService;

	    public GameController(ILogger<GameController> logger, IStringLocalizer<GameController> stringLocalizer, IGameService gameService)
	    {
		    _logger = logger;
		    _stringLocalizer = stringLocalizer;
		    _gameService = gameService;
	    }

	    /// <summary>
	    /// Start a game.
	    /// </summary>
	    /// <response code="201">Returns object of game.</response>
	    /// <response code="400">Bad request, details in response.</response>
	    /// <response code="401">Unauthorized request.</response>
	    /// <response code="500">Server error</response>
	    [ProducesResponseType(typeof(SuccessResponse<GameDto>), 201)]
	    [ProducesResponseType(typeof(ErrorResponse), 400)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpPost("startgame")]
	    public async Task<IActionResult> StartGame([FromForm] StartGameRequestDto startGameRequest)
	    {
		    try
		    {
			    var result = await _gameService.StartGame(UserId, startGameRequest);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }


	    /// <summary>
	    /// Send result of game.
	    /// </summary>
	    /// <response code="200">Returns object of account balance.</response>
	    /// <response code="400">Bad request, details in response.</response>
	    /// <response code="401">Unauthorized request.</response>
	    /// <response code="500">Server error</response>
	    [ProducesResponseType(typeof(SuccessResponse<GameDto>), 201)]
	    [ProducesResponseType(typeof(ErrorResponse), 400)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpPost("sendgameresult")]
	    public async Task<IActionResult> SendGameResult([FromForm] GameResultRequestDto gameResultRequestDto)
	    {
		    try
		    {
			    var result = await _gameService.SendGameResult(UserId, gameResultRequestDto);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }
	}
}