﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace LottoGO.Api.Controllers
{
	[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
	[Route("api/[controller]")]
	public class UserController : BaseController
    {
	    private readonly ILogger<UserController> _logger;
	    private readonly IUserService _userService;
	    private readonly IStringLocalizer<BaseController> _stringLocalizer;

	    public UserController(ILogger<UserController> logger, IUserService userService, IStringLocalizer<BaseController> stringLocalizer)
		{
			_logger = logger;
			_userService = userService;
			_stringLocalizer = stringLocalizer;
		}

	    /// <summary>
	    /// User registration.
	    /// </summary>
	    /// <response code="201">Returns object of user.</response>
	    /// <response code="400">Bad request, details in response.</response>
	    /// <response code="500">Server error</response>
	    [ProducesResponseType(typeof(SuccessResponse<UserAuthResponseDto>), 201)]
	    [ProducesResponseType(typeof(ErrorResponse), 400)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpPost("register")]
		[AllowAnonymous]
	    public async Task<IActionResult> Register([FromForm] RegisterRequestDto registerRequest)
        {
	        try
	        {
		        var result = await _userService.RegisterUser(registerRequest);
		        return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.Created : HttpStatusCode.BadRequest);
	        }
	        catch (Exception e)
	        {
				_logger.LogError(e.ToString());
				return CreateServerResponse(new ErrorResponse()
				{
					ErrorDetails = new []
					{
						new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
					}
				}, HttpStatusCode.InternalServerError);
	        }
        }

	    /// <summary>
	    /// User login.
	    /// </summary>
	    /// <response code="200">Returns object of user.</response>
	    /// <response code="400">Bad request, details in response.</response>
	    /// <response code="500">Server error</response>
	    [ProducesResponseType(typeof(SuccessResponse<UserAuthResponseDto>), 200)]
	    [ProducesResponseType(typeof(ErrorResponse), 400)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpPost("login")]
	    [AllowAnonymous]
	    public async Task<IActionResult> Login([FromForm] LoginRequestDto loginRequestDto)
	    {
		    try
		    {
			    var result = await _userService.LoginUser(loginRequestDto);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }


		/// <summary>
		/// Push notification token send.
		/// </summary>
		/// <response code="200">Returns empty string object.</response>
		/// <response code="400">Bad request, details in response.</response>
		/// <response code="401">Unauthorized request.</response>
		/// <response code="500">Server error</response>
		[ProducesResponseType(typeof(SuccessResponse<string>), 201)]
	    [ProducesResponseType(typeof(ErrorResponse), 400)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpPost("pushtoken")]
	    public async Task<IActionResult> PushToken([FromForm] PushTokenRequestDto pushTokenRequestDto)
	    {
		    try
		    {
			    var result = await _userService.SavePushToken(UserId, pushTokenRequestDto);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.Created : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }

	    /// <summary>
	    /// Add funds to user account
	    /// </summary>
	    /// <response code="200">Returns object of current user funds.</response>
	    /// <response code="401">Unauthorized request.</response>
	    /// <response code="500">Server error</response>
	    [ProducesResponseType(typeof(SuccessResponse<MoneyDto>), 200)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpPost("addfunds")]
	    public async Task<IActionResult> AddFunds([FromForm] AddFundsDto addFunds)
	    {
		    try
		    {
			    var result = await _userService.AddFunds(UserId, addFunds.Amount);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }

	    /// <summary>
	    /// Get game history for user
	    /// </summary>
	    /// <response code="200">Returns array of game objects.</response>
	    /// <response code="401">Unauthorized request.</response>
	    /// <response code="500">Server error</response>
	    [ProducesResponseType(typeof(SuccessResponse<IEnumerable<GameHistoryDto>>), 200)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpGet("getgamehistory")]
	    public async Task<IActionResult> GetGameHistory()
	    {
		    try
		    {
			    var result = await _userService.GetGameHistory(UserId);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }

	    /// <summary>
	    /// Get game history for user
	    /// </summary>
	    /// <response code="200">Returns array of game objects.</response>
	    /// <response code="401">Unauthorized request.</response>
	    /// <response code="500">Server error</response>
	    [ProducesResponseType(typeof(SuccessResponse<MoneyDto>), 200)]
	    [ProducesResponseType(typeof(ErrorResponse), 500)]
	    [HttpPost("usereferallcode")]
	    public async Task<IActionResult> UseReferallCode([FromForm] ReferralCodeRequestDto referralCodeRequestDto)
	    {
		    try
		    {
			    var result = await _userService.UseReferallCode(UserId, referralCodeRequestDto);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }
	}
}