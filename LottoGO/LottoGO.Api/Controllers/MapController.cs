﻿using System;
using System.Net;
using System.Threading.Tasks;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace LottoGO.Api.Controllers
{
	[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
	[Route("api/[controller]")]
	public class MapController : BaseController
	{
		private readonly ILogger<MapController> _logger;
		private readonly IStringLocalizer<BaseController> _stringLocalizer;
		private readonly IMapService _mapService;

		public MapController(ILogger<MapController> logger, IStringLocalizer<BaseController> stringLocalizer, IMapService mapService)
		{
			_logger = logger;
			_stringLocalizer = stringLocalizer;
			_mapService = mapService;
		}

		/// <summary>
		/// Get map points.
		/// </summary>
		/// <response code="200">Returns array of map objects.</response>
		/// <response code="400">Bad request, details in response.</response>
		/// <response code="401">Unauthorized request.</response>
		/// <response code="500">Server error</response>
		[ProducesResponseType(typeof(SuccessResponse<MapPinDto>), 200)]
		[ProducesResponseType(typeof(ErrorResponse), 400)]
		[ProducesResponseType(typeof(ErrorResponse), 500)]
		[HttpGet("getmappoints")]
		public async Task<IActionResult> GetMapPoints()
		{
			try
		    {
			    var result = await _mapService.GetMapPoints(UserId);
			    return CreateServerResponse(result, result.IsSuccess ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		    }
		    catch (Exception e)
		    {
			    _logger.LogError(e.ToString());
			    return CreateServerResponse(new ErrorResponse()
			    {
				    ErrorDetails = new[]
				    {
					    new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("UnknownError"))
				    }
			    }, HttpStatusCode.InternalServerError);
		    }
	    }
	}
}