﻿using System.Text.RegularExpressions;

namespace LottoGO.Api.Extensions
{
	public static class StringExtensions
	{
		public static bool IsValidEmail(this string input)
		{
			var strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
			                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
			                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			var re = new Regex(strRegex);
			return re.IsMatch(input);
		}
	}
}
