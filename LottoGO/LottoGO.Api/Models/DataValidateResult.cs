﻿using System.Collections.Generic;
using LottoGO.Common.Models.DTOs.Responses;

namespace LottoGO.Api.Models
{
    public class DataValidateResult
    {
	    public bool IsValid { get; set; }
	    public IList<ServerErrorResponseDto> Errors { get; set; }
    }
}