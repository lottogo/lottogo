﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using LottoGO.Api.Helpers;
using LottoGO.Api.Helpers.Validators;
using LottoGO.Api.Services;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Database;
using LottoGO.Database.Entities;
using LottoGO.Database.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace LottoGO.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

	    public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
	        services.AddLocalization(opt => opt.ResourcesPath = "Resources");
	        services.AddDbContext<LottoGoDbContext>();
	        services.AddIdentity<UserEntity, IdentityRole>((o) =>
		        {
			        o.Password.RequireDigit = false;
			        o.Password.RequireLowercase = false;
			        o.Password.RequireUppercase = false;
			        o.Password.RequireNonAlphanumeric = false;
			        o.Password.RequiredLength = Consts.MinimumPasswordLength;
			        o.User.RequireUniqueEmail = false;
			        o.User.AllowedUserNameCharacters = null;
		        })
		        .AddEntityFrameworkStores<LottoGoDbContext>()
		        .AddDefaultTokenProviders();

	        services.AddTransient<IAuthenticationService, AuthenticationService>();
	        services.AddTransient<IUserDataValidator, UserDataValidator>();
	        services.AddTransient<IGameValidator, GameValidator>();
			services.AddTransient<IUserService, UserService>();
	        services.AddTransient<IMapService, MapService>();
	        services.AddTransient<IGameService, GameService>();

			var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Consts.SecretKey));

	        var tokenValidationParameters = new TokenValidationParameters
	        {
		        ValidateIssuerSigningKey = true,
		        IssuerSigningKey = signingKey,
		        ValidateIssuer = true,
		        ValidIssuer = Consts.AppName,
		        ValidateAudience = true,
		        ValidAudience = Consts.AppName,
		        ValidateLifetime = true,
		        ClockSkew = TimeSpan.Zero,
	        };

	        services.AddAuthentication().AddJwtBearer(options =>
	        {
		        options.TokenValidationParameters = tokenValidationParameters;
	        });

	        services.AddSwaggerGen(c =>
	        {
		        c.SwaggerDoc("v1", new Info { Title = "LottoGO REST API", Version = "v1", Description = "REST API methods documentation for LottoGO application" });

		        var basePath = AppContext.BaseDirectory;
		        var xmlPath = Path.Combine(basePath, "LottoGO.Api.xml");
		        c.IncludeXmlComments(xmlPath);
		        c.DocumentFilter<LowercaseDocumentFilter>();
	        });

			services.Configure<AppConfig>(Configuration);
		}

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

	        var supportedCultures = new[] { new CultureInfo("pl-PL") };
	        app.UseRequestLocalization(new RequestLocalizationOptions
	        {
		        DefaultRequestCulture = new RequestCulture("pl-PL"),
		        SupportedCultures = supportedCultures,
		        SupportedUICultures = supportedCultures,
		        RequestCultureProviders = new List<IRequestCultureProvider>()
	        });

	        app.UseSwagger();
	        app.UseSwaggerUI(o => o.SwaggerEndpoint("/swagger/v1/swagger.json", "Cards REST API service documentation"));
			loggerFactory.AddFile("Logs/lottogo_log.txt");

	        using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
	        {
		        using (var db = serviceScope.ServiceProvider.GetService<LottoGoDbContext>())
		        {
			        db.Database.Migrate();
		        }
	        }

			app.UseMvc();
        }
    }
}
