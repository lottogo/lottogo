﻿using System.Threading.Tasks;
using LottoGO.Common.Models.DTOs;

namespace LottoGO.Api.Services.Interfaces
{
	public interface IMapService
	{
		Task<ResponseBase> GetMapPoints(string userId);
	}
}