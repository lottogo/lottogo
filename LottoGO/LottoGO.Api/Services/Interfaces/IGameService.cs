﻿using System.Threading.Tasks;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;

namespace LottoGO.Api.Services.Interfaces
{
	public interface IGameService
	{
		Task<ResponseBase> StartGame(string userId, StartGameRequestDto startGameRequest);
		Task<ResponseBase> SendGameResult(string userId, GameResultRequestDto gameResultRequestDto);
	}
}
