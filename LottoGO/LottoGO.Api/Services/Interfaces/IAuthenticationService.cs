﻿using LottoGO.Database.Entities;

namespace LottoGO.Api.Services.Interfaces
{
	public interface IAuthenticationService
	{
		string GenerateToken(UserEntity user);
	}
}
