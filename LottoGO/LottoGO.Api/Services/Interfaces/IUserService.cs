﻿using System.Threading.Tasks;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;

namespace LottoGO.Api.Services.Interfaces
{
    public interface IUserService
    {
	    Task<ResponseBase> RegisterUser(RegisterRequestDto registerRequestDto);
	    Task<ResponseBase> LoginUser(LoginRequestDto loginRequestDto);
	    Task<ResponseBase> SavePushToken(string userId, PushTokenRequestDto pushTokenRequestDto); 
	    Task<ResponseBase> AddFunds(string userId, decimal amount);
	    Task<ResponseBase> GetGameHistory(string userId);
	    Task<ResponseBase> UseReferallCode(string userId, ReferralCodeRequestDto referralCodeRequestDto);
    }
}
