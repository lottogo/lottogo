﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LottoGO.Api.Helpers;
using LottoGO.Api.Helpers.Validators;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using LottoGO.Database;
using LottoGO.Database.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace LottoGO.Api.Services
{
	public class UserService : IUserService
	{
		private readonly IStringLocalizer<UserService> _stringLocalizer;
		private readonly IUserDataValidator _userDataValidator;
		private readonly SignInManager<UserEntity> _signInManager;
		private readonly IAuthenticationService _authenticationService;
		private readonly ILogger<UserService> _logger;
		private readonly LottoGoDbContext _context;
		private readonly UserManager<UserEntity> _userManager;

		public UserService(IStringLocalizer<UserService> stringLocalizer, IUserDataValidator userDataValidator, UserManager<UserEntity> userManager, SignInManager<UserEntity> signInManager,
			IAuthenticationService authenticationService, ILogger<UserService> logger, LottoGoDbContext context)
		{
			_stringLocalizer = stringLocalizer;
			_userDataValidator = userDataValidator;
			_signInManager = signInManager;
			_authenticationService = authenticationService;
			_logger = logger;
			_context = context;
			_userManager = userManager;
		}

		public async Task<ResponseBase> RegisterUser(RegisterRequestDto registerRequestDto)
		{
			var validationResult = await _userDataValidator.RegisterUserValidate(registerRequestDto);
			if (!validationResult.IsValid)
			{
				return new ErrorResponse
				{
					ErrorDetails = validationResult.Errors
				};
			}

			var userEntity = new UserEntity
			{
				Email = registerRequestDto.Email,
				UserName = registerRequestDto.Username,
				IsActive = true,
				ReferralCode = RandomCodeGenerator.GenerateRandomString(7)
			};

			var registrationResult = await _userManager.CreateAsync(userEntity, registerRequestDto.Password);
			if (!registrationResult.Succeeded)
			{
				_logger.LogError($"ERROR DURING USER REGISTRATION: \n");
				registrationResult.Errors.ToList().ForEach(e => _logger.LogError($"{e.Code} {e.Description}"));
				return new ErrorResponse()
				{
					ErrorDetails = new[]
					{
						new ServerErrorResponseDto(_stringLocalizer.GetString("ServerError"), _stringLocalizer.GetString("CannotRegisterUser"))
					}
				};
			}

			return new SuccessResponse<UserAuthResponseDto>()
			{
				IsSuccess = true,
				Response = CreateAuthUserResponse(userEntity)
			};
		}

		public async Task<ResponseBase> LoginUser(LoginRequestDto loginRequestDto)
		{
			var validationResult = _userDataValidator.LoginUserValidate(loginRequestDto);
			if (!validationResult.IsValid)
			{
				return new ErrorResponse
				{
					ErrorDetails = validationResult.Errors
				};
			}

			var userEntity = await _userManager.FindByNameAsync(loginRequestDto.Username);
			if (userEntity == null || !userEntity.IsActive)
			{
				return new ErrorResponse
				{
					ErrorDetails = new[]
					{
						new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("WrongUsernameOrPassword"))
					}
				};
			}

			var loginResult = await _signInManager.PasswordSignInAsync(userEntity, loginRequestDto.Password, false, false);

			if (!loginResult.Succeeded)
			{
				return new ErrorResponse
				{
					ErrorDetails = new[]
					{
						new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("WrongUsernameOrPassword"))
					}
				};
			}

			return new SuccessResponse<UserAuthResponseDto>()
			{
				IsSuccess = true,
				Response = CreateAuthUserResponse(userEntity)
			};
		}

		public async Task<ResponseBase> SavePushToken(string userId, PushTokenRequestDto pushTokenRequestDto)
		{
			var validationResult = _userDataValidator.PushTokenValidate(pushTokenRequestDto);
			if (!validationResult.IsValid)
			{
				return new ErrorResponse
				{
					ErrorDetails = validationResult.Errors
				};
			}

			var user = await _context.Users.FindAsync(userId);
			user.PushToken = pushTokenRequestDto.Token;
			_context.Entry(user).State = EntityState.Modified;
			await _context.SaveChangesAsync();

			return new SuccessResponse<string>()
			{
				IsSuccess = true,
				Response = string.Empty
			};
		}

		public async Task<ResponseBase> AddFunds(string userId, decimal funds)
		{
			var userEntity = await _context.Users.FindAsync(userId);
			userEntity.AccountBalance += funds;
			_context.Entry(userEntity).State = EntityState.Modified;
			await _context.SaveChangesAsync();

			return new SuccessResponse<MoneyDto>()
			{
				IsSuccess = true,
				Response = new MoneyDto(userEntity.AccountBalance, Consts.Currency)
			};
		}

		public async Task<ResponseBase> GetGameHistory(string userId)
		{
			var userGames = await _context.UserGames.Where(u => u.UserId == userId && u.EndDateTime != null).Select(g => new GameHistoryDto(g.Id, g.IsWon.Value, new MoneyDto(g.Prize, Consts.Currency), g.EndDateTime.Value)).Take(20).ToListAsync();

			return new SuccessResponse<IEnumerable<GameHistoryDto>>()
			{
				IsSuccess = true,
				Response = userGames
			};
		}

		public async Task<ResponseBase> UseReferallCode(string userId, ReferralCodeRequestDto referralCodeRequestDto)
		{
			var validationResult = _userDataValidator.RefereralCodeValidate(referralCodeRequestDto);
			if (!validationResult.IsValid)
			{
				return new ErrorResponse
				{
					ErrorDetails = validationResult.Errors
				};
			}

			var user = await _context.Users.FindAsync(userId);
			var secondUser =
				await _context.Users.FirstOrDefaultAsync(u => u.ReferralCode == referralCodeRequestDto.ReferralCode);

			user.AccountBalance += Consts.ReferralPrize;
			secondUser.AccountBalance += Consts.ReferralPrize;

			_context.Entry(user).State = EntityState.Modified;
			_context.Entry(secondUser).State = EntityState.Modified;

			await _context.SaveChangesAsync();

			return new SuccessResponse<MoneyDto>()
			{
				Response = new MoneyDto(user.AccountBalance, Consts.Currency)
			};
		}

		private UserAuthResponseDto CreateAuthUserResponse(UserEntity user)
		{
			return new UserAuthResponseDto
			{
				Email = user.Email,
				Username = user.UserName,
				AccessTokenExpirationSeconds = Consts.TokenExpirationSeconds,
				AccountBalance = new MoneyDto(user.AccountBalance, Consts.Currency),
				AccessToken = _authenticationService.GenerateToken(user),
				ReferralCode = user.ReferralCode
			};
		}
	}
}