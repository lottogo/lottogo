﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LottoGO.Api.Helpers;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Common.Models;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Responses;
using LottoGO.Database;
using Microsoft.EntityFrameworkCore;

namespace LottoGO.Api.Services
{
	public class MapService : IMapService
	{
		private readonly LottoGoDbContext _context;

		public MapService(LottoGoDbContext context)
		{
			_context = context;
		}

		public async Task<ResponseBase> GetMapPoints(string userId)
		{
			var mapPoints = await _context.MapPoints.Where(p=>p.IsActive).ToListAsync();
			var userMapPoints = await _context.Users.Where(u => u.Id == userId).SelectMany(u => u.UserGames.Select(g => g.PointId))
				.ToListAsync();

			return new SuccessResponse<IEnumerable<MapPinDto>>()
			{
				Response = mapPoints.Select(p=> new MapPinDto(p.Id, p.Longitude, p.Latitude, userMapPoints.Contains(p.Id), p.ExtraPrize != decimal.Zero ? p.ExtraPrize : p.StandardPrize, p.MarkerId, (GameType)p.GameType, new MoneyDto(p.SingleGameCost, Consts.Currency), p.ExtraPrize != decimal.Zero)).ToList(),
				IsSuccess = true
			};
		}
	}
}
