﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using LottoGO.Api.Helpers;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Database.Entities;
using Microsoft.IdentityModel.Tokens;

namespace LottoGO.Api.Services
{
    public class AuthenticationService : IAuthenticationService
    {
	    public string GenerateToken(UserEntity user)
	    {
		    var dateTimeNow = DateTime.UtcNow;

		    var claims = new[]
		    {
			    new Claim(ClaimTypes.Name, user.Id),
			    new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
			    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
			    new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.FromUnixTimeSeconds(dateTimeNow.Second).ToString(), ClaimValueTypes.Integer64)
		    };

		    var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Consts.SecretKey));

		    var jwt = new JwtSecurityToken(
			    Consts.AppName,
			    Consts.AppName,
			    claims,
			    dateTimeNow,
			    dateTimeNow.Add(TimeSpan.FromSeconds(Consts.TokenExpirationSeconds)),
			    new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
		    return new JwtSecurityTokenHandler().WriteToken(jwt);
	    }
	}
}
