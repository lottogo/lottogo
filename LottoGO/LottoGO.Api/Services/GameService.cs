﻿using System;
using System.Threading.Tasks;
using LottoGO.Api.Helpers;
using LottoGO.Api.Helpers.Validators;
using LottoGO.Api.Services.Interfaces;
using LottoGO.Common.Models;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using LottoGO.Database;
using LottoGO.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace LottoGO.Api.Services
{
	public class GameService : IGameService
	{
		private readonly IGameValidator _validator;
		private readonly LottoGoDbContext _context;

		public GameService(IGameValidator validator, LottoGoDbContext context)
		{
			_validator = validator;
			_context = context;
		}

		public async Task<ResponseBase> StartGame(string userId, StartGameRequestDto startGameRequest)
		{
			var validationResult = await _validator.StartGameValidate(userId, startGameRequest?.MapPointId, startGameRequest?.MarkerId);
			var mapPoint = await _context.MapPoints.FindAsync(startGameRequest.MapPointId);

			if (!validationResult.IsValid)
			{
				return new ErrorResponse
				{
					ErrorDetails = validationResult.Errors
				};
			}

			var gameEntity = new UserGames()
			{
				PointId = startGameRequest.MapPointId,
				UserId = userId,
				StartDateTime = DateTime.UtcNow,
				GameType = mapPoint.GameType,
				Prize = mapPoint.ExtraPrize != decimal.Zero ? mapPoint.ExtraPrize : mapPoint.StandardPrize,
				GameCost = mapPoint.SingleGameCost
			};

			switch ((GameType)gameEntity.GameType)
			{
				case GameType.Geo:
					gameEntity.MarkerIdToWin = GetMarkerIdToWin(mapPoint.MultipleMarkers);
					break;
				case GameType.ThrowADice:
					gameEntity.SumToWin = GetSumToWin();
					break;
			}

			var user = await _context.Users.FindAsync(userId);
			user.AccountBalance -= mapPoint.SingleGameCost;

			_context.Entry(gameEntity).State = EntityState.Added;
			_context.Entry(user).State = EntityState.Modified;

			await _context.SaveChangesAsync();

			return new SuccessResponse<GameDto>
			{
				Response = new GameDto(gameEntity.Id, (GameType)gameEntity.GameType, gameEntity.Prize, gameEntity.SumToWin)
			};
		}

		public async Task<ResponseBase> SendGameResult(string userId, GameResultRequestDto gameResultRequestDto)
		{
			var validationResult = _validator.GameResultValidate(gameResultRequestDto);

			if (!validationResult.IsValid)
			{
				return new ErrorResponse
				{
					ErrorDetails = validationResult.Errors
				};
			}

			var userGame = await _context.UserGames.FindAsync(gameResultRequestDto.GameId);
			userGame.IsWon = gameResultRequestDto.IsWon;
			userGame.EndDateTime = DateTime.UtcNow;

			var user = await _context.Users.FindAsync(userId);

			if (gameResultRequestDto.IsWon)
			{
				user.AccountBalance += userGame.Prize;
				_context.Entry(user).State = EntityState.Modified;
			}

			_context.Entry(userGame).State = EntityState.Modified;
			await _context.SaveChangesAsync();

			return new SuccessResponse<MoneyDto>
			{
				Response = new MoneyDto(user.AccountBalance, Consts.Currency)
			};
		}

		private static string GetMarkerIdToWin(string mapPointMultipleMarkers)
		{
			var ids = mapPointMultipleMarkers.Split(';');
			return ids[new Random().Next(0, ids.Length - 1)];
		}

		private static int GetSumToWin()
		{
			return new Random().Next(8,30);
		}
	}
}
