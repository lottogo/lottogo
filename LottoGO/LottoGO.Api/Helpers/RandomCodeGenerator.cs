﻿using System;
using System.Linq;

namespace LottoGO.Api.Helpers
{
	public class RandomCodeGenerator
	{
		public static string GenerateRandomString(int length)
		{
			var random = new Random();
			const string chars = "abcdeFGHiJKLMnoPQRSTuvWXYZ0123456789";
			return new string(Enumerable.Repeat(chars, length)
				.Select(s => s[random.Next(s.Length)]).ToArray());
		}
	}
}
