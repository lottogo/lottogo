﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LottoGO.Api.Helpers
{
	public class LowercaseJsonConverter
	{
		public static string Serialize(object objectToSerialize)
		{
			return JsonConvert.SerializeObject(objectToSerialize, new JsonSerializerSettings()
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			});
		}
	}
}
