﻿namespace LottoGO.Api.Helpers
{
    public static class Consts
    {
	    public static int MinimumPasswordLength => 6;
	    public static int TokenExpirationSeconds => 86400;
	    public static string Currency => "PLN";
	    public static string AppName => "LottoGO";
	    public static string SecretKey => "lotto9CJ*Y5sauyts33k";
	    public static decimal ReferralPrize => 50;
    }
}
