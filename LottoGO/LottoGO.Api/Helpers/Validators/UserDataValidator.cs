﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LottoGO.Api.Extensions;
using LottoGO.Api.Models;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using LottoGO.Database.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Localization;

namespace LottoGO.Api.Helpers.Validators
{
	public class UserDataValidator : IUserDataValidator
	{
		private readonly UserManager<UserEntity> _userManager;
		private readonly IStringLocalizer<UserDataValidator> _stringLocalizer;

		public UserDataValidator(UserManager<UserEntity> userManager, IStringLocalizer<UserDataValidator> stringLocalizer)
		{
			_userManager = userManager;
			_stringLocalizer = stringLocalizer;
		}

		public async Task<DataValidateResult> RegisterUserValidate(RegisterRequestDto registerRequestDto)
		{
			var errors = new List<ServerErrorResponseDto>();

			if (string.IsNullOrEmpty(registerRequestDto?.Email))
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("EmailEmptyError")));
			}
			else if (!registerRequestDto.Email.IsValidEmail())
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("EmailWrongFormatError")));
			}

			if (string.IsNullOrEmpty(registerRequestDto?.Username))
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("UsernameEmptyError")));
			}

			if (string.IsNullOrEmpty(registerRequestDto?.Password) || registerRequestDto?.Password.Length < Consts.MinimumPasswordLength)
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("MinimumLengthError")));
			}

			if (errors.Any())
			{
				return new DataValidateResult()
				{
					Errors = errors
				};
			}

			if (await _userManager.FindByEmailAsync(registerRequestDto?.Email) != null)
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("EmailExistsError")));
			}

			if (registerRequestDto != null && await _userManager.FindByNameAsync(registerRequestDto.Username) != null)
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("UsernameExists")));
			}

			return new DataValidateResult()
			{
				Errors = errors,
				IsValid = !errors.Any()
			};
		}

		public DataValidateResult LoginUserValidate(LoginRequestDto loginRequestDto)
		{
			var errors = new List<ServerErrorResponseDto>();

			if (string.IsNullOrEmpty(loginRequestDto?.Username) || string.IsNullOrEmpty(loginRequestDto?.Password))
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("AllDataRequired")));
			}

			return new DataValidateResult()
			{
				Errors = errors,
				IsValid = !errors.Any()
			};
		}

		public DataValidateResult PushTokenValidate(PushTokenRequestDto pushTokenRequestDto)
		{
			var errors = new List<ServerErrorResponseDto>();

			if (string.IsNullOrEmpty(pushTokenRequestDto?.Token))
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("AllDataRequired")));
			}

			return new DataValidateResult()
			{
				Errors = errors,
				IsValid = !errors.Any()
			};
		}

		public DataValidateResult RefereralCodeValidate(ReferralCodeRequestDto referralCodeRequestDto)
		{
			var errors = new List<ServerErrorResponseDto>();

			if (string.IsNullOrEmpty(referralCodeRequestDto?.ReferralCode))
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("AllDataRequired")));
			}

			return new DataValidateResult()
			{
				Errors = errors,
				IsValid = !errors.Any()
			};
		}
	}
}
