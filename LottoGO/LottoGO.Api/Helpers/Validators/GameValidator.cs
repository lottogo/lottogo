﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LottoGO.Api.Models;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using LottoGO.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace LottoGO.Api.Helpers.Validators
{
	public class GameValidator : IGameValidator
	{
		private readonly IStringLocalizer<GameValidator> _stringLocalizer;
		private readonly LottoGoDbContext _context;

		public GameValidator(IStringLocalizer<GameValidator> stringLocalizer, LottoGoDbContext context)
		{
			_stringLocalizer = stringLocalizer;
			_context = context;
		}

		public async Task<DataValidateResult> StartGameValidate(string userId, int? mapPointId, string markerId)
		{
			var errors = new List<ServerErrorResponseDto>();

			if (mapPointId == null || string.IsNullOrEmpty(markerId))
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("AllDataRequired")));
				return new DataValidateResult()
				{
					Errors = errors
				};
			}

			var userAccountBalance = (await _context.Users.FirstOrDefaultAsync(u => u.Id == userId)).AccountBalance;
			var mapPoint = await _context.MapPoints.FindAsync(mapPointId);
			
			if (mapPoint == null)
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("MapPointNotExists")));
				return new DataValidateResult()
				{
					Errors = errors
				};
			}

			if (string.IsNullOrEmpty(markerId) || !mapPoint.MarkerId.Equals(markerId))
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("WrongMarkerId")));
				return new DataValidateResult()
				{
					Errors = errors
				};
			}

			if (userAccountBalance < mapPoint.SingleGameCost)
			{
				errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("InsufficientFunds"), _stringLocalizer.GetString("InsufficientFundsDetail")));
			}

			return new DataValidateResult()
			{
				Errors = errors,
				IsValid = !errors.Any(),
			};
		}

		public DataValidateResult GameResultValidate(GameResultRequestDto gameResultRequestDto)
		{
			var errors = new List<ServerErrorResponseDto>();

			if (gameResultRequestDto.GameId != 0)
				return new DataValidateResult()
				{
					Errors = errors,
					IsValid = !errors.Any()
				};

			errors.Add(new ServerErrorResponseDto(_stringLocalizer.GetString("WrongData"), _stringLocalizer.GetString("AllDataRequired")));
			return new DataValidateResult()
			{
				Errors = errors
			};
		}
	}
}
