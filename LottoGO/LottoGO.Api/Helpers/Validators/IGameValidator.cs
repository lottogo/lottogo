﻿using System.Threading.Tasks;
using LottoGO.Api.Models;
using LottoGO.Common.Models.DTOs.Requests;

namespace LottoGO.Api.Helpers.Validators
{
	public interface IGameValidator
	{
		Task<DataValidateResult> StartGameValidate(string userId, int? mapPointId, string markerId);
		DataValidateResult GameResultValidate(GameResultRequestDto gameResultRequestDto);
	}
}