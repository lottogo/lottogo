﻿using System.Threading.Tasks;
using LottoGO.Api.Models;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;

namespace LottoGO.Api.Helpers.Validators
{
    public interface IUserDataValidator
    {
	    Task<DataValidateResult> RegisterUserValidate(RegisterRequestDto registerRequestDto);
	    DataValidateResult LoginUserValidate(LoginRequestDto loginRequestDto);
	    DataValidateResult PushTokenValidate(PushTokenRequestDto pushTokenRequestDto);
	    DataValidateResult RefereralCodeValidate(ReferralCodeRequestDto referralCodeRequestDto);
	}
}
