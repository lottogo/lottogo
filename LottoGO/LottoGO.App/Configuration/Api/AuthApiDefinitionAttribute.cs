﻿using System;
using Refit.Insane.PowerPack.Attributes;

namespace LottoGO.App.Configuration.Api
{
	public class AuthApiDefinitionAttribute : ApiDefinitionAttribute
	{
		public AuthApiDefinitionAttribute() : base(ApiConfiguration.ApiUrl, typeof(AuthClientHandler))
		{
			ApiTimeout = TimeSpan.FromMinutes(2);
		}
        
		public new TimeSpan ApiTimeout { get; }
	}
}