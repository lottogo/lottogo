﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.Helpers;
using Refit.Insane.PowerPack.Attributes;

namespace LottoGO.App.Configuration.Api
{
	public class AuthClientHandler : HttpClientDiagnosticsHandler
	{
		public AuthClientHandler(HttpMessageHandler innerHandler)
			: base(innerHandler)
		{
		}
    
		public AuthClientHandler()
		{
		}
        
		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			//todo add header based on authorization type
			if (request.Headers.Authorization?.Parameter == null)
			{
//				string accesToken = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Settings.Login}:{Settings.Password}"));
				request.Headers.Add("Authorization", $"Bearer {Settings.AuthData?.AccessToken}");    
			}

			Debug.WriteLine(request.RequestUri);
			if (request.Content != null)
			{
				var requestContent = await request.Content.ReadAsStringAsync();
				if (requestContent != null)
				{
					Debug.WriteLine(requestContent);
				}
			}
            
			var response = await base.SendAsync(request, cancellationToken);
			if (response.Content != null)
			{
				var content = await response.Content?.ReadAsStringAsync();
				if (content != null)
				{
					Debug.WriteLine(content);
				}
			}
            
			return response;
		}
	}

	public class AuthHttpClientHandler : HttpClientHandler
	{
		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			//todo add header based on authorization type
			if (request.Headers.Authorization?.Parameter == null)
			{

				string username = "ace-abb-user";
				string password = "ojbBB47?#]zpkcRGGZT961)";

				//:TODO Add to Settings

				string accesToken = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));
				request.Headers.Add("Authorization", $"Basic {accesToken}");
			}

			return base.SendAsync(request, cancellationToken);
		}
	}
}