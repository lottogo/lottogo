﻿using Autofac;
using LottoGO.App.Services;
using LottoGO.App.Services.Contracts;

namespace LottoGO.App.Configuration.Ioc.Modules
{
	public class PortableDataServiceDependenciesModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			builder.RegisterType<MasterMenuDataService>().As<IMasterMenuDataService>();
		}
	}
}