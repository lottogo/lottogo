﻿using System.Reflection;
using Autofac;
using LottoGO.App.Views.Base;
using Module = Autofac.Module;

namespace LottoGO.App.Configuration.Ioc.Modules
{
	public class ViewsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);
			
			builder.RegisterAssemblyTypes(IntrospectionExtensions.GetTypeInfo(typeof(PortableDepenedenciesModule)).Assembly)
				.AssignableTo<IBasePage>()
				.AsSelf();
		}
	}
}