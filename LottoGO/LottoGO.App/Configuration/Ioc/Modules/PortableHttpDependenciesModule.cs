﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Autofac;
using LottoGO.App.Configuration.Api;
using LottoGO.App.Services;
using LottoGO.App.Services.Contracts;
using Refit.Insane.PowerPack.Services;

namespace LottoGO.App.Configuration.Ioc.Modules
{
	public class PortableHttpDependenciesModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			builder.Register(context =>
			{
				var dict = new Dictionary<Type, DelegatingHandler>();
				dict.Add(typeof(AuthClientHandler), new AuthClientHandler());
				return new RefitRestServiceRetryProxy(new RefitRestService(dict), ThisAssembly);
			}).As<IRestService>()
				.SingleInstance();

			builder.RegisterType<MapPinsService>().As<IMapPinsService>();
			builder.RegisterType<GameHistoryService>().As<IGameHistoryService>();
			builder.RegisterType<ReferralService>().As<IReferralService>();
			builder.RegisterType<AuthenticationService>().As<IAuthenticationService>();
		}
	}
}