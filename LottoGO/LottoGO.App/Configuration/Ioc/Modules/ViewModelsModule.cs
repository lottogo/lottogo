﻿using System.Reflection;
using Autofac;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;
using Module = Autofac.Module;

namespace LottoGO.App.Configuration.Ioc.Modules
{
	public class ViewModelsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			builder.RegisterAssemblyTypes(IntrospectionExtensions.GetTypeInfo(typeof(PortableDepenedenciesModule))
					.Assembly)
				.AssignableTo<IBaseViewModel>()
				.As<IBaseViewModel, BaseViewModel>()
				.AsSelf()
				.OnActivating(e =>
				{
					var navi = e.Context.Resolve<INavigationService>();
					(e.Instance as INavigationServiceInjector)?.Inject(navi);

					var messenger = e.Context.Resolve<IMvxMessenger>();
					(e.Instance as IMessengerInjector)?.Inject(messenger);
				});
		}
	}
}