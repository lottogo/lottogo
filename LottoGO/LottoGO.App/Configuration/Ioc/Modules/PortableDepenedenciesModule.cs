﻿using Autofac;
using Autofac.Core;
using LottoGO.App.Configuration.Ioc.Contracts;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services;
using LottoGO.App.Services.Contracts;
using LottoGO.App.Services.Contracts.Contracts;

namespace LottoGO.App.Configuration.Ioc.Modules
{
	public class PortableDepenedenciesModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			builder.RegisterType<ViewAndViewModelResolver>().As<IViewAndViewModelResolver>().SingleInstance();

			builder.RegisterType<MvxMessengerHub>().As<IMvxMessenger>().SingleInstance();

			builder.RegisterType<FormsNavigationService>().As<INavigationService>()
				.AsSelf().SingleInstance();

			builder.RegisterType<FileService>().AsSelf();
			builder.RegisterType<CacheFileService>().As<IFileService>().As<ICacheFileDownloadService>()
				.WithParameter(new ResolvedParameter(
					(info, context) => info.ParameterType == typeof(IFileService),
					(info, context) => context.Resolve<FileService>()));
		}
	}
}