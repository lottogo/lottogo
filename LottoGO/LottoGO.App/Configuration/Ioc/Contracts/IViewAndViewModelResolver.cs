﻿using System;
using LottoGO.App.ViewModels.Base;
using LottoGO.App.Views.Base;
using Xamarin.Forms;

namespace LottoGO.App.Configuration.Ioc.Contracts
{
    public interface IViewAndViewModelResolver : IMasterPresenterInjecter
    {
        (TViewModel, IBasePage<TViewModel, TParameter>) ResolveViewModelAndPage<TViewModel, TParameter>()
            where TViewModel : class, IBaseViewModel<TParameter> where TParameter : class;

	    IBaseViewModel ResolveViewModel(Type viewModelType);

	    Page GetFormsPage(Type viewModelType);
    }

	public interface IMasterPresenterInjecter
	{
		void InjectMasterPresenterAction(Action presentAction);
	}
}