﻿using System.Collections.Generic;
using Autofac;
using Autofac.Features.ResolveAnything;
using LottoGO.App.Configuration.Ioc.Modules;

namespace LottoGO.App.Configuration.Ioc
{
	public abstract class ContainerDependencyModulesProvider
	{
		protected abstract IEnumerable<Module> GetPlatformSpecificModules();

		public virtual ContainerBuilder RegisterModules()
		{
			var builder = new ContainerBuilder();
			builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());

			builder.RegisterModule<PortableDepenedenciesModule>();
			builder.RegisterModule<PortableDataServiceDependenciesModule>();
			builder.RegisterModule<PortableHttpDependenciesModule>();
			builder.RegisterModule<ViewModelsModule>();
			builder.RegisterModule<ViewsModule>();

			foreach (var platformSpecificModule in GetPlatformSpecificModules())
				builder.RegisterModule(platformSpecificModule);

			return builder;
		}
	}
}