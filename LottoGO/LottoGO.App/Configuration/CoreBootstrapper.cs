﻿using AutoMapper;

namespace LottoGO.App.Configuration
{
	public static class CoreBootstrapper
	{
		private static bool _initialized;

		public static void Init()
		{
			if (_initialized) return;
			Mapper.Initialize(cfg =>
				{
				});

			Mapper.AssertConfigurationIsValid();
			_initialized = true;
		}
	}
}