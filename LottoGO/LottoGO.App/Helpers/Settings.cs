﻿using LottoGO.App.Services.Contracts;
using LottoGO.Common.Models.DTOs.Responses;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace LottoGO.App.Helpers
{
	public static class Settings
	{
		private const string AuthDataKey = "auth_data_key";
		
		private static ISettings AppSettings => CrossSettings.Current;

		public static UserAuthResponseDto AuthData
		{
			get => GetValueOrDefault<UserAuthResponseDto>(AuthDataKey);
			set => AddOrUpdateValue(value, AuthDataKey);
		}

		private static void AddOrUpdateValue<TValue>(TValue value, string settingsKey)
		{
			if (value is string stringValue)
			{
				AppSettings.AddOrUpdateValue(settingsKey, stringValue);
				return;
			}

			var serialized = JsonConvert.SerializeObject(value);
			AppSettings.AddOrUpdateValue(settingsKey, serialized);
		}

		private static TValue GetValueOrDefault<TValue>(string settingsKey)
		{
			var value = AppSettings.GetValueOrDefault(settingsKey, null);

			if (value is TValue stringValue)
				return stringValue;

			if (value == null) return default;
			var deserialized = JsonConvert.DeserializeObject<TValue>(value);
			return deserialized;
		}
	}
}