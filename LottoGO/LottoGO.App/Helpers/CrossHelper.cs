﻿using System.IO;
using System.Reflection;

namespace LottoGO.App.Helpers
{
	public static class CrossHelper
	{
		public static Stream GetResourceStream(string assetName)
		{
			var assembly = typeof(CrossHelper).GetTypeInfo().Assembly;
			return assembly.GetManifestResourceStream($"LottoGo.Assets.{assetName}");
		}
	}
}