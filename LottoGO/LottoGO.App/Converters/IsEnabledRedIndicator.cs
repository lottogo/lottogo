﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
    public class IsEnabledRedIndicator : IValueConverter
    {
	    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
	    {
			var boolValue = bool.Parse(value.ToString());
		    return !boolValue ? Color.White : Color.Red;
		}

	    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
	    {
		    throw new NotImplementedException();
	    }
    }
}
