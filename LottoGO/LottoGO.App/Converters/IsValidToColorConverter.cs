﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
	public class IsValidToColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var validColor = Color.Black;
			var invalidColor = Color.Red;
			return (bool)value ? validColor : invalidColor;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}