﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
	public class IsEnabledToButtonStyleConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var boolValue = bool.Parse(value.ToString());
			var name = !boolValue ? "DisabledRedButtonStyle" : "RedButtonStyle";
			return Application.Current.Resources[name];
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}