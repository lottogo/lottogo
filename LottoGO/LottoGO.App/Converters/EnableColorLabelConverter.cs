﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
	public class EnableColorLabelConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var converterParameter = Boolean.Parse(parameter.ToString());
			var enabledColor = Color.Black;
			var disabledColor = Color.Silver;
			return (bool) value == converterParameter ? enabledColor : disabledColor;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
	
	public class GameWonToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var won = Boolean.Parse(value.ToString());
			return won ? "WYGRANA" : "PRZEGRANA";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}