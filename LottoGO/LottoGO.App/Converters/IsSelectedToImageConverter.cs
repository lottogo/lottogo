﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
	public class IsSelectedToImageConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var boolValue = bool.Parse(value.ToString());
			return !boolValue ? ImageSource.FromFile("ico_check0.png") : ImageSource.FromFile("ico_check1.png");
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}