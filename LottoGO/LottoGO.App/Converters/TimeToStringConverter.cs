﻿using System;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
	public class TimeToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var time = value.ToString();
			if (!time.Any(char.IsDigit) || time.Length>10)
			{
				return time="VARIES";
			}
			return time;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
