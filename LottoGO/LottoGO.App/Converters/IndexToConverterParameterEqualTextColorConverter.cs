﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
	public class IndexToConverterParameterEqualTextColorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var selectedIndex = int.Parse(value.ToString());
			var parameterIndex = int.Parse(parameter.ToString());

			return selectedIndex == parameterIndex ? Color.Black : Color.LightGray;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}