﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LottoGO.App.Converters
{
	public class ScreenHeightToViewHeightConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			double multiplier;
			try
			{
				multiplier = double.Parse(parameter.ToString().Replace(".", ","), NumberStyles.AllowDecimalPoint);
			}
			catch (Exception e)
			{
				multiplier = double.Parse(parameter.ToString().Replace(",", "."), NumberStyles.AllowDecimalPoint);
			}

			if (multiplier > 0)
			{
				return multiplier * (Device.Info.PixelScreenSize.Height / Device.Info.ScalingFactor);
			}
			else
				return Device.Info.PixelScreenSize.Height / Device.Info.ScalingFactor;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}