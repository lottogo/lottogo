﻿using System;
using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;

namespace LottoGO.App.Commands.History
{
    public class LoadGameHistoryDataCommandBuilder : AsyncGuardedCommandBuilder
    {
        private readonly HistoryViewModel _historyViewModel;
        private readonly IGameHistoryService _gameHistoryService;

        public LoadGameHistoryDataCommandBuilder(HistoryViewModel historyViewModel, IGameHistoryService gameHistoryService, IMvxMessenger messenger) : base(messenger)
        {
            _historyViewModel = historyViewModel;
            _gameHistoryService = gameHistoryService;
        }

        protected override async Task ExecuteCommandAction()
        {
            var result = await _gameHistoryService.GetGameHistory();
            if (result.IsSuccess)
            {
                _historyViewModel.Items.Clear();
                foreach (var pin in result.Results)
                {
                    _historyViewModel.Items.Add(pin);
                }
            }
        }
    }
}