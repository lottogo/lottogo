﻿using System;
using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace LottoGO.App.Commands.Login
{
    public class LoginCommandBuilder : AsyncGuardedCommandBuilder
    {
        private readonly LoginViewModel _loginViewModel;
        private readonly IAuthenticationService _authenticationService;

        public LoginCommandBuilder(LoginViewModel loginViewModel, IAuthenticationService authenticationService,
            IMvxMessenger messenger) : base(messenger)
        {
            _loginViewModel = loginViewModel;
            _authenticationService = authenticationService;
        }

        protected override async Task ExecuteCommandAction()
        {
            var loginResult = await _authenticationService.Login(_loginViewModel.Login, _loginViewModel.Password);
            if (loginResult.IsSuccess)
            {
                if (await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location) !=
                    PermissionStatus.Granted)
                {
                    var result = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    if (result[Permission.Location] != PermissionStatus.Granted)
                        return;
                }

                if (await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 5))
                    App.GeolocationWasStarted = true;
                
                await _loginViewModel.NavigationService.NavigateTo<MainViewModel, UserAuthResponse>(loginResult.Results);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Błąd", $"{loginResult.FormattedErrorMessages}", "OK");
            }
        }
    }
}