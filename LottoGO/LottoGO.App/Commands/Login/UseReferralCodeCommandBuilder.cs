﻿using System;
using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Helpers;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using LottoGO.Common.Models.DTOs.Responses;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace LottoGO.App.Commands.Login
{
    public class UseReferralCodeCommandBuilder : AsyncGuardedCommandBuilder
    {
        private readonly AcceptReferViewModel _acceptReferViewModel;
        private readonly IReferralService _referralService;

        public UseReferralCodeCommandBuilder(AcceptReferViewModel acceptReferViewModel, IReferralService referralService,
            IMvxMessenger messenger) : base(messenger)
        {
            _acceptReferViewModel = acceptReferViewModel;
            _referralService = referralService;
        }

        protected override async Task ExecuteCommandAction()
        {
            var result = await _referralService.UseReferralCode(_acceptReferViewModel.Code);
            if (result.IsSuccess)
            {
                var authData = Settings.AuthData;
                authData.AccountBalance = new MoneyDto(result.Results.Amount, result.Results.Currency);
                Settings.AuthData = authData;
                await _acceptReferViewModel.NavigationService.NavigateTo<MapViewModel, string>(string.Empty);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Błąd", $"{result.FormattedErrorMessages}", "OK");
            }
        }
    }
}