﻿using System;
using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace LottoGO.App.Commands.Login
{
    public class RegisterCommandBuilder : AsyncGuardedCommandBuilder
    {
        private readonly RegisterViewModel _registerViewModel;
        private readonly IAuthenticationService _authenticationService;

        public RegisterCommandBuilder(RegisterViewModel registerViewModel, IAuthenticationService authenticationService, IMvxMessenger messenger) : base(messenger)
        {
            _registerViewModel = registerViewModel;
            _authenticationService = authenticationService;
        }

        protected override async Task ExecuteCommandAction()
        {
            if (!(!string.IsNullOrEmpty(_registerViewModel.Email) && !string.IsNullOrEmpty(_registerViewModel.Login) &&
                !string.IsNullOrEmpty(_registerViewModel.Password) &&
                !string.IsNullOrEmpty(_registerViewModel.PasswordConfirmation)))
            {
                Application.Current.MainPage.DisplayAlert("Błąd", $"Wymagane wszystkie dane.", "OK");
                return;
            }

            if (!(_registerViewModel.Password == _registerViewModel.PasswordConfirmation))
            {
                Application.Current.MainPage.DisplayAlert("Błąd", $"Hasła nie pasują do siebie.", "OK");
                return;
            }
                
            var loginResult = await _authenticationService.Register(_registerViewModel.Login, _registerViewModel.Password, _registerViewModel.Email);
            if (loginResult.IsSuccess)
            {
                if (await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location) !=
                    PermissionStatus.Granted)
                {
                    var result = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    if (result[Permission.Location] != PermissionStatus.Granted)
                        return;
                }
                
                if (await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 5))
                    App.GeolocationWasStarted = true;

                await _registerViewModel.NavigationService.NavigateTo<MainViewModel, UserAuthResponse>(loginResult.Results);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Błąd", $"{loginResult.FormattedErrorMessages}", "OK");
            }
        }
    }
}