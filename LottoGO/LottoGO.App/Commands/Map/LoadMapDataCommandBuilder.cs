﻿using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace LottoGO.App.Commands.Map
{
    public class LoadMapDataCommandBuilder : AsyncGuardedCommandBuilder
    {
        private readonly MapViewModel _mapViewModel;
        private readonly IMapPinsService _mapPinsService;

        public LoadMapDataCommandBuilder(MapViewModel mapViewModel, IMapPinsService mapPinsService, IMvxMessenger messenger) : base(messenger)
        {
            _mapViewModel = mapViewModel;
            _mapPinsService = mapPinsService;
        }

        protected override async Task ExecuteCommandAction()
        {
            var result = await _mapPinsService.GetMapPins();
            if (result.IsSuccess)
            {
                _mapViewModel.Items.Clear();
                foreach (var pin in result.Results)
                {
                    _mapViewModel.Items.Add(pin);
                }
            }

            var position = await CrossGeolocator.Current.GetPositionAsync();
            _mapViewModel.CurrentPosition = new Xamarin.Forms.Maps.Position(position.Latitude, position.Longitude);
            _mapViewModel.SetClosestLocationCommand.Execute(null);
        }
    }
}