﻿using System;
using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using Xamarin.Forms;

namespace LottoGO.App.Commands.Map
{
    public class PinTappedCommandBuilder : AsyncGuardedCommandBuilder<LottoGoPin>
    {
        private const string Yes = "Tak";
        private const string No = "Nie";

        private readonly MapViewModel _mapViewModel;
        private readonly IMapPinsService _mapPinsService;

        public PinTappedCommandBuilder(MapViewModel mapViewModel, IMapPinsService mapPinsService, IMvxMessenger messenger) : base(messenger)
        {
            _mapViewModel = mapViewModel;
            _mapPinsService = mapPinsService;
        }

        protected override async Task ExecuteCommandAction(LottoGoPin item)
        {
            await Application.Current.MainPage.DisplayAlert("Info", $"To jest gra typu: {item.GameType.ToString()}", "OK");
        }
    }
}