﻿using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Messenger.Base;
using LottoGO.App.ViewModels;

namespace LottoGO.App.Commands.Map
{
    public class StartTaskCommandBuilder : AsyncGuardedCommandBuilder
    {
        private readonly MapViewModel _mapViewModel;

        public StartTaskCommandBuilder(MapViewModel mapViewModel, IMvxMessenger messenger) : base(messenger)
        {
            _mapViewModel = mapViewModel;
        }

        protected override async Task ExecuteCommandAction()
        {
            await _mapViewModel.NavigationService.NavigateModalTo<GameViewModel, string>(string.Empty);
        }
    }
}