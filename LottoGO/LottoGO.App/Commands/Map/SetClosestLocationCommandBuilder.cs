﻿using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Extensions;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services;
using LottoGO.App.ViewModels;
using Xamarin.Forms.Maps;

namespace LottoGO.App.Commands.Map
{
    public class SetClosestLocationCommandBuilder : AsyncGuardedCommandBuilder
    {
        private readonly MapViewModel _mapViewModel;

        public SetClosestLocationCommandBuilder(MapViewModel mapViewModel, IMvxMessenger messenger) : base(messenger)
        {
            _mapViewModel = mapViewModel;
        }

        protected override Task ExecuteCommandAction()
        {
            LottoGoPin closestPin = null;
            double closestMeters = double.MaxValue;
            const int maxDistance = 500000;
            
            foreach (var pin in _mapViewModel.Items)
            {
                var distance = pin.Position.DistanceTo(_mapViewModel.CurrentPosition);
                if (distance < maxDistance && distance < closestMeters)
                {
                    _mapViewModel.ClosestPin = pin;
                    closestMeters = distance;
                }
            }
            
            return Task.FromResult(true);
        }
    }
}