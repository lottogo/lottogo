﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Messenger.Messages.Base;
using Xamarin.Forms;

namespace LottoGO.App.Commands.Base
{
	public abstract class AsyncGuardedCommandBuilder
	{
		private readonly IMvxMessenger _messenger;
		private readonly Queue<Action> _invokeAfterCommandExecutedActionQueue = new Queue<Action>();
		private bool _commandBeingExecuted;

		public AsyncGuardedCommandBuilder(IMvxMessenger messenger)
		{
			_messenger = messenger;
		}

		public Command BuildCommand()
		{
			Command command = null;

			command = new Command(async () =>
			{
				try
				{
					if (ShouldNotifyAboutProgress)
					{
						_messenger.Publish(LongRunningAsyncOperationMessage
							.BuildOperationStartedMessage(this));
						_commandBeingExecuted = true;
						command.ChangeCanExecute();
					}

					await ExecuteCommandAction(); //.ConfigureAwait(false);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex);
				}
				finally
				{
					if (ShouldNotifyAboutProgress)
					{
						_messenger.Publish(LongRunningAsyncOperationMessage.BuildOperationFinishedMessage(this));
						_commandBeingExecuted = false;
						command.ChangeCanExecute();
					}

					while (_invokeAfterCommandExecutedActionQueue.Any())
						_invokeAfterCommandExecutedActionQueue.Dequeue().Invoke();
				}
			}, () =>
			{
				if (!ShouldNotifyAboutProgress)
					return true;
				return !_commandBeingExecuted;
			});

			return command;
		}

		protected virtual bool ShouldNotifyAboutProgress { get; } = true;

		protected abstract Task ExecuteCommandAction();

		protected void EnqueueAfterCommandExecuted(Action actionToInvoke)
			=> _invokeAfterCommandExecutedActionQueue.Enqueue(actionToInvoke);

	}

	public abstract class AsyncGuardedCommandBuilder<TItem>
	{
		private readonly IMvxMessenger _messenger;
		private readonly Queue<Action> _invokeAfterCommandExecutedActionQueue = new Queue<Action>();
		private bool _commandBeingExecuted;

		public AsyncGuardedCommandBuilder(IMvxMessenger messenger)
		{
			_messenger = messenger;
		}

		public Command<TItem> BuildCommand()
		{
			Command<TItem> command = null;

			command = new Command<TItem>(async (model) =>
			{
				try
				{
					if (ShouldNotifyAboutProgress)
					{
						_messenger.Publish(LongRunningAsyncOperationMessage.BuildOperationStartedMessage(this));
						_commandBeingExecuted = true;
						command.ChangeCanExecute();
					}

					await ExecuteCommandAction(model);//.ConfigureAwait(false);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex);
				}
				finally
				{
					if (ShouldNotifyAboutProgress)
					{
						_messenger.Publish(LongRunningAsyncOperationMessage.BuildOperationFinishedMessage(this));
						_commandBeingExecuted = false;
						command.ChangeCanExecute();
					}

					while (_invokeAfterCommandExecutedActionQueue.Any())
						_invokeAfterCommandExecutedActionQueue.Dequeue().Invoke();
				}
			}, (i) =>
			{
				if (!ShouldNotifyAboutProgress)
					return true;
				return !_commandBeingExecuted;
			});

			return command;
		}
		protected virtual bool ShouldNotifyAboutProgress { get; } = true;
		protected abstract Task ExecuteCommandAction(TItem item);

		protected void EnqueueAfterCommandExecuted(Action actionToInvoke)
			=> _invokeAfterCommandExecutedActionQueue.Enqueue(actionToInvoke);

	}
}
