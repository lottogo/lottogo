﻿using System;
using System.Threading.Tasks;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.Commands.Base
{
	public class NavigateToViewModelCommandBuilder<TViewModel, TParameter> : AsyncGuardedCommandBuilder where TViewModel : class, IBaseViewModel<TParameter> where TParameter : class
	{
		private readonly INavigationService _navigationService;
		private readonly Func<TParameter> _payloadProvider;

		public NavigateToViewModelCommandBuilder(INavigationService navigationService, Func<TParameter> payloadProvider,
			IMvxMessenger messanger) : base(messanger)
		{
			_navigationService = navigationService;
			_payloadProvider = payloadProvider;
		}

		protected override async Task ExecuteCommandAction()
		{
			var payload = _payloadProvider?.Invoke();
			await _navigationService.NavigateTo<TViewModel, TParameter>(payload);
		}
	}
}