﻿using System.Threading.Tasks;
using LottoGO.App.Commands.Base;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Models;
using LottoGO.App.ViewModels;

namespace LottoGO.App.Commands.Master
{
	public class MasterItemTappedCommandBuilder : AsyncGuardedCommandBuilder<IMasterMenuItem>
	{
		private readonly MasterViewModel _masterViewModel;

		public MasterItemTappedCommandBuilder(MasterViewModel masterViewModel, IMvxMessenger messenger) :
			base(messenger)
		{
			_masterViewModel = masterViewModel;
		}

		protected override async Task ExecuteCommandAction(IMasterMenuItem item)
		{
			switch (item)
			{
				case IViewModelItem viewModelItem:
					await _masterViewModel.NavigationService.NavigateTo(viewModelItem.ViewModelType, string.Empty);
					break;

				case MasterMenuCommandItem commandItem:
					commandItem.Command?.Execute(null);
					break;
			}

			_masterViewModel.MenuCommand.Execute(null);
		}
	}
}