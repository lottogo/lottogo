﻿namespace LottoGO.App.Views.Base
{
	public interface IShadowElement
	{
		bool HasShadow { get; set; }
	}
}