﻿using LottoGO.App.Configuration.Ioc.Contracts;
using LottoGO.App.Models;
using LottoGO.App.Services;
using LottoGO.App.ViewModels.Base;
using Xamarin.Forms;

namespace LottoGO.App.Views.Base
{
    public class MasterDetailPageBase<TOwnViewModel, TOwnParameter, TMasterViewModel, TMasterParameter, TDetailViewModel, TDetailParameter> : MasterDetailPage, IBasePage<TOwnViewModel, TOwnParameter> 
        where TOwnViewModel : class, IBaseViewModel<TOwnParameter> where TOwnParameter : class
        where TMasterViewModel : class, IBaseViewModel<TMasterParameter> where TMasterParameter : class
        where TDetailViewModel : class, IBaseViewModel<TDetailParameter> where TDetailParameter : class
    {
	    private readonly IViewAndViewModelResolver _resolver;
	    private TOwnViewModel _viewModel;

	    public MasterDetailPageBase(IViewAndViewModelResolver resolver)
	    {
		    _resolver = resolver;
		    _resolver.InjectMasterPresenterAction(() => IsPresented = !IsPresented);
		    CreatePages();
			NavigationPage.SetHasNavigationBar(this, false);
	    }
        
        private void CreatePages()
        {
            var resolvedMaster = _resolver.ResolveViewModelAndPage<TMasterViewModel, TMasterParameter>();
            resolvedMaster.Item1.Prepare(default(TMasterParameter));
            this.Master = resolvedMaster.Item2 as Page;
	        if (resolvedMaster.Item1 is IMasterMenuInvocator menuInvocator)
	        {
		        menuInvocator.MenuStateChanger = () => IsPresented = !IsPresented;
	        }

			var resolvedAbout = _resolver.ResolveViewModelAndPage<TDetailViewModel, TDetailParameter>();
            resolvedAbout.Item1.Prepare(default(TDetailParameter));
            this.Detail = FormsNavigationService.GetPageBasedOnConfiguration(resolvedAbout.Item2);
        }

        IBaseViewModel IBasePage.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (TOwnViewModel)value;
        }

        public TOwnViewModel ViewModel
        {
	        get => _viewModel;
	        set
	        {
		        _viewModel = value;
		        OnViewModelSet();
	        }
        }

        protected virtual void OnViewModelSet()
        {
        }

        public bool IsNavigationPage => true;
        
        public bool IsNavigationBarVisible => false;

	    public Orientation RequestedOrientation => Orientation.Portrait;

	    protected override bool OnBackButtonPressed()
	    {
		    var backResult = base.OnBackButtonPressed();

			if (Detail is NavigationPage navi && navi.StackDepth == 2)
		    {
			    IsGestureEnabled = true;
		    }

		    return backResult;
	    }
    }
}