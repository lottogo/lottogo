﻿using LottoGO.App.Models;
using Xamarin.Forms;

namespace LottoGO.App.Views.Base
{
	public class OrientationNavigationPage : NavigationPage
	{
		public OrientationNavigationPage(Page page) : base(page)
		{
			NavigationPage.SetHasNavigationBar(this, false);
			if (page is IBasePage basePage)
			{
				Orientation = basePage.RequestedOrientation;
			}
		}
		
		public Orientation Orientation { get; set; }
	}
}