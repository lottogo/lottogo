﻿using LottoGO.App.Models;
using LottoGO.App.ViewModels;
using LottoGO.App.ViewModels.Base;
using LottoGO.App.Views.Base;

namespace LottoGO.App.Views
{
    public partial class MasterPage : IBasePage<MasterViewModel, string>
    {
        private MasterViewModel _viewModel;

        public MasterPage()
        {
            InitializeComponent();
        }

        public MasterViewModel ViewModel
        {
            get => _viewModel;
            set
            {
                _viewModel = value;
                BindingContext = value;
            }
        }

        IBaseViewModel IBasePage.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MasterViewModel)value;
        }

        public bool IsNavigationPage => false;

        public bool IsNavigationBarVisible => false;
	    
	    public Orientation RequestedOrientation => Orientation.Portrait;
    }
}