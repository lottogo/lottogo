﻿using LottoGO.App.ViewModels;
using LottoGO.App.Views.Base;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GamePage : BasePage<GameViewModel, string>
	{
		public GamePage()
		{
			InitializeComponent();
		}
	}
}