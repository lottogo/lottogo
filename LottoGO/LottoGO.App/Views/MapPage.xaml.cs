﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using BulletSharp;
using LottoGO.App.Services;
using LottoGO.App.ViewModels;
using LottoGO.App.Views.Base;
using LottoGO.App.Views.CustomViews;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : BasePage<MapViewModel, string>
	{
		public MapPage()
		{
			InitializeComponent ();
		}

	    public override bool IsNavigationPage => true;
	}

	public class LottoMap : Map
	{
		private INotifyCollectionChanged _collection;

		public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
			nameof(ItemsSource),
			typeof(IEnumerable),
			typeof(LottoMap),
			null,
			propertyChanged: ItemsSourcePropertyChanged);

		public static readonly BindableProperty CurrentPositionProperty = BindableProperty.Create(
			nameof(CurrentPosition),
			typeof(Position),
			typeof(LottoMap),
			null,
			propertyChanged: CurrentPositionPropertyChanged);

		public static readonly BindableProperty PinTappedCommandProperty = BindableProperty.Create(
			nameof(PinTappedCommand),
			typeof(ICommand),
			typeof(LottoMap),
			null);

		private Position _latestPosition;

		private static void CurrentPositionPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var lottoMap = (LottoMap) bindable;
			if (newvalue is Position position && !position.Equals(lottoMap._latestPosition))
			{
				lottoMap._latestPosition = position;
				lottoMap?.MoveToRegion(new MapSpan(position, 0.1, 0.1));
			}
		}

		private static void ItemsSourcePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var lottoMap = (LottoMap) bindable;

			if (newvalue is INotifyCollectionChanged collectionChanged)
			{
				lottoMap._collection = collectionChanged;
				lottoMap._collection.CollectionChanged += lottoMap.ItemsSourceOnCollectionChanged;
			}
		}

		private void ItemsSourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			var newItems = e.NewItems;
			var oldItems = e.OldItems;

			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Add:
					Pins.Add((Pin) newItems[0]);
					break;
				case NotifyCollectionChangedAction.Move:
					break;
				case NotifyCollectionChangedAction.Remove:
					Pins.Remove((Pin) oldItems[0]);
					break;
				case NotifyCollectionChangedAction.Replace:
					Pins.Clear();
					foreach (var item in newItems)
					{
						Pins.Add((Pin) item);
					}

					break;
				case NotifyCollectionChangedAction.Reset:
					Pins.Clear();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public IEnumerable ItemsSource
		{
			get => (IEnumerable) GetValue(ItemsSourceProperty);
			set => SetValue(ItemsSourceProperty, value);
		}

		public Position CurrentPosition
		{
			get => (Position) GetValue(CurrentPositionProperty);
			set => SetValue(CurrentPositionProperty, value);
		}
		
		public ICommand PinTappedCommand
		{
			get => (ICommand) GetValue(PinTappedCommandProperty);
			set => SetValue(PinTappedCommandProperty, value);
		}
	}
}