﻿using LottoGO.App.Configuration.Ioc.Contracts;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using LottoGO.App.ViewModels.Base;
using LottoGO.App.Views.Base;

namespace LottoGO.App.Views
{
    public partial class MainPage : MasterDetailPageBase<MainViewModel, UserAuthResponse, MasterViewModel, string, MapViewModel, string>
    {
        public MainPage(IViewAndViewModelResolver resolver) : base(resolver)
        {
            InitializeComponent();
	        //IsGestureEnabled = false;
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            
            if (((OrientationNavigationPage)Detail).RootPage.BindingContext is IAuthDataInjector authDataInjector)
            {
                authDataInjector.Inject(ViewModel?.UserAuth);
            }
        }
    }
}