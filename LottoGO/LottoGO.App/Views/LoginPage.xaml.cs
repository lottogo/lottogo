﻿using System;
using System.Collections.Generic;
using LottoGO.App.ViewModels;
using LottoGO.App.Views.Base;
using Xamarin.Forms;

namespace LottoGO.App.Views
{
    public partial class LoginPage : BasePage<LoginViewModel, string>
    {
        public LoginPage()
        {
            InitializeComponent();
        }
    }
}
