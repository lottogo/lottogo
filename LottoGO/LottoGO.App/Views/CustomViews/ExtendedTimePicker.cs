﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
	public class ExtendedTimePicker : TimePicker
	{
		public static readonly BindableProperty MinimumTimeProperty = BindableProperty.Create(
			nameof(MinimumTime),
			typeof(DateTime),
			typeof(ExtendedTimePicker),
			DateTime.MinValue,
			BindingMode.TwoWay);

		public static readonly BindableProperty TimeDateProperty = BindableProperty.Create(
			nameof(TimeDate), 
			typeof(DateTime),
			typeof(ExtendedTimePicker), 
			DateTime.UtcNow, 
			BindingMode.TwoWay,
			propertyChanged: DateTimePropertyChanged);

		public ExtendedTimePicker()
		{
			PropertyChanged += OnPropertyChanged;
		}

		private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == TimeProperty.PropertyName)
			{
				TimeDate = new DateTime(TimeDate.Year, TimeDate.Month, TimeDate.Day, Time.Hours, Time.Minutes, Time.Seconds);
			}
		}

		private static void DateTimePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var picker = bindable as ExtendedTimePicker;
			picker.Time = new TimeSpan(picker.TimeDate.Hour, picker.TimeDate.Minute, picker.TimeDate.Second);
		}

		public DateTime TimeDate
		{
			get => (DateTime)GetValue(TimeDateProperty);
			set => SetValue(TimeDateProperty, value);
		}

		public DateTime MinimumTime
		{
			get => (DateTime)GetValue(MinimumTimeProperty);
			set => SetValue(MinimumTimeProperty, value);
		}
	}

	public class ExtendedDatePicker : DatePicker
	{
		public static readonly BindableProperty TimeDateProperty = BindableProperty.Create(
			nameof(TimeDate),
			typeof(DateTime),
			typeof(ExtendedDatePicker),
			DateTime.UtcNow,
			BindingMode.TwoWay,
			propertyChanged: DateTimePropertyChanged);

		public ExtendedDatePicker()
		{
			PropertyChanged += OnPropertyChanged;
		}

		private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == DateProperty.PropertyName)
			{
				TimeDate = new DateTime(Date.Year, Date.Month, Date.Day, TimeDate.Hour, TimeDate.Minute, TimeDate.Second);
			}
		}

		private static void DateTimePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var picker = bindable as ExtendedDatePicker;
			picker.Date = new DateTime(picker.TimeDate.Year, picker.TimeDate.Month, picker.TimeDate.Day, picker.TimeDate.Hour, picker.TimeDate.Minute, picker.TimeDate.Second);
		}

		public DateTime TimeDate
		{
			get => (DateTime)GetValue(TimeDateProperty);
			set => SetValue(TimeDateProperty, value);
		}
	}
}