﻿using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
    public class ExtendedUnderlineLabel : Label
    {
	    public static readonly BindableProperty IsUnderlinedProperty = BindableProperty.Create(
		    nameof(IsUnderlined),
		    typeof(bool),
		    typeof(ExtendedLabel),
		    false);

	    public bool IsUnderlined
	    {
		    get => (bool)GetValue(IsUnderlinedProperty);
		    set => SetValue(IsUnderlinedProperty, value);
	    }
	}
}
