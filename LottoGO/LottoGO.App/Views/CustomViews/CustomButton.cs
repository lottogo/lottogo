﻿using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
    public class CustomButton : Button
    {
	    public static BindableProperty HorizontalTextAlignmentProperty =
		    BindableProperty.Create<CustomButton, TextAlignment>(x => x.HorizontalTextAlignment, TextAlignment.Center);

		public TextAlignment HorizontalTextAlignment
	    {
		    get => (TextAlignment)GetValue(HorizontalTextAlignmentProperty);
		    set => SetValue(HorizontalTextAlignmentProperty, value);
	    }
	}
}
