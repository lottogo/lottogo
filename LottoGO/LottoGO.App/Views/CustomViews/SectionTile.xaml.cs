﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SectionTile : BaseContentView
	{
		public SectionTile ()
		{
			InitializeComponent ();
		}

		public ImageSource ImageSource
		{
			get => (ImageSource)GetValue(ImageSourceProperty);
			set => SetValue(ImageSourceProperty, value);
		}

		public string Title
		{
			get => (string)GetValue(TitleProperty);
			set => SetValue(TitleProperty, value);
		}

		public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(
			nameof(ImageSource),
			typeof(ImageSource),
			typeof(SectionTile),
			null,
			propertyChanged: ImageSourcePropertyChanged);

		public static readonly BindableProperty TitleProperty = BindableProperty.Create(
			nameof(Title),
			typeof(string),
			typeof(SectionTile),
			string.Empty,
			propertyChanged: TitlePropertyChanged);

		private static void ImageSourcePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var sectionTile = bindable as SectionTile;
			sectionTile.Image.Source = newvalue as ImageSource;
		}

		private static void TitlePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var sectionTile = bindable as SectionTile;
			sectionTile.TitleLabel.Text = newvalue as string;
		}
	}
}