﻿using LottoGO.App.Views.Base;
using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
	public class ExtendedStackLayout : StackLayout, IShadowElement
	{
		public static readonly BindableProperty HasShadowProperty = BindableProperty.Create(
			nameof(HasShadow),
			typeof(bool),
			typeof(BaseContentView),
			true);

		public bool HasShadow
		{
			get => (bool)GetValue(HasShadowProperty);
			set => SetValue(HasShadowProperty, value);
		}
	}
}