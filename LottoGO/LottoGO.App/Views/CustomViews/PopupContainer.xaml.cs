﻿using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views.CustomViews
{
	[ContentProperty("PopupContent")]
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupContainer : ContentView
	{
		public static PopupContainer VisiblePopup { get; private set; }

		public static readonly BindableProperty OpenPopupCommandProperty = BindableProperty.Create(
			nameof(OpenPopupCommand),
			typeof(ICommand),
			typeof(PopupContainer),
			null,
			BindingMode.OneWayToSource);

		public static readonly BindableProperty ClosePopupCommandProperty = BindableProperty.Create(
			nameof(ClosePopupCommand),
			typeof(ICommand),
			typeof(PopupContainer),
			null,
			BindingMode.OneWayToSource);

		public PopupContainer ()
		{
			InitializeComponent ();
			OpenPopupCommand = new Command(OpenPopup);
			ClosePopupCommand = new Command(ClosePopup);
		}

		public ICommand OpenPopupCommand
		{
			get => (ICommand)GetValue(OpenPopupCommandProperty);
			set => SetValue(OpenPopupCommandProperty, value);
		}

		public ICommand ClosePopupCommand
		{
			get => (ICommand)GetValue(ClosePopupCommandProperty);
			set => SetValue(ClosePopupCommandProperty, value);
		}

		public View PopupContent
		{
			get => PopupFrame.Content;
			set => PopupFrame.Content = value;
		}

		private void ClosePopup()
		{
			if (!IsVisible) return;
			this.Animate("alpha", callback: d => { this.Opacity = d; }, start: 1, end: 0, finished: (d, b) =>
			{
				VisiblePopup = null;
				IsVisible = false;
			});
		}

		private void OpenPopup()
		{
			if (IsVisible) return;
			VisiblePopup = this;
			IsVisible = true;
			this.Animate("alpha", callback: d => { this.Opacity = d; }, start: 0, end: 1);
		}
	}
}