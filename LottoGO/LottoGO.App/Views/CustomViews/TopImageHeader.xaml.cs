﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TopImageHeader : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(
			nameof(Title),
			typeof(string),
			typeof(TopImageHeader),
			string.Empty,
			propertyChanged: TitlePropertyChanged);

		public static readonly BindableProperty TextProperty = BindableProperty.Create(
			nameof(Text),
			typeof(string),
			typeof(TopImageHeader),
			string.Empty,
			propertyChanged: TextPropertyChanged);

		public static readonly BindableProperty TitleFontAttributesProperty = BindableProperty.Create(
			nameof(TitleFontAttributes),
			typeof(FontAttributes),
			typeof(TopImageHeader),
			FontAttributes.Bold,
			propertyChanged: TitleFontPropertyChanged);

		private static void TitleFontPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopImageHeader;
			topTitleHeaderView.TitleLabel.FontAttributes= (FontAttributes)newvalue;
		}

		private static void TextFonAttributestPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopImageHeader;
			topTitleHeaderView.TextLabel.FontAttributes = (FontAttributes)newvalue;
		}

		public static readonly BindableProperty TextFontAttributesProperty = BindableProperty.Create(
			nameof(TextFontAttributes),
			typeof(FontAttributes),
			typeof(TopImageHeader),
			FontAttributes.None,
			propertyChanged: TextFonAttributestPropertyChanged);

		public static readonly BindableProperty TextVisibleProperty = BindableProperty.Create(
			nameof(TextVisible),
			typeof(string),
			typeof(TopImageHeader),
			string.Empty,
			propertyChanged: TextVisiblePropertyChanged);

		public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(
			nameof(ImageSource),
			typeof(string),
			typeof(TopImageHeader),
			string.Empty,
			propertyChanged: ImageSourcePropertyChanged);

		public static readonly BindableProperty ButtonTextProperty = BindableProperty.Create(
			nameof(ButtonText),
			typeof(string),
			typeof(TopImageHeader),
			string.Empty,
			propertyChanged: ButtonTextPropertyChanged);

		public static readonly BindableProperty ButtonVisibleProperty = BindableProperty.Create(
			nameof(ButtonVisible),
			typeof(string),
			typeof(TopImageHeader),
			string.Empty,
			propertyChanged: ButtonVisiblePropertyChanged);

		public static readonly BindableProperty ButtonCommandProperty = BindableProperty.Create(
			nameof(ButtonCommand),
			typeof(ICommand),
			typeof(TopImageHeader),
			null,
			propertyChanged: ButtonCommandPropertyChanged);

		public static readonly BindableProperty TitleFontSizeProperty = BindableProperty.Create(
			nameof(TitleFontSize),
			typeof(double),
			typeof(TopImageHeader),
			15.0,
			propertyChanged: TitleFontSizePropertyChanged);

		public static readonly BindableProperty TextFontSizeProperty = BindableProperty.Create(
			nameof(TextFontSize),
			typeof(double),
			typeof(TopImageHeader),
			22.0,
			propertyChanged: TextFontSizePropertyChanged);

		public static readonly BindableProperty TitleColorProperty = BindableProperty.Create(
			nameof(TitleColor),
			typeof(Color),
			typeof(TopImageHeader),
			Color.Black,
			propertyChanged: TitleColorPropertyChanged);

		public static readonly BindableProperty TextColorProperty = BindableProperty.Create(
			nameof(TextColor),
			typeof(Color),
			typeof(TopImageHeader),
			Color.Black,
			propertyChanged: TextColorPropertyChanged);

		public static readonly BindableProperty AbbLineColorProperty = BindableProperty.Create(
			nameof(AbbLineColor),
			typeof(Color),
			typeof(TopImageHeader),
			Color.Red,
			propertyChanged: AbbLineColorPropertyChanged);

		private static void AbbLineColorPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopImageHeader;
			topTitleHeaderView.AbbLine.Color = (Color)newvalue;
		}

		private static void TextColorPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopImageHeader;
			topTitleHeaderView.TextLabel.TextColor = (Color)newvalue;
		}

		private static void TitleColorPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopImageHeader;
			topTitleHeaderView.TitleLabel.TextColor = (Color)newvalue;
		}

		private static void TitlePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.TitleLabel.Text = newvalue as string;
		}
		private static void TextPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.TextLabel.Text = newvalue as string;
		}
		private static void TextVisiblePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.TextLabel.IsVisible = Convert.ToBoolean(newvalue);
		}
		private static void ImageSourcePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.Image.Source = newvalue as string;
		}
		private static void ButtonTextPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.Button.Text = newvalue as string;
		}
		private static void ButtonVisiblePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.Button.IsVisible = Convert.ToBoolean(newvalue);
		}
		private static void ButtonCommandPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.Button.Command = newvalue as ICommand;
		}

		private static void TextFontSizePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.TextLabel.FontSize = Convert.ToDouble(newvalue);
		}

		private static void TitleFontSizePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topImageHeader = bindable as TopImageHeader;
			topImageHeader.TitleLabel.FontSize = Convert.ToDouble(newvalue);
		}

		public string Title
		{
			get => (string)GetValue(TitleProperty);
			set => SetValue(TitleProperty, value);
		}
		public string Text
		{
			get => (string)GetValue(TextProperty);
			set => SetValue(TextProperty, value);
		}
		public string TextVisible
		{
			get => (string)GetValue(TextVisibleProperty);
			set => SetValue(TextVisibleProperty, value);
		}
		public string ImageSource
		{
			get => (string)GetValue(ImageSourceProperty);
			set => SetValue(ImageSourceProperty, value);
		}
		public string ButtonText
		{
			get => (string)GetValue(ButtonTextProperty);
			set => SetValue(ButtonTextProperty, value);
		}
		public string ButtonVisible
		{
			get => (string)GetValue(ButtonVisibleProperty);
			set => SetValue(ButtonVisibleProperty, value);
		}
		public double TextFontSize
		{
			get => (double)GetValue(TextFontSizeProperty);
			set => SetValue(TextFontSizeProperty, value);
		}

		public double TitleFontSize
		{
			get => (double)GetValue(TitleFontSizeProperty);
			set => SetValue(TitleFontSizeProperty, value);
		}

		public ICommand ButtonCommand
		{
			get => (ICommand)GetValue(ButtonCommandProperty);
			set => SetValue(ButtonCommandProperty, value);
		}

		public TopImageHeader()
		{
			InitializeComponent();
		}

		public Color TitleColor
		{
			get => (Color)GetValue(TitleColorProperty);
			set => SetValue(TitleColorProperty, value);
		}

		public Color TextColor
		{
			get => (Color)GetValue(TextColorProperty);
			set => SetValue(TextColorProperty, value);
		}

		public Color AbbLineColor
		{
			get => (Color)GetValue(AbbLineColorProperty);
			set => SetValue(AbbLineColorProperty, value);
		}

		public FontAttributes TitleFontAttributes
		{
			get => (FontAttributes)GetValue(TitleFontAttributesProperty);
			set => SetValue(TitleFontAttributesProperty, value);
		}

		public FontAttributes TextFontAttributes
		{
			get => (FontAttributes)GetValue(TextFontAttributesProperty);
			set => SetValue(TextFontAttributesProperty, value);
		}
	}
}