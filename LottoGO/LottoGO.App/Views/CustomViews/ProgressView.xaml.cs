﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProgressView : ContentView
	{
		public static readonly BindableProperty TextProperty = BindableProperty.Create(
			nameof(Text),
			typeof(string),
			typeof(ProgressView),
			string.Empty,
			propertyChanged: (bindable, value, newValue) => ((ProgressView)bindable).LabelText.Text = (string)newValue);

		public static readonly BindableProperty ProgressProperty = BindableProperty.Create(
			nameof(Progress),
			typeof(float),
			typeof(ProgressView),
			(float)0,
			propertyChanged: (bindable, value, newValue) => ((ProgressView)bindable).ProgressBar.Progress = (float)newValue);

		public ProgressView ()
		{
			InitializeComponent ();
		}

		public string Text
		{
			get => (string)GetValue(TextProperty);
			set => SetValue(TextProperty, value);
		}

		public float Progress
		{
			get => (float)GetValue(ProgressProperty);
			set => SetValue(ProgressProperty, value);
		}
	}
}