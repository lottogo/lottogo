﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace LottoGO.App.Views.CustomViews
{
	public class PinchToZoomContainer : ContentView
	{
		private const double MinScale = 1;
		private const double MaxScale = 4;
		private double _startScale, _currentScale;
		private double _startX, _startY;
		private double _xOffset, _yOffset;

		public PinchToZoomContainer()
		{
			var pinchGesture = new PinchGestureRecognizer();
			pinchGesture.PinchUpdated += OnPinchUpdated;
			GestureRecognizers.Add(pinchGesture);

			var pan = new PanGestureRecognizer();
			pan.PanUpdated += OnPanUpdated;
			GestureRecognizers.Add(pan);

			TapGestureRecognizer tap = new TapGestureRecognizer { NumberOfTapsRequired = 2 };
			tap.Tapped += OnTapped;
			GestureRecognizers.Add(tap);

			Scale = MinScale;
			TranslationX = TranslationY = 0;
			AnchorX = AnchorY = 0;
		}

		private void OnTapped(object sender, EventArgs e)
		{
			if (Content.Scale > MinScale)
			{
				RestoreScaleValues();
			}
			else
			{
				Content.AnchorX = Content.AnchorY = 0.5;
				Content.ScaleTo(MaxScale, 250, Easing.CubicInOut);
			}
		}
		void RestoreScaleValues()
		{
			Content.ScaleTo(MinScale, 250, Easing.CubicInOut);
			Content.TranslateTo(0.5, 0.5, 250, Easing.CubicInOut);

			_currentScale = 1;

			Content.TranslationX = 0.5;
			Content.TranslationY = 0.5;

			_xOffset = Content.TranslationX;
			_yOffset = Content.TranslationY;
		}

		void OnPinchUpdated(object sender, PinchGestureUpdatedEventArgs e)
		{
			if (e.Status == GestureStatus.Started)
			{
				_startScale = Content.Scale;
				Content.AnchorX = 0;
				Content.AnchorY = 0;
			}
			if (e.Status == GestureStatus.Running)
			{
				// Calculate the scale factor to be applied.
				_currentScale += (e.Scale - 1) * _startScale;
				_currentScale = Math.Max(1, _currentScale);

				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the X pixel coordinate.
				double renderedX = Content.X + _xOffset;
				double deltaX = renderedX / Width;
				double deltaWidth = Width / (Content.Width * _startScale);
				double originX = (e.ScaleOrigin.X - deltaX) * deltaWidth;

				// The ScaleOrigin is in relative coordinates to the wrapped user interface element,
				// so get the Y pixel coordinate.
				double renderedY = Content.Y + _yOffset;
				double deltaY = renderedY / Height;
				double deltaHeight = Height / (Content.Height * _startScale);
				double originY = (e.ScaleOrigin.Y - deltaY) * deltaHeight;

				// Calculate the transformed element pixel coordinates.
				double targetX = _xOffset - (originX * Content.Width) * (_currentScale - _startScale);
				double targetY = _yOffset - (originY * Content.Height) * (_currentScale - _startScale);

				// Apply translation based on the change in origin.
				Content.TranslationX = targetX.Clamp(-Content.Width * (_currentScale - 1), 0);
				Content.TranslationY = targetY.Clamp(-Content.Height * (_currentScale - 1), 0);

				// Apply scale factor.
				Content.Scale = _currentScale;
			}
			if (e.Status == GestureStatus.Completed)
			{
				// Store the translation delta's of the wrapped user interface element.
				_xOffset = Content.TranslationX;
				_yOffset = Content.TranslationY;
			}
		}

		void OnPanUpdated(object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType)
			{
				case GestureStatus.Started:
					_startX = e.TotalX;
					_startY = e.TotalY;
					Content.AnchorX = 0;
					Content.AnchorY = 0;
					break;

				case GestureStatus.Running:
					var maxTranslationX = Content.Scale * Content.Width - Content.Width;
					Content.TranslationX = Math.Min(0, Math.Max(-maxTranslationX, _xOffset + e.TotalX - _startX));

					var maxTranslationY = Content.Scale * Content.Height - Content.Height;
					Content.TranslationY = Math.Min(0, Math.Max(-maxTranslationY, _yOffset + e.TotalY - _startY));

					break;

				case GestureStatus.Completed:
					_xOffset = Content.TranslationX;
					_yOffset = Content.TranslationY;
					break;
			}
		}
	}
}