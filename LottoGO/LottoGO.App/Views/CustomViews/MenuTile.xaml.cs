﻿using System.Windows.Input;
using LottoGO.App.Views.Base;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views.CustomViews
{
	public class BaseContentView : ContentView, IShadowElement
	{
		public static readonly BindableProperty HasShadowProperty = BindableProperty.Create(
			nameof(HasShadow),
			typeof(bool),
			typeof(BaseContentView),
			false);
		
		public bool HasShadow
		{
			get => (bool)GetValue(HasShadowProperty);
			set => SetValue(HasShadowProperty, value);
		}
	}
	
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuTile : BaseContentView
	{
		public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create(
			nameof(IsSelected),
			typeof(bool),
			typeof(MenuTile),
			false,
			propertyChanged: IsSelectedPropertyChanged);

		private static void IsSelectedPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var menuTile = bindable as MenuTile;
			var isSelected = bool.Parse(newvalue.ToString());
			menuTile.BackgroundButton.BackgroundColor = isSelected ? Color.Red : Color.White;
			menuTile.TextLabel.TextColor = isSelected ? Color.White : Color.Black;
			menuTile.TitleLabel.TextColor = isSelected ? Color.White : Color.Black;
			menuTile.Image.TintColor = isSelected ? Color.White : Color.Black;
			menuTile.BackgroundButton.CornerRadius = 0;
		}

		public static readonly BindableProperty TextProperty = BindableProperty.Create(
			nameof(Text),
			typeof(string),
			typeof(MenuTile),
			string.Empty,
			propertyChanged: TextPropertyChanged);

		public static readonly BindableProperty TitleProperty = BindableProperty.Create(
	        nameof(Title),
	        typeof(string),
	        typeof(MenuTile),
	        string.Empty,
	        propertyChanged: TitlePropertyChanged);
		

	    public static readonly BindableProperty ImageSourceProperty = BindableProperty.Create(
	        nameof(ImageSource),
	        typeof(ImageSource),
	        typeof(MenuTile),
	        null,
	        propertyChanged: ImageSourcePropertyChanged);

		public static readonly BindableProperty TileCommandProperty = BindableProperty.Create(
			nameof(TileCommand),
			typeof(ICommand),
			typeof(MenuTile),
			null,
			propertyChanged: (bindable, value, newValue) => ((MenuTile)bindable).BackgroundButton.Command = (ICommand)newValue);



		private static void ImageSourcePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
	        var menuTile = bindable as MenuTile;
            menuTile.Image.Source = (ImageSource)newvalue;
        }

        private static void TextPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
	        var menuTile = bindable as MenuTile;
	        menuTile.TextLabel.Text = newvalue as string;
	    }

		private static void TitlePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var menuTile = bindable as MenuTile;
			menuTile.TitleLabel.Text = newvalue as string;
		}

		public MenuTile ()
		{
			InitializeComponent ();
		}

		public bool IsSelected
		{
			get => (bool)GetValue(IsSelectedProperty);
			set => SetValue(IsSelectedProperty, value);
		}

		public string Text
	    {
	        get => (string)GetValue(TextProperty);
	        set => SetValue(TextProperty, value);
	    }

		public string Title
		{
			get => (string)GetValue(TitleProperty);
			set => SetValue(TitleProperty, value);
		}

		public ImageSource ImageSource
	    {
	        get => (ImageSource)GetValue(ImageSourceProperty);
	        set => SetValue(ImageSourceProperty, value);
	    }

		public ICommand TileCommand
		{
			get => (ICommand)GetValue(TileCommandProperty);
			set => SetValue(TileCommandProperty, value);
		}
	}
}