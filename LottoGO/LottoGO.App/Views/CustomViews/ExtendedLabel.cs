﻿using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
	public class ExtendedLabel : Label
	{
		public static readonly BindableProperty ShouldRenderAsHtmlProperty = BindableProperty.Create(
			nameof(ShouldRenderAsHtml),
			typeof(bool),
			typeof(ExtendedLabel),
			false);

		public static readonly BindableProperty LinesCountProperty = BindableProperty.Create(
			nameof(LinesCount),
			typeof(int),
			typeof(ExtendedLabel),
			-1);

		public bool ShouldRenderAsHtml
		{
			get => (bool)GetValue(ShouldRenderAsHtmlProperty);
			set => SetValue(ShouldRenderAsHtmlProperty, value);
		}

		public int LinesCount
		{
			get => (int)GetValue(LinesCountProperty);
			set => SetValue(LinesCountProperty, value);
		}
	}
}