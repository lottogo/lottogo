﻿using System.Windows.Input;
using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
	[ContentProperty("NavigationBarContent")]
	public partial class ExtendedNavigationBarView : BaseContentView
    {
	    public static readonly BindableProperty TitleProperty = BindableProperty.Create(
		    nameof(Title),
		    typeof(string),
		    typeof(ExtendedNavigationBarView),
		    string.Empty,
		    propertyChanged: TitlePropertyChanged);

	    public static readonly BindableProperty LeftImageSourceProperty = BindableProperty.Create(
		    nameof(LeftImageSource),
		    typeof(ImageSource),
		    typeof(ExtendedNavigationBarView),
		    null,
		    propertyChanged: LeftButtonImageSourceChanged);
	    
	    public static readonly BindableProperty ImageCommandProperty = BindableProperty.Create(
		    nameof(ImageCommand),
		    typeof(ICommand),
		    typeof(ExtendedNavigationBarView),
		    null,
		    propertyChanged: ImageCommandPropertyChanged);

	    private static void ImageCommandPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
		    var navigationBarView = bindable as ExtendedNavigationBarView;
		    navigationBarView.BaseNavigationBar.ImageCommand = (ICommand)newvalue;
	    }

	    private static void LeftButtonImageSourceChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
		    var navigationBarView = bindable as ExtendedNavigationBarView;
			navigationBarView.BaseNavigationBar.LeftImage.Source = (ImageSource)newvalue; 
	    }

	    private static void TitlePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
		    var navigationBarView = bindable as ExtendedNavigationBarView;
			navigationBarView.BaseNavigationBar.TitleLabel.Text = newvalue as string;
	    }

	    public ExtendedNavigationBarView ()
	    {
		    InitializeComponent ();
	    }

	    public string Title
	    {
		    get => (string)GetValue(TitleProperty);
		    set => SetValue(TitleProperty, value);
	    }

	    public ImageSource LeftImageSource
	    {
		    get => (ImageSource)GetValue(LeftImageSourceProperty);
		    set => SetValue(LeftImageSourceProperty, value);
	    }
	    
	    public ICommand ImageCommand
	    {
		    get => (ICommand)GetValue(ImageCommandProperty);
		    set => SetValue(ImageCommandProperty, value);
	    }
	    
	    public View NavigationBarContent
	    {
		    get => ContentFrame.Content;
		    set => ContentFrame.Content = value;
	    }
    }
}
