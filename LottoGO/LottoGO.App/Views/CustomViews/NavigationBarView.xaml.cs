﻿using System;
using System.Windows.Input;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
	public partial class NavigationBarView : BaseContentView
    {
	    public static readonly BindableProperty HasTopSpacingProperty = BindableProperty.Create(
		    nameof(HasTopSpacing),
		    typeof(bool),
		    typeof(NavigationBarView),
		    true,
		    propertyChanged: HasTopSpacingPropertyChanged);

	    private static void HasTopSpacingPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
		    var naviBar = bindable as NavigationBarView;
		    var value = bool.Parse(newvalue.ToString());
			
		    switch (Device.RuntimePlatform)
			{
				case Device.iOS:
					naviBar.HeightRequest = value ? 65 : 45;
					naviBar.FirstRow.Height = new GridLength(value ? 20 : 0);
					break;
				case Device.Android:
					break;
			}
	    }

	    public static readonly BindableProperty TitleProperty = BindableProperty.Create(
		    nameof(Title),
		    typeof(string),
		    typeof(NavigationBarView),
		    string.Empty,
		    propertyChanged: TitlePropertyChanged);
	    
	    public static readonly BindableProperty RightTextProperty = BindableProperty.Create(
		    nameof(RightText),
		    typeof(string),
		    typeof(NavigationBarView),
		    string.Empty,
		    propertyChanged: LabelRightPropertyChanged);

	    public static readonly BindableProperty LeftImageSourceProperty = BindableProperty.Create(
		    nameof(LeftImageSource),
		    typeof(ImageSource),
		    typeof(NavigationBarView),
		    null,
		    propertyChanged: LeftButtonImageSourceChanged);

		public static readonly BindableProperty ImageCommandProperty = BindableProperty.Create(
			nameof(ImageCommand),
			typeof(ICommand),
			typeof(NavigationBarView),
			null,
			propertyChanged: ImageCommandPropertyChanged);

		public static readonly BindableProperty EventContextMenuCommandProperty = BindableProperty.Create(
		 nameof(EventContextMenuCommand),
		 typeof(ICommand),
		 typeof(NavigationBarView),
		 null,
		 propertyChanged: EventContextMenuCommandPropertyChanged);

		public static readonly BindableProperty ContextMenuIsVisibleProperty = BindableProperty.Create(
			nameof(ContextMenuIsVisible),
			typeof(bool),
			typeof(NavigationBarView),
			false,
			propertyChanged: ContextMenuIsVisibilePropertyChanged);

		private static void ImageCommandPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var navigationBarView = bindable as NavigationBarView;
			navigationBarView.LeftButton.Command = (ICommand)newvalue;
		}

		private static void LeftButtonImageSourceChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
		    var navigationBarView = bindable as NavigationBarView;
		    //TODO source
		    navigationBarView.Image.Source = (ImageSource)newvalue; 
	    }

	    private static void TitlePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
		    var navigationBarView = bindable as NavigationBarView;
		    navigationBarView.LabelTitle.Text = newvalue as string;
	    }
	    
	    private static void LabelRightPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
	    {
		    var navigationBarView = bindable as NavigationBarView;
		    navigationBarView.LabelRight.Text = newvalue as string;
	    }

		private static void EventContextMenuCommandPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var navigationBarView = bindable as NavigationBarView;
			//navigationBarView.EventContextMenuButton.Command = (ICommand)newvalue;
		}

		private static void ContextMenuIsVisibilePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var navigationBarView = bindable as NavigationBarView;
//			navigationBarView.ContextMenu.IsVisible = Convert.ToBoolean(newvalue);
		}

		public NavigationBarView ()
	    {
		    InitializeComponent ();
		}

		public string Title
	    {
		    get => (string)GetValue(TitleProperty);
		    set => SetValue(TitleProperty, value);
	    }
		
		public string RightText
		{
			get => (string)GetValue(RightTextProperty);
			set => SetValue(RightTextProperty, value);
		}

	    public ImageSource LeftImageSource
	    {
		    get => (ImageSource)GetValue(LeftImageSourceProperty);
		    set => SetValue(LeftImageSourceProperty, value);
	    }

		public ICommand ImageCommand
		{
			get => (ICommand)GetValue(ImageCommandProperty);
			set => SetValue(ImageCommandProperty, value);
		}

		public ICommand EventContextMenuCommand
		{
			get => (ICommand)GetValue(EventContextMenuCommandProperty);
			set => SetValue(EventContextMenuCommandProperty, value);
		}

		public bool ContextMenuIsVisible
		{
			get => (bool)GetValue(ContextMenuIsVisibleProperty);
			set => SetValue(ContextMenuIsVisibleProperty, value);
		}

		public CachedImage LeftImage => Image;

	    public Label TitleLabel => LabelTitle;

		public new static readonly BindableProperty HasShadowProperty = BindableProperty.Create(
		    nameof(HasShadow),
		    typeof(bool),
		    typeof(BaseContentView),
		    false,
			propertyChanged: (bindable, value, newValue) =>
			{
				var mainGrid = ((NavigationBarView) bindable).MainGrid;
				if (mainGrid == null) return;
				mainGrid.HasShadow = bool.Parse(newValue.ToString());
			});

	    public new bool HasShadow
	    {
		    get => (bool)GetValue(HasShadowProperty);
		    set => SetValue(HasShadowProperty, value);
	    }
	    
	    public bool HasTopSpacing
	    {
		    get => (bool)GetValue(HasTopSpacingProperty);
		    set => SetValue(HasTopSpacingProperty, value);
	    }
	}
}
