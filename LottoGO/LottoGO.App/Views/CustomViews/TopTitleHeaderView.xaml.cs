﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LottoGO.App.Views.CustomViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TopTitleHeaderView : ContentView
	{
		public static readonly BindableProperty TitleProperty = BindableProperty.Create(
			nameof(Title),
			typeof(string),
			typeof(TopTitleHeaderView),
			string.Empty,
			propertyChanged: TitlePropertyChanged);

		public static readonly BindableProperty TitleColorProperty = BindableProperty.Create(
			nameof(TitleColor),
			typeof(Color),
			typeof(TopTitleHeaderView),
			Color.Black,
			propertyChanged: TitleColorPropertyChanged);

		public static readonly BindableProperty TextColorProperty = BindableProperty.Create(
			nameof(TextColor),
			typeof(Color),
			typeof(TopTitleHeaderView),
			Color.Black,
			propertyChanged: TextColorPropertyChanged);

		public static readonly BindableProperty AbbLineColorProperty = BindableProperty.Create(
			nameof(AbbLineColor),
			typeof(Color),
			typeof(TopTitleHeaderView),
			Color.Red,
			propertyChanged: AbbLineColorPropertyChanged);

		private static void AbbLineColorPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopTitleHeaderView;
			topTitleHeaderView.AbbLine.Color = (Color)newvalue;
		}

		private static void TextColorPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopTitleHeaderView;
			topTitleHeaderView.TextLabel.TextColor = (Color)newvalue;
		}

		private static void TitleColorPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopTitleHeaderView;
			topTitleHeaderView.TitleLabel.TextColor = (Color)newvalue;
		}

		public static readonly BindableProperty TextProperty = BindableProperty.Create(
			nameof(Text),
			typeof(string),
			typeof(TopTitleHeaderView),
			string.Empty,
			propertyChanged: TextPropertyChanged);

		public static readonly BindableProperty TextVisibleProperty = BindableProperty.Create(
			nameof(TextVisible),
			typeof(bool),
			typeof(TopTitleHeaderView),
			true,
			propertyChanged: TextVisiblePropertyChanged);

		public static readonly BindableProperty TextFontSizeProperty = BindableProperty.Create(
			nameof(TextFontSize),
			typeof(double),
			typeof(TopTitleHeaderView),
			20.0,
			propertyChanged: TextFontSizePropertyChanged);

		private static void TextFontSizePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopTitleHeaderView;
			topTitleHeaderView.TextLabel.FontSize = (double)newvalue;
		}

		private static void TitlePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopTitleHeaderView;
			topTitleHeaderView.TitleLabel.Text = newvalue as string;
		}
		private static void TextVisiblePropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopTitleHeaderView;
			topTitleHeaderView.TextLabel.IsVisible = Convert.ToBoolean(newvalue);
		}
		private static void TextPropertyChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var topTitleHeaderView = bindable as TopTitleHeaderView;
			topTitleHeaderView.TextLabel.Text = newvalue as string;
		}

		public string Title
		{
			get => (string) GetValue(TitleProperty);
			set => SetValue(TitleProperty, value);
		}

		public Color TitleColor
		{
			get => (Color)GetValue(TitleColorProperty);
			set => SetValue(TitleColorProperty, value);
		}

		public Color TextColor
		{
			get => (Color)GetValue(TextColorProperty);
			set => SetValue(TextColorProperty, value);
		}

		public Color AbbLineColor
		{
			get => (Color)GetValue(AbbLineColorProperty);
			set => SetValue(AbbLineColorProperty, value);
		}

		public bool TextVisible
		{
			get => (bool)GetValue(TextVisibleProperty);
			set => SetValue(TextVisibleProperty, value);
		}
		public string Text
		{
			get => (string)GetValue(TextProperty);
			set => SetValue(TextProperty, value);
		}

		public double TextFontSize
		{
			get => (double)GetValue(TextFontSizeProperty);
			set => SetValue(TextFontSizeProperty, value);
		}

		public TopTitleHeaderView ()
		{
			InitializeComponent ();
		}
	}
}