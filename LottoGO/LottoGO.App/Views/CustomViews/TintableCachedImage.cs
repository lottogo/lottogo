﻿using FFImageLoading.Forms;
using Xamarin.Forms;

namespace LottoGO.App.Views.CustomViews
{
	public class TintableCachedImage : CachedImage
	{
		public static readonly BindableProperty TintColorProperty = BindableProperty.Create(
			nameof(TintColor),
			typeof(Color),
			typeof(TintableCachedImage),
			Color.Default);

		public Color TintColor
		{
			get => (Color)GetValue(TintColorProperty);
			set => SetValue(TintColorProperty, value);
		}
	}
}