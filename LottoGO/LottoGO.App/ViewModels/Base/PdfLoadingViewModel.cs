﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using LottoGO.App.Helpers;
using LottoGO.App.Models;
using LottoGO.App.Services.Contracts.Contracts;
using Xamarin.Forms;

namespace LottoGO.App.ViewModels.Base
{
	public abstract class PdfLoadingViewModel<TParameter>: BaseViewModel<TParameter>
	{
		private readonly ICacheFileDownloadService _cacheFileDownloadService;
		protected readonly Progress<float> PdfLoadingProgressListener;
		protected CancellationTokenSource DetailsTokenSource;

		public PdfLoadingViewModel(ICacheFileDownloadService cacheFileDownloadService)
		{
			_cacheFileDownloadService = cacheFileDownloadService;
			PdfLoadingProgressListener = new Progress<float>();
			PdfLoadingProgressListener.ProgressChanged += PdfLoadingProgressListenerOnProgressChanged;
		}

		private void PdfLoadingProgressListenerOnProgressChanged(object sender, float e)
		{
			PdfLoadingProgressPercent = (int)(e * 100);
			Debug.WriteLine(PdfLoadingProgressPercent);
		}

		public override void Prepare(TParameter parameter)
		{
			base.Prepare(parameter);
			Data = parameter;
			LoadNewData();
		}

		public async void LoadNewData()
		{
			DetailsTokenSource = new CancellationTokenSource();
			await GetPdfData();
		}

		public async Task GetPdfData()
		{
			try
			{
				if (!FileData.FileUrl.Contains("http"))
				{
					var stream = CrossHelper.GetResourceStream(FileData.FileUrl);
					DetailsStream = stream;
					PdfLoadingProgressPercent = 100;
					return;
				}

				var file = await _cacheFileDownloadService.GetFileFromCacheOrDownload(FileData, PdfLoadingProgressListener,
					DetailsTokenSource.Token);

				DetailsStream = file;
				PdfLoadingProgressPercent = 100;
			}
			catch (TaskCanceledException)
			{
				CloseCommand?.Execute(null);
			}
			catch (OperationCanceledException)
			{
				CloseCommand?.Execute(null);
			}
			catch (Exception e)
			{
				Debug.WriteLine(e);
				if (!e.Message.ToLower().Contains("canceled"))
					await Application.Current.MainPage.DisplayAlert("There is no PDF to be loaded", e.Message, "OK");
				CloseCommand?.Execute(null);
			}
		}

		private int _pdfLoadingProgressPercent;
		public int PdfLoadingProgressPercent
		{
			get => _pdfLoadingProgressPercent;
			set
			{
				if (SetProperty(ref _pdfLoadingProgressPercent, value))
					OnPropertyChanged(nameof(ShouldDisplayProgress));
			}
		}

		public bool ShouldDisplayProgress => PdfLoadingProgressPercent < 100;

		private TParameter _data;
		public TParameter Data
		{
			get => _data;
			set => SetProperty(ref _data, value);
		}

		public abstract IFileData FileData { get; }

		private Stream _detailsStream;
		public Stream DetailsStream
		{
			get => _detailsStream;
			set
			{
				if (_detailsStream is FileStream fs)
					fs.Close();
				SetProperty(ref _detailsStream, value);
			}
		}
		
		public ICommand GoToPageCommand { get; set; }

		public ICommand ErrorOccuredCommand => new Command<string>(async s =>
			{
				await Application.Current.MainPage.DisplayAlert("Error", s, "OK");
				await _cacheFileDownloadService.RemoveFileFromCache(FileData);
				CloseCommand?.Execute(null);
			});

		public override void OnDisappearing()
		{
			base.OnDisappearing();
			DetailsTokenSource?.Cancel(false);
			DetailsStream?.Close();
		}
	}
}