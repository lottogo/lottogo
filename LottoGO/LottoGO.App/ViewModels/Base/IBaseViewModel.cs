﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;

namespace LottoGO.App.ViewModels.Base
{
    public interface IBaseViewModel : INotifyPropertyChanged, IMasterMenuInvocator
    {
        bool IsBusy { get; set; }
        string Title { get; set; }
        void OnAppearing();
        void OnDisappearing();
	    ICommand CloseCommand { get; }
	    IMvxMessenger Messanger { get; }
    }

	public interface IPreparableViewModel
	{
		void Prepare(object parameter);
	}

	public interface IMasterMenuInvocator
	{
		Action MenuStateChanger { get; set; }
	}

	public interface IBaseViewModel<TNavigationParameter> : IBaseViewModel
    {
        void Prepare(TNavigationParameter parameter);
    }

    public interface IDetailViewModel : IAuthDataInjector
    {
    }

    public interface IAuthDataInjector
    {
	    void Inject(UserAuthResponse authResponse);
    }
}