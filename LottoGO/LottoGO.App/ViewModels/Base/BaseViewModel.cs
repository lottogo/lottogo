﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using LottoGO.App.Helpers;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Messenger.Observers.Base;
using LottoGO.App.Services.Contracts;
using Xamarin.Forms;

namespace LottoGO.App.ViewModels.Base
{
	public abstract class BaseViewModel : Bindable, IBaseViewModel, IMessengerInjector, INavigationServiceInjector, IDisposingView
	{
		private bool _isBusy;
		private string _title = string.Empty;
		private bool _alreadyDisappeared;
		protected readonly List<IMessageObserver> Observers = new List<IMessageObserver>();
		private UserAuthResponse _userAuth;

		public INavigationService NavigationService { get; private set; }

		public ICommand CloseCommand => new Command(Close);

		public UserAuthResponse UserAuth
		{
			get => _userAuth;
			protected set => SetProperty(ref _userAuth, value);
		}

		protected virtual void Close()
		{
			if (_alreadyDisappeared) return;
			NavigationService.Close();
		}

		protected virtual IEnumerable<IMessageObserver> RegisterObservers()
		{
			return Enumerable.Empty<IMessageObserver>();
		}

		public ICommand MenuCommand => new Command(() => ((IMasterMenuInvocator) this)?.MenuStateChanger?.Invoke());

		public string AccountBalance => $"{Settings.AuthData?.AccountBalance?.Amount} {Settings.AuthData?.AccountBalance?.Currency}";
		
		public bool IsBusy
		{
			get => _isBusy;
			set => SetProperty(ref _isBusy, value);
		}

		public string Title
		{
			get => _title;
			set => SetProperty(ref _title, value);
		}

		public virtual void OnAppearing()
		{
			((IDisposingView)this).IsDisposing = true;
			foreach (var messageObserver in Observers)
			{
				messageObserver.Start(Messanger);
			}

			_alreadyDisappeared = false;
		}

		public virtual void OnDisappearing()
		{
			foreach (var messageObserver in Observers)
			{
				if (((IDisposingView)this).IsDisposing || messageObserver.ShouldStopOnDisappearing())
					messageObserver.Stop();
			}

			_alreadyDisappeared = true;
		}

		Action IMasterMenuInvocator.MenuStateChanger { get; set; }
		
		void INavigationServiceInjector.Inject(INavigationService navigationService)
		{
			NavigationService = navigationService;
		}

		void IMessengerInjector.Inject(IMvxMessenger messenger)
		{
			Messanger = messenger;
		}

		bool IDisposingView.IsDisposing { get; set; }

		public IMvxMessenger Messanger { get; private set; }
	}

	public interface IDisposingView
	{
		bool IsDisposing { get; set; }
	}

	public interface IMessengerInjector
	{
		void Inject(IMvxMessenger messenger);
	}

	public interface INavigationServiceInjector
	{
		void Inject(INavigationService navigationService);
	}

	public abstract class BaseViewModel<TParameter> : BaseViewModel, IBaseViewModel<TParameter>, IPreparableViewModel
	{
		public virtual void Prepare(TParameter parameter)
		{
			Observers.AddRange(RegisterObservers());
		}

		void IPreparableViewModel.Prepare(object parameter)
		{
			Prepare((TParameter) parameter);
		}
	}
}