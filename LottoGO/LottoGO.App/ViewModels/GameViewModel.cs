﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LottoGO.App.ViewModels.Base;
using WaveEngine.Framework;
using LottoGO;
using LottoGO.App.Services.Contracts;

namespace LottoGO.App.ViewModels
{
	public class GameViewModel : BaseViewModel<string>, IDetailViewModel
    {
	    private  Game _game;

		public static GameViewModel Instance { get; private set; }

	    public GameViewModel()
	    {
		    Instance = this;
	    }

	    public Game Game
	    {
		    get { return _game; }
		    set { SetProperty(ref _game, value); }
	    }

	    public async void NotifyGameLost()
	    {
		    await Task.Delay(3000);
		    DisplayInfo(false);
	    }

	    public async void NotifyGameWon()
	    {
		    await Task.Delay(3000);
		    DisplayInfo(true);
	    }

		public override async void OnAppearing()
	    {
		    base.OnAppearing();
			Game = new Game();
	    }

	    private GameInfoData ObtainInfoData(bool hasWon)
	    {
		    return new GameInfoData
		    {
			    Title = hasWon ? "Wygrana" : "Przegrana",
			    Message = hasWon ? "Dobrze Ci poszło, zwyciężyłeś!" : "No cóż, spróbuj szczęścia ponownie",
			    ImageUri = hasWon ? "win.png" : "lose.png"
		    };
	    }
	    
	    private async void DisplayInfo(bool hasWon)
	    {
		    await NavigationService.Close();
		    await NavigationService.NavigateModalTo<GameInfoViewModel, GameInfoData>(ObtainInfoData(hasWon));
	    }

	    void IAuthDataInjector.Inject(UserAuthResponse authResponse)
	    {
		    UserAuth = authResponse;
	    }
    }

    public class GameInfoViewModel : BaseViewModel<GameInfoData>
    {
	    private GameInfoData _data;

	    public GameInfoData Data
	    {
		    get => _data;
		    private set => SetProperty(ref _data, value);
	    }

	    public override void Prepare(GameInfoData parameter)
	    {
		    base.Prepare(parameter);
		    Data = parameter;
	    }

	    public override async void OnAppearing()
	    {
		    base.OnAppearing();
		    await Task.Delay(3000);
		    Close();
	    }
    }

    public class GameInfoData
    {
	    public string Title { get; set; }

	    public string Message { get; set; }

	    public string ImageUri { get; set; }
    }
}
