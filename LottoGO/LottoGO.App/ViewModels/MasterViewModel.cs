﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using LottoGO.App.Commands.Master;
using LottoGO.App.Extensions;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Models;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.ViewModels
{
    public class MasterViewModel : BaseViewModel<string>
    {
	    private readonly IMasterMenuDataService _masterMenuDataService;
	    private readonly IMvxMessenger _messenger;

	    public MasterViewModel(IMasterMenuDataService masterMenuDataService, IMvxMessenger messenger)
	    {
		    _masterMenuDataService = masterMenuDataService;
		    _messenger = messenger;
	    }

        public ObservableCollection<IMasterMenuItem> MenuCollection { get; } = new ObservableCollection<IMasterMenuItem>();

	    public ICommand MasterItemTappedCommand =>
		    new MasterItemTappedCommandBuilder(this, _messenger).BuildCommand();

		public override void Prepare(string parameter)
	    {
		    base.Prepare(parameter);
			MenuCollection.AddRange(_masterMenuDataService.GetMasterItemsForUser());
	    }
    }
}