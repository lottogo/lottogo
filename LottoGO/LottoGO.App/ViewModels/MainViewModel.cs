﻿using LottoGO.App.Messenger.Base;
using LottoGO.App.Messenger.Messages;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Position = Xamarin.Forms.Maps.Position;

namespace LottoGO.App.ViewModels
{
    public class MainViewModel : BaseViewModel<UserAuthResponse>, IRootViewModel
    {
	    private readonly IMvxMessenger _messenger;

	    public MainViewModel(IMvxMessenger messenger)
	    {
		    _messenger = messenger;
		    CrossGeolocator.Current.PositionChanged += CurrentOnPositionChanged;
	    }

        private void CurrentOnPositionChanged(object sender, PositionEventArgs e)
        {
	        _messenger.Publish(new CurrentLocationChangedMessage(this, new Position(e.Position.Latitude, e.Position.Longitude)));
        }

        public override void Prepare(UserAuthResponse parameter)
	    {
		    base.Prepare(parameter);
		    UserAuth = parameter;
	    }
    }

    public interface IRootViewModel
	{
	}
}