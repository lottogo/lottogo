﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using LottoGO.App.Commands.History;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.ViewModels
{
    public class HistoryViewModel : BaseViewModel<string>, IDetailViewModel
    {
        private readonly IGameHistoryService _gameHistoryService;
        private readonly IMvxMessenger _messenger;

        public HistoryViewModel(IGameHistoryService gameHistoryService, IMvxMessenger messenger)
        {
            _gameHistoryService = gameHistoryService;
            _messenger = messenger;
            Items = new ObservableCollection<GameHistory>();
            Items.CollectionChanged += (sender, args) => OnPropertyChanged(nameof(HasAnyItems));
        }
	    
        public ObservableCollection<GameHistory> Items { get; }

        public bool HasAnyItems => Items.Any();

        public ICommand LoadGameHistoryDataCommand =>
            new LoadGameHistoryDataCommandBuilder(this, _gameHistoryService, _messenger).BuildCommand();

        public override void Prepare(string parameter)
        {
            base.Prepare(parameter);
            LoadGameHistoryDataCommand.Execute(null);
        }

        public void Inject(UserAuthResponse authResponse)
        {
            UserAuth = authResponse;
        }
    }
}