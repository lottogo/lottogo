﻿using System.Windows.Input;
using LottoGO.App.Commands.Base;
using LottoGO.App.Commands.Login;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.ViewModels
{
    public class LoginViewModel : BaseViewModel<string>, IRootViewModel
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IMvxMessenger _messenger;
        private string _login = "Immons";
        private string _password = "test1234";

        public LoginViewModel(IAuthenticationService authenticationService, IMvxMessenger messenger)
        {
            _authenticationService = authenticationService;
            _messenger = messenger;
        }
		
        public string Login
        {
            get => _login;
            set => SetProperty(ref _login, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public ICommand LoginCommand => new LoginCommandBuilder(this, _authenticationService, _messenger).BuildCommand();

        public ICommand RegisterCommand =>
            new NavigateToViewModelCommandBuilder<RegisterViewModel, string>(NavigationService, () => string.Empty, _messenger).BuildCommand();
    }
}