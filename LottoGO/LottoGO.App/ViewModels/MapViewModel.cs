﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using LottoGO.App.Commands.Map;
using LottoGO.App.Helpers;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Messenger.Observers;
using LottoGO.App.Messenger.Observers.Base;
using LottoGO.App.Services;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Position = Xamarin.Forms.Maps.Position;

namespace LottoGO.App.ViewModels
{
	public class MapViewModel : BaseViewModel<string>, IDetailViewModel
    {
	    private readonly IMvxMessenger _messenger;
	    private readonly IMapPinsService _mapPinsService;
	    private decimal _moneyEarned;
	    private bool _canStartTask;
	    private Position _currentPosition;
	    private LottoGoPin _closestPin;

	    public MapViewModel(IMvxMessenger messenger, IMapPinsService mapPinsService)
	    {
		    _messenger = messenger;
		    _mapPinsService = mapPinsService;
		    Items = new ObservableCollection<LottoGoPin>();
	    }

	    public Position CurrentPosition
	    {
		    get => _currentPosition;
		    set => SetProperty(ref _currentPosition, value);
	    }

	    public LottoGoPin ClosestPin
	    {
		    get => _closestPin;
		    set
		    {
			    SetProperty(ref _closestPin, value);
			    OnPropertyChanged(nameof(CanStartTask));
		    }
	    }

	    public bool CanStartTask => ClosestPin != null;

	    public ICommand StartTaskCommand => new StartTaskCommandBuilder(this, _messenger).BuildCommand();

	    public ICommand LoadMapData => new LoadMapDataCommandBuilder(this, _mapPinsService, _messenger).BuildCommand();

	    public ICommand PinTappedCommand => new PinTappedCommandBuilder(this, _mapPinsService, _messenger).BuildCommand();

	    public ICommand SetClosestLocationCommand =>
		    new SetClosestLocationCommandBuilder(this, _messenger).BuildCommand();

	    public ObservableCollection<LottoGoPin> Items { get; }

	    public override void Prepare(string parameter)
	    {
		    base.Prepare(parameter);
		    LoadMapData.Execute(null);
	    }

	    protected override IEnumerable<IMessageObserver> RegisterObservers()
	    {
		    yield return new CurrentLocationChangedMessageObserver(this);
	    }

	    void IAuthDataInjector.Inject(UserAuthResponse authResponse)
	    {
		    if (authResponse != null)
				UserAuth = authResponse;
	    }
    }
}