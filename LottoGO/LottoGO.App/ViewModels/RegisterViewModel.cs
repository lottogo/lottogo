﻿using System.Windows.Input;
using LottoGO.App.Commands.Login;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.ViewModels
{
    public class RegisterViewModel : BaseViewModel<string>
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IMvxMessenger _messenger;
        private string _login;
        private string _email;
        private string _password;
        private string _passwordConfirmation;

        public RegisterViewModel(IAuthenticationService authenticationService, IMvxMessenger messenger)
        {
            _authenticationService = authenticationService;
            _messenger = messenger;
        }
		
        public string Login
        {
            get => _login;
            set => SetProperty(ref _login, value);
        }
        
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }
        
        public string PasswordConfirmation
        {
            get => _passwordConfirmation;
            set => SetProperty(ref _passwordConfirmation, value);
        }

        public ICommand RegisterCommand => new RegisterCommandBuilder(this, _authenticationService, _messenger).BuildCommand();
    }
}