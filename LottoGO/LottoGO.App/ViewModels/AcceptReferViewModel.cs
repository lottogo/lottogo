﻿using System.Windows.Input;
using LottoGO.App.Commands.Login;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.ViewModels
{
    public class AcceptReferViewModel : BaseViewModel<string>, IDetailViewModel
    {
        private readonly IReferralService _referralService;
        private readonly IMvxMessenger _messenger;
        private string _code;

        public AcceptReferViewModel(IReferralService referralService, IMvxMessenger messenger)
        {
            _referralService = referralService;
            _messenger = messenger;
        }

        public string Code
        {
            get => _code;
            set => SetProperty(ref _code, value);
        }

        public ICommand UseCodeCommand =>
            new UseReferralCodeCommandBuilder(this, _referralService, _messenger).BuildCommand();

        void IAuthDataInjector.Inject(UserAuthResponse authResponse)
        {
            UserAuth = authResponse;
        }
    }
}