﻿using System;
using System.Windows.Input;
using LottoGO.App.Commands.Base;
using LottoGO.App.Commands.Map;
using LottoGO.App.Helpers;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.ViewModels
{
    public class ReferFriendViewModel : BaseViewModel<string>, IDetailViewModel
    {
	    private readonly IMvxMessenger _messenger;

	    public ReferFriendViewModel(IMvxMessenger messenger)
	    {
		    _messenger = messenger;
	    }
	    
	    public string ReferalCode => Settings.AuthData?.ReferralCode;

	    public ICommand FinishCommand =>
		    new NavigateToViewModelCommandBuilder<MapViewModel, string>(NavigationService, () => string.Empty, _messenger)
			    .BuildCommand();

	    void IAuthDataInjector.Inject(UserAuthResponse authResponse)
	    {
		    UserAuth = authResponse;
	    }
    }

    public class GameHistory : Bindable
    {
	    public int GameId { get; set; }
	    public bool IsWon { get; set; }
	    public Money Prize { get; set; }
	    public DateTime DateTime { get; set; }
    }
}