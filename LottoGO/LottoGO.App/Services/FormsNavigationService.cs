﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using LottoGO.App.Configuration.Ioc.Contracts;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using LottoGO.App.ViewModels.Base;
using LottoGO.App.Views.Base;
using Xamarin.Forms;

namespace LottoGO.App.Services
{
	public class FormsNavigationService : INavigationService
    {
	    private readonly IViewAndViewModelResolver _viewAndViewModelResolver;

	    public FormsNavigationService(IViewAndViewModelResolver viewAndViewModelResolver)
	    {
		    _viewAndViewModelResolver = viewAndViewModelResolver;
	    }
	    
        private INavigation FormsNavigation => Application.Current.MainPage.Navigation;
        
        public async Task Close<TViewModel>(TViewModel viewModel) where TViewModel : IBaseViewModel
        {
            for (var i = FormsNavigation.NavigationStack.Count - 1; i >= 0; i--)
            {
                var page = FormsNavigation.NavigationStack[i];
                if (page is IBasePage basePage)
                {
                    if (basePage.ViewModel is TViewModel)
                    {
                        if (i == FormsNavigation.NavigationStack.Count)
                            await FormsNavigation.PopAsync(true);
                        else
                            FormsNavigation.RemovePage(page);

                        return;
                    }
                }
            }
        }

        public async Task Close()
        {
	        if (Application.Current.MainPage is OrientationNavigationPage orientationPage)
	        {
		        if (orientationPage.RootPage is MasterDetailPage masterDetailPage)
		        {
			        if (FormsNavigation.ModalStack.Count > 0)
				        await FormsNavigation.PopModalAsync();
			        else
			        {
				        var navi = (masterDetailPage.Detail as NavigationPage);
				        if (navi != null && orientationPage.StackDepth == 1)
				        {
					        masterDetailPage.IsGestureEnabled = true;
				        }

				        await orientationPage?.PopAsync(true);
			        }
		        }
                else
                    await FormsNavigation.PopAsync();
            }
	        else
		        await FormsNavigation.PopAsync();
		}

	    public async Task CloseToRoot()
	    {
		    if (Application.Current.MainPage is OrientationNavigationPage naviPage)
		    {
			    {
				    await naviPage.PopToRootAsync();
			    }
		    }
	    }

	    public async Task NavigateTo<T, TParameter>(TParameter parameter) where T : class, IBaseViewModel<TParameter> where TParameter : class
        {
	        IBasePage page = null;
	        
			try
			{
				var resolved = _viewAndViewModelResolver.ResolveViewModelAndPage<T, TParameter>();
				resolved.Item1.Prepare(parameter);
				page = resolved.Item2;
                page.ViewModel = resolved.Item1;

				await PushPage(page);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);

				try
				{
					await PushPage(page);
				}
				catch (Exception e)
				{
					Debug.WriteLine(e);
					throw;
				}
			}
        }

        private async Task PushPage(IBasePage page)        
        {
	        if (page.ViewModel is IRootViewModel)
	        {
				Application.Current.MainPage = GetPageBasedOnConfiguration(page);
		        return;
	        }

	        if (Application.Current.MainPage is OrientationNavigationPage naviPage)
	        {
		        if (naviPage.RootPage is MasterDetailPage masterDetailPage)
		        {
			        if (page.ViewModel is IAuthDataInjector authDataInjector)
			        {
				        var mainContext = Application.Current.MainPage.BindingContext as MainViewModel;
				        authDataInjector.Inject(mainContext?.UserAuth);
			        }
			        
			        if (page.ViewModel is IDetailViewModel)
			        {
				        masterDetailPage.Detail = GetPageBasedOnConfiguration(page);
				        masterDetailPage.IsPresented = false;
			        }
			        else
			        {
				        IBaseViewModel bindingContext = null;
				        if (naviPage.CurrentPage.BindingContext != null)
					        bindingContext = naviPage.CurrentPage.BindingContext as IBaseViewModel;
						else if (masterDetailPage.Detail?.BindingContext != null)
							bindingContext = masterDetailPage.Detail.BindingContext as IBaseViewModel;
						else if ((masterDetailPage.Detail as NavigationPage)?.CurrentPage?.BindingContext != null)
							bindingContext = (masterDetailPage.Detail as NavigationPage)?.CurrentPage?.BindingContext as IBaseViewModel;

						if (bindingContext != null)
							((IDisposingView)bindingContext).IsDisposing = false;

						await naviPage.PushAsync(page as Page);
			        }
		        }
		        else
		        {
					await FormsNavigation.PushAsync(GetPageBasedOnConfiguration(page));
		        }
	        }
	        else
		        await FormsNavigation.PushAsync(GetPageBasedOnConfiguration(page));
        }

        public async Task NavigateTo(Type viewModelType, object parameter)
        {
	        if (Application.Current.MainPage is OrientationNavigationPage naviPage)
	        {
		        if (naviPage.RootPage is MasterDetailPage masterDetailPage)
		        {
			        if (masterDetailPage.Detail is NavigationPage navi)
			        {
				        if (navi.RootPage.BindingContext.GetType() == viewModelType && navi.StackDepth == 1)
				        {
					        masterDetailPage.IsPresented = false;
					        return;
				        }
			        }
		        }
	        }

	        var viewModel = _viewAndViewModelResolver.ResolveViewModel(viewModelType);
            var page = (IBasePage)_viewAndViewModelResolver.GetFormsPage(viewModelType);
            page.ViewModel = viewModel;
			((IPreparableViewModel)viewModel).Prepare(parameter);
            await PushPage(page);
        }

        public async Task NavigateModalTo<T, TParameter>(TParameter parameter) where T : class, IBaseViewModel<TParameter> where TParameter : class 
        {
            var resolved = _viewAndViewModelResolver.ResolveViewModelAndPage<T, TParameter>();
            resolved.Item1.Prepare(parameter);
            await FormsNavigation.PushModalAsync(GetPageBasedOnConfiguration(resolved.Item2));
        }

        public static Page GetPageBasedOnConfiguration(IBasePage page)
        {
                return new OrientationNavigationPage(page as Page);
        }
    }
}