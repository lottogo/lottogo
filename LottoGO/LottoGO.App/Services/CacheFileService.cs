﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.Models;
using LottoGO.App.Services.Contracts.Contracts;

namespace LottoGO.App.Services
{
	public class CacheFileService : IFileService, ICacheFileDownloadService
	{
		private readonly IFileService _fileService;
		private HttpClient _client;

		private string GetPathToFile(string fileName) => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName);

		public CacheFileService(IFileService fileService)
		{
			_fileService = fileService;

			//TODO add auth handler
			_client = new HttpClient();
		}

		public Task SaveFile(string fileName, Stream file)
		{
			return _fileService.SaveFile(fileName, file);
		}

		public Task<FileStream> SaveFileAndKeepOpen(string fileName, Stream file)
		{
			return _fileService.SaveFileAndKeepOpen(fileName, file);
		}

		public Task<FileStream> LoadFile(string fileName)
		{
			var filePath = GetPathToFile(fileName);
			var nameOfFile = Path.GetFileName(filePath);
			var fileIdDirectoryPath = filePath.Replace(nameOfFile, "");

			if (File.Exists(filePath))
				return _fileService.LoadFile(fileName);

			if (Directory.Exists(fileIdDirectoryPath))
			{
				var filesToRemove = Directory.GetFiles(fileIdDirectoryPath);
				foreach (var s in filesToRemove)
				{
					File.Delete(s);
				}

				throw new FileNotFoundException(fileName);
			}
			else
			{
				Directory.CreateDirectory(fileIdDirectoryPath);
			}

			throw new FileNotFoundException(fileName);
		}

		public Task<FileStream> CreateOrOpenFileForWrite(string fileName)
		{
			return _fileService.CreateOrOpenFileForWrite(fileName);
		}

		public Task DeleteFile(string fileName)
		{
			return _fileService.DeleteFile(fileName);
		}

		public async Task<FileStream> GetFileFromCacheOrDownload(IFileData fileData, Progress<float> progressListener, CancellationToken cancellationToken = default(CancellationToken))
		{
			var fileName = Path.Combine($"{fileData.Id.ToString()}", $"{fileData.LastModifiedAt.ToUnixTimeSeconds()}.pdf");

			try
			{
				var stream = await LoadFile(fileName);
				if (stream != null)
				{
					return stream;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine($"Cached file not found for: {fileData.Id}. Downloading...");
			}

			try
			{
				var obtainedStream =
					await DownloadFileAndCache(fileData.FileUrl, fileName, progressListener, cancellationToken);
				return obtainedStream;
			}
			catch (Exception e)
			{
				Debug.WriteLine(e);
				throw;
			}
		}

		public Task RemoveFileFromCache(IFileData fileData)
		{
			var fileName = Path.Combine($"{fileData.Id.ToString()}", $"{fileData.LastModifiedAt.ToUnixTimeSeconds()}.pdf");
			var filePath = GetPathToFile(fileName);
			var nameOfFile = Path.GetFileName(filePath);

			if (File.Exists(filePath))
				return _fileService.DeleteFile(fileName);

			return Task.FromResult(true);
		}

		public async Task<FileStream> DownloadFileAndCache(string fileUrl, string fileName, Progress<float> progressListener = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			var openFile = await _fileService.CreateOrOpenFileForWrite(fileName);
			try
			{
				await _client.DownloadAsync(fileUrl, openFile, progressListener, cancellationToken);
				return openFile;
			}
			catch (Exception e)
			{
				openFile.Close();
				_fileService.DeleteFile(fileName);
				Debug.WriteLine(e);
				throw;
			}
		}
	}
}