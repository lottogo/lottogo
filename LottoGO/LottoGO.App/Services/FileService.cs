﻿using System;
using System.IO;
using System.Threading.Tasks;
using LottoGO.App.Services.Contracts.Contracts;

namespace LottoGO.App.Services
{

	public class FileService : IFileService
	{
		private string GetPathToFile(string fileName) => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName);

		public async Task SaveFile(string fileName, Stream file)
		{
			var stream = await Save(fileName, file);
			stream.Close();
		}

		public async Task<FileStream> SaveFileAndKeepOpen(string fileName, Stream file) => await Save(fileName, file);

		private async Task<FileStream> Save(string fileName, Stream file)
		{
			var path = GetPathToFile(fileName);
			if (File.Exists(path))
				File.Delete(path);

			var fileStream = File.Create(path);
			await file.CopyToAsync(fileStream);
			return fileStream;
		}

		public async Task<FileStream> LoadFile(string fileName)
		{
			var path = GetPathToFile(fileName);
			if (!File.Exists(path)) throw new FileNotFoundException(fileName);
			var opened = File.Open(path, FileMode.Open);
			if (opened.Length == 0)
			{
				opened.Close();
				throw new FileNotFoundException(fileName);
			}

			return opened;
		}

		public Task<FileStream> CreateOrOpenFileForWrite(string fileName)
		{
			var path = GetPathToFile(fileName);
			var opened = File.Open(path, FileMode.OpenOrCreate);
			return Task.FromResult(opened);
		}

		public Task DeleteFile(string fileName)
		{
			var path = GetPathToFile(fileName);
			File.Delete(path);
			return Task.FromResult(true);
		}
	}
}