﻿namespace LottoGO.App.Services.Contracts
{
	public interface IGoToLocationService
    {
	    void GoToLocation(string address, string latlong);
    }
}
