﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.Helpers;
using Refit.Insane.PowerPack.Data;

namespace LottoGO.App.Services.Contracts
{
    public interface IReferralService
    {
        Task<Response<Money>> UseReferralCode(string code, CancellationToken cancellationToken = default);
    }
}