﻿using System;
using System.Threading.Tasks;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.Services.Contracts
{
	public interface INavigationService
    {
        Task Close<TVievModel>(TVievModel viewModel) where TVievModel : IBaseViewModel;
        Task Close();
	    Task CloseToRoot();
        Task NavigateTo<TViewModel, TParameter>(TParameter parameter) where TViewModel : class, IBaseViewModel<TParameter> where TParameter : class;

        Task NavigateTo(Type viewModelType, object parameter); //where TViewModel : class, IBaseViewModel<TParameter> where TParameter : class;
        
        Task NavigateModalTo<TViewModel, TParameter>(TParameter parameter) where TViewModel : class, IBaseViewModel<TParameter> where TParameter : class;
    }
}