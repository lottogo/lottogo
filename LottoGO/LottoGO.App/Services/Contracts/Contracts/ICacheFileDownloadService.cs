﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.Models;

namespace LottoGO.App.Services.Contracts.Contracts
{
	public interface ICacheFileDownloadService
	{
		Task<FileStream> GetFileFromCacheOrDownload(IFileData fileData, Progress<float> progressListener,
			CancellationToken cancellationToken = default(CancellationToken));

		Task RemoveFileFromCache(IFileData fileData);
	}
}