﻿namespace LottoGO.App.Services.Contracts.Contracts
{
	public interface ILocalFilePathObtainerService
	{
		string ObtainLocalFilePath(string fileName);
	}
}