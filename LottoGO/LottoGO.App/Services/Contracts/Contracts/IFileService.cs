﻿using System.IO;
using System.Threading.Tasks;

namespace LottoGO.App.Services.Contracts.Contracts
{
	public interface IFileService
	{
		Task SaveFile(string fileName, Stream file);
		Task<FileStream> SaveFileAndKeepOpen(string fileName, Stream file);
		Task<FileStream> LoadFile(string fileName);
		Task<FileStream> CreateOrOpenFileForWrite(string fileName);
		Task DeleteFile(string fileName);
	}
}