﻿using System.Collections.Generic;
using LottoGO.App.Models;

namespace LottoGO.App.Services.Contracts
{
	public interface IMasterMenuDataService
	{
		List<IMasterMenuItem> GetMasterItemsForUser();
	}
}