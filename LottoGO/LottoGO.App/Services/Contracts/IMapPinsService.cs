﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LottoGO.App.ApiInterfaces;
using Refit.Insane.PowerPack.Data;
using Xamarin.Forms.Maps;

namespace LottoGO.App.Services.Contracts
{
    public interface IMapPinsService
    {
        Task<Response<List<LottoGoPin>>> GetMapPins();
    }
}