﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LottoGO.App.ViewModels;
using Refit.Insane.PowerPack.Data;

namespace LottoGO.App.Services.Contracts
{
    public interface IGameHistoryService
    {
        Task<Response<List<GameHistory>>> GetGameHistory();
    }
}