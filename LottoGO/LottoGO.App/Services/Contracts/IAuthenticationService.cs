﻿using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.ViewModels.Base;
using Refit.Insane.PowerPack.Data;

namespace LottoGO.App.Services.Contracts
{
    public interface IAuthenticationService
    {
        Task<Response<UserAuthResponse>> Login(string login, string password, CancellationToken cancellationToken = default);
        
        Task<Response<UserAuthResponse>> Register(string login, string password, string email, CancellationToken cancellationToken = default);
    }
    
    public class UserAuthResponse : Bindable
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public Money AccountBalance { get; set; }
        public string AccessToken { get; set; }
        public int AccessTokenExpirationSeconds { get; set; }
        
        public string ReferralCode { get; set; }
    }
    
    public class Money : Bindable
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public string Formatted => $"{Amount} {Currency}";
    }
}