﻿using System.Collections.Generic;
using LottoGO.App.Commands.Base;
using LottoGO.App.Commands.Master;
using LottoGO.App.Helpers;
using LottoGO.App.Messenger.Base;
using LottoGO.App.Models;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using Plugin.Geolocator;
using Xamarin.Forms;

namespace LottoGO.App.Services
{
	public class MasterMenuDataService : IMasterMenuDataService
	{
		private readonly INavigationService _navigationService;
		private readonly IMvxMessenger _messenger;

		public MasterMenuDataService(INavigationService navigationService, IMvxMessenger messenger)
		{
			_navigationService = navigationService;
			_messenger = messenger;
		}
		
		public List<IMasterMenuItem> GetMasterItemsForUser()
		{
			return new List<IMasterMenuItem>
			{
				new MasterMenuViewModelItem<MapViewModel, string>()
				{
					Title = "Mapa",
				},
				new MasterMenuViewModelItem<HistoryViewModel, string>()
				{
					Title = "Historia",
				},
				new MasterMenuViewModelItem<ReferFriendViewModel, string>()
				{
					Title = "Poleć znajomemu",
				},
				new MasterMenuViewModelItem<AcceptReferViewModel, string>()
				{
					Title = "Podaj kod bonusowy",
				},
				new MasterMenuCommandItem
				{
					Title = "Wyloguj",
					Command = new Command(async () =>
					{
						Settings.AuthData = null;
						await CrossGeolocator.Current.StopListeningAsync();
                        await _navigationService.NavigateTo<LoginViewModel, string>(string.Empty);
					})
				},
			};
		}
	}
}