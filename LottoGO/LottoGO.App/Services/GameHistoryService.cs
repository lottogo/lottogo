﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LottoGO.App.ApiInterfaces;
using LottoGO.App.Services.Contracts;
using LottoGO.App.ViewModels;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Responses;
using Refit.Insane.PowerPack.Data;
using Refit.Insane.PowerPack.Services;
using Xamarin.Forms.Maps;

namespace LottoGO.App.Services
{
    public class GameHistoryService : IGameHistoryService
    {
        private readonly IRestService _restService;

        public GameHistoryService(IRestService restService)
        {
            _restService = restService;
        }
        public async Task<Response<List<GameHistory>>> GetGameHistory()
        {
            try
            {
                var result = await _restService.Execute<ILottoGoApi, GenericResponse<List<GameHistoryDto>>>(api => api.GetGameHistory(default));
                if (result.IsSuccess && result.Results.IsSuccess)
                {
                    return new Response<List<GameHistory>>(result.Results.Response.Select(history =>
                    {
                        return new GameHistory
                        {
                            Prize = new Money
                            {
                                Amount = history.Prize.Amount,
                                Currency = history.Prize.Currency
                            },
                            IsWon = history.IsWon,
                            GameId = history.GameId,
                            DateTime = history.DateTime
                        };
                    }).ToList());
                }
                
                return new Response<List<GameHistory>>().AddErrorMessage("Error");
            }
            catch (Exception e)
            {
                return new Response<List<GameHistory>>().AddErrorMessage("Error");
            }
        }
    }
}