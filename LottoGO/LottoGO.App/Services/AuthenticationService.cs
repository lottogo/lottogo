﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.ApiInterfaces;
using LottoGO.App.Helpers;
using LottoGO.App.Services.Contracts;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using Newtonsoft.Json;
using Refit;
using Refit.Insane.PowerPack.Data;
using Refit.Insane.PowerPack.Services;

namespace LottoGO.App.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private const string TimeOutExceptionText = "Błąd połączenia z serwerem. Sprawdź czy masz połączenie z sięcią internet";
        private readonly IRestService _restService;

        public AuthenticationService(IRestService restService)
        {
            _restService = restService;
        }
        
        public async Task<Response<UserAuthResponse>> Login(string login, string password, CancellationToken cancellationToken = default)
        {
            try
            {
                var body = new LoginRequestDto
                {
                    Username = login,
                    Password = password
                };

                var result =
                    await _restService.Execute<ILottoGoApi, GenericResponse<UserAuthResponseDto>>(api =>
                        api.Login(body, cancellationToken));

                if (result.IsSuccess && result.Results.IsSuccess)
                {
                    Settings.AuthData = result.Results.Response;

                    return new Response<UserAuthResponse>(new UserAuthResponse
                    {
                        Username = result.Results.Response.Username,
                        Email = result.Results.Response.Email,
                        AccessToken = result.Results.Response.AccessToken,
                        AccessTokenExpirationSeconds = result.Results.Response.AccessTokenExpirationSeconds,
                        AccountBalance = new Money
                        {
                            Amount = result.Results.Response.AccountBalance.Amount,
                            Currency = result.Results.Response.AccountBalance.Currency,
                        },
                        ReferralCode = result.Results.Response.ReferralCode
                    });
                }

                var response = new Response<UserAuthResponse>();
                foreach (var errorDetail in result.Results.ErrorDetails)
                {
                    response.AddErrorMessage(errorDetail.ErrorDescription);
                }

                return response;
            }
            catch (ApiException apiEx)
            {
                var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(apiEx.Content);
                
                var response = new Response<UserAuthResponse>();
                foreach (var errorDetail in errorResponse.ErrorDetails)
                {
                    response.AddErrorMessage(errorDetail.ErrorDescription);
                }

                return response;
            }
            catch (TaskCanceledException)
            {
                var response = new Response<UserAuthResponse>();
                response.AddErrorMessage(TimeOutExceptionText);
                return response;
            }
            catch (Exception e)
            {
                var response = new Response<UserAuthResponse>();
                response.AddErrorMessage(e.Message);
                return response;
            }
        }

        public async Task<Response<UserAuthResponse>> Register(string login, string password, string email, CancellationToken cancellationToken = default)
        {
            try
            {
                var body = new RegisterRequestDto
                {
                    Username = login,
                    Password = password,
                    Email = email
                };

                var result =
                    await _restService.Execute<ILottoGoApi, GenericResponse<UserAuthResponseDto>>(api =>
                        api.Register(body, cancellationToken));

                if (result.IsSuccess && result.Results.IsSuccess)
                {
                    Settings.AuthData = result.Results.Response;

                    return new Response<UserAuthResponse>(new UserAuthResponse
                    {
                        Username = result.Results.Response.Username,
                        Email = result.Results.Response.Email,
                        AccessToken = result.Results.Response.AccessToken,
                        AccessTokenExpirationSeconds = result.Results.Response.AccessTokenExpirationSeconds,
                        AccountBalance = new Money
                        {
                            Amount = result.Results.Response.AccountBalance.Amount,
                            Currency = result.Results.Response.AccountBalance.Currency,
                        },
                        ReferralCode = result.Results.Response.ReferralCode
                    });
                }

                var response = new Response<UserAuthResponse>();
                foreach (var errorDetail in result.Results.ErrorDetails)
                {
                    response.AddErrorMessage(errorDetail.ErrorDescription);
                }

                return response;
            }
            catch (ApiException apiEx)
            {
                var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(apiEx.Content);

                var response = new Response<UserAuthResponse>();
                foreach (var errorDetail in errorResponse.ErrorDetails)
                {
                    response.AddErrorMessage(errorDetail.ErrorDescription);
                }

                return response;
            }
            catch (TaskCanceledException)
            {
                var response = new Response<UserAuthResponse>();
                response.AddErrorMessage(TimeOutExceptionText);
                return response;
            }
            catch (Exception e)
            {
                var response = new Response<UserAuthResponse>();
                response.AddErrorMessage(e.Message);
                return response;
            }
        }
    }
}