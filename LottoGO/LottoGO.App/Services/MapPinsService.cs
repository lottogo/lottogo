﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LottoGO.App.ApiInterfaces;
using LottoGO.App.Services.Contracts;
using LottoGO.Common.Models;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Responses;
using Refit.Insane.PowerPack.Data;
using Refit.Insane.PowerPack.Services;
using Xamarin.Forms.Maps;

namespace LottoGO.App.Services
{
    public class LottoGoPin : Pin
    {
        public bool VisitedByUser { get; set; }
        public decimal? Prize { get; set; }
        public GameType GameType { get; set; }
        public string MarkerId { get; set; }
        public Money SingleGameCost { get; set; }
    }
    
    public class MapPinsService : IMapPinsService
    {
        private readonly IRestService _restService;

        public MapPinsService(IRestService restService)
        {
            _restService = restService;
        }
        
        public async Task<Response<List<LottoGoPin>>> GetMapPins()
        {
            try
            {
                var result = await _restService.Execute<ILottoGoApi, GenericResponse<List<MapPinDto>>>(api => api.GetMapPoints(default));
                if (result.IsSuccess && result.Results.IsSuccess)
                {
                    return new Response<List<LottoGoPin>>(result.Results.Response.Select(pin =>
                    {
                        return new LottoGoPin
                        {
                            Id = pin.Id,
                            Type = PinType.Place,
                            Label = pin.Id.ToString(),
                            Position = new Position(pin.Latitude, pin.Longitude),
                            MarkerId = pin.MarkerId,
                            Prize = pin.Prize,
                            GameType = pin.GameType,
                            SingleGameCost = new Money
                            {
                                Amount = pin.SingleGameCost.Amount,
                                Currency = pin.SingleGameCost.Currency
                            },
                            VisitedByUser = pin.VisitedByUser
                        };
                    }).ToList());
                }
                
                return new Response<List<LottoGoPin>>().AddErrorMessage("Error");
            }
            catch (Exception e)
            {
                return new Response<List<LottoGoPin>>().AddErrorMessage("Error");
            }
        }
    }
}