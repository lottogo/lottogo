﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.ApiInterfaces;
using LottoGO.App.Services.Contracts;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using Newtonsoft.Json;
using Refit;
using Refit.Insane.PowerPack.Data;
using Refit.Insane.PowerPack.Services;

namespace LottoGO.App.Services
{
    public class ReferralService : IReferralService
    {
        private const string TimeOutExceptionText = "Błąd połączenia z serwerem. Sprawdź czy masz połączenie z sięcią internet";
        private readonly IRestService _restService;

        public ReferralService(IRestService restService)
        {
            _restService = restService;
        }
        
        public async Task<Response<Money>> UseReferralCode(string code, CancellationToken cancellationToken = default)
        {
            try
            {
                var request = new ReferralCodeRequestDto {ReferralCode = code};
                var result =
                    await _restService.Execute<ILottoGoApi, GenericResponse<MoneyDto>>(api =>
                        api.PostReferalCode(request, cancellationToken));

                if (result.IsSuccess && result.Results.IsSuccess)
                {
                    return new Response<Money>(new Money
                    {
                        Amount = result.Results.Response.Amount,
                        Currency = result.Results.Response.Currency,
                    });
                }

                var response = new Response<Money>();
                foreach (var errorDetail in result.Results.ErrorDetails)
                {
                    response.AddErrorMessage(errorDetail.ErrorDescription);
                }

                return response;
            }
            catch (ApiException apiEx)
            {
                var errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(apiEx.Content);

                var response = new Response<Money>();
                foreach (var errorDetail in errorResponse.ErrorDetails)
                {
                    response.AddErrorMessage(errorDetail.ErrorDescription);
                }

                return response;
            }
            catch (TaskCanceledException)
            {
                var response = new Response<Money>();
                response.AddErrorMessage(TimeOutExceptionText);
                return response;
            }
            catch (Exception e)
            {
                var response = new Response<Money>();
                response.AddErrorMessage(e.Message);
                return response;
            }
        }
    }
}