﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using LottoGO.App.Configuration.Api;
using LottoGO.Common.Models.DTOs;
using LottoGO.Common.Models.DTOs.Requests;
using LottoGO.Common.Models.DTOs.Responses;
using Refit;
using Refit.Insane.PowerPack.Retry;

namespace LottoGO.App.ApiInterfaces
{
	[AuthApiDefinition]
	[RefitRetry]
	public interface ILottoGoApi
    {
	    [Get("/api/user/getgamehistory")]
	    Task<GenericResponse<List<GameHistoryDto>>> GetGameHistory(CancellationToken cancellationToken);
	    
	    [Get("/api/map/getmappoints")]
	    Task<GenericResponse<List<MapPinDto>>> GetMapPoints(CancellationToken cancellationToken);
	    
	    [Post("/api/user/login")]
	    Task<GenericResponse<UserAuthResponseDto>> Login([Body(BodySerializationMethod.UrlEncoded)] LoginRequestDto body,
		    CancellationToken cancellationToken);
	    
	    [Post("/api/user/register")]
	    Task<GenericResponse<UserAuthResponseDto>> Register([Body(BodySerializationMethod.UrlEncoded)] RegisterRequestDto body,
		    CancellationToken cancellationToken);
	    
	    [Post("/api/user/usereferallcode")]
	    Task<GenericResponse<MoneyDto>> PostReferalCode([Body(BodySerializationMethod.UrlEncoded)] ReferralCodeRequestDto body,
		    CancellationToken cancellationToken);
	}
}
