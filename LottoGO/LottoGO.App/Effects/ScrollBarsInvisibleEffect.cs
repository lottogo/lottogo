﻿using Xamarin.Forms;

namespace LottoGO.App.Effects
{
	public class ScrollBarsInvisibleEffect : RoutingEffect
	{
		public ScrollBarsInvisibleEffect() : base("LottoGo.ScrollBarsInvisibleEffect")
		{
		}
	}
}