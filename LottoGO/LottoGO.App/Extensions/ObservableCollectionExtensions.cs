using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LottoGO.App.Extensions
{
	public static class ObservableCollectionExtensions
	{
		public static void AddRange<T>(this ObservableCollection<T> observableCollection, IEnumerable<T> items)
		{
			foreach (var item in items)
			{
				observableCollection.Add(item);
			}
		}

		public static void InsertRange<T>(this ObservableCollection<T> observableCollection, int index, IEnumerable<T> items)
		{
			var insertionIndex = index;
			foreach (var item in items)
			{
				observableCollection.Insert(insertionIndex, item);
				insertionIndex++;
			}
		}
	}
}