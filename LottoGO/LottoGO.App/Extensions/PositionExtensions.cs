﻿using System;
using Xamarin.Forms.Maps;

namespace LottoGO.App.Extensions
{
    public static class PositionExtensions
    {
        private static double Deg2Rad(double deg) => deg * (Math.PI / 180.0);
		
        public static double DistanceTo(this Position position, Position toPosition)
        {
            var dLat = Deg2Rad(toPosition.Latitude - position.Latitude);
            var dLon = Deg2Rad(toPosition.Longitude - position.Longitude);
            var a =
                Math.Pow(Math.Sin(dLat / 2.0), 2) +
                Math.Cos(Deg2Rad(position.Latitude)) * Math.Cos(Deg2Rad(toPosition.Latitude)) *
                Math.Pow(Math.Sin(dLon / 2.0), 2);
            var b = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1.0 - a));
            return 6371000.0 * b;
        }
    }
}