﻿using System;
using Autofac;
using LottoGO.App.Configuration;
using LottoGO.App.ViewModels;
using Plugin.Geolocator;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace LottoGO.App
{
    public partial class App : BaseFormsApp
    {
        public App(IContainer container) : base(container)
        {
		InitializeComponent();
	        ResolveAppStart<LoginViewModel, string>();
            System.Net.ServicePointManager.ServerCertificateValidationCallback = (message, cert, chain, errors) => 
            { 
                return true; 
            };
        }

        public static bool GeolocationWasStarted;

		protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override async void OnSleep()
        {
            await CrossGeolocator.Current.StopListeningAsync();
        }

        protected override async void OnResume()
        {
            if (GeolocationWasStarted)
                await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 5);
        }
    }
}
