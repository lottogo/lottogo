﻿namespace LottoGO.App.Models
{
	public enum Orientation
	{
		Portrait,
		Landscape,
		All
	}
}