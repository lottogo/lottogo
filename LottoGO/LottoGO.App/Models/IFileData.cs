﻿using System;

namespace LottoGO.App.Models
{
	public interface IFileData
	{
		Guid Id { get;  }
		DateTimeOffset CreatedAt { get;  }
		DateTimeOffset LastModifiedAt { get; }
		string FileUrl { get;  }
	}
}