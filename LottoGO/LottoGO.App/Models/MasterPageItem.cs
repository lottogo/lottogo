﻿using System;
using System.Windows.Input;
using LottoGO.App.ViewModels.Base;

namespace LottoGO.App.Models
{
    public interface IMasterMenuItem
    {
        string Title { get; }

        string IconSource { get; }
    }
    
    public interface IMasterMenuItem<TViewModel, TParameter> : IMasterMenuItem, IViewModelItem where TViewModel : IBaseViewModel<TParameter>
    {
        TViewModel ViewModel { get; set; }
        
        TParameter Parameter { get; set; }
    }

	public interface IViewModelItem
	{
		Type ViewModelType { get; }
        
		Type ParameterType { get; }
	}

	public class MasterMenuViewModelItem<TViewModel, TParameter>  : IMasterMenuItem<TViewModel, TParameter> where TViewModel : IBaseViewModel<TParameter>
    {
        public string Title { get; set; }

        public string IconSource { get; set; }

        public Type ViewModelType => typeof(TViewModel);
        
        public Type ParameterType => typeof(TParameter);

        public TViewModel ViewModel { get; set; }
        
        public TParameter Parameter { get; set; }
    }

	public class MasterMenuCommandItem : IMasterMenuItem
	{
		public string Title { get; set; }
		
		public string IconSource { get; set; }
		
		public ICommand Command { get; set; }
	}
}
