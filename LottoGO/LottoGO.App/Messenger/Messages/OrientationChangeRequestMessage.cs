﻿using LottoGO.App.Messenger.Base;
using LottoGO.App.Models;

namespace LottoGO.App.Messenger.Messages
{
	public class OrientationChangeRequestMessage : MvxMessage
	{
		public OrientationChangeRequestMessage(object sender, Orientation orientation) : base(sender)
		{
			RequestedOrientation = orientation;
		}

		public Orientation RequestedOrientation { get; }
	}
}