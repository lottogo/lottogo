﻿using LottoGO.App.Messenger.Base;
using Xamarin.Forms.Maps;

namespace LottoGO.App.Messenger.Messages
{
    public class CurrentLocationChangedMessage : MvxMessage
    {
        public CurrentLocationChangedMessage(object sender, Position position) : base(sender)
        {
            Position = position;
        }
        
        public Position Position { get; }
    }
}