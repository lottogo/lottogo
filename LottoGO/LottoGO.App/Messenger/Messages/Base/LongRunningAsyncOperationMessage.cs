﻿using LottoGO.App.Messenger.Base;

namespace LottoGO.App.Messenger.Messages.Base
{
	public class LongRunningAsyncOperationMessage : MvxMessage
	{
		public LongRunningAsyncOperationMessage(object sender) : base(sender)
		{
		}

		public bool HasStarted { get; private set; }

		public bool HasFinished { get; private set; }

		public static LongRunningAsyncOperationMessage BuildOperationStartedMessage(object sender)
		{
			return new LongRunningAsyncOperationMessage(sender)
			{
				HasStarted = true
			};
		}

		public static LongRunningAsyncOperationMessage BuildOperationFinishedMessage(object sender)
		{
			return new LongRunningAsyncOperationMessage(sender)
			{
				HasFinished = true
			};
		}
	}
}