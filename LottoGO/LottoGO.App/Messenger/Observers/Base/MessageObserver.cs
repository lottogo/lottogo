﻿using LottoGO.App.Messenger.Base;

namespace LottoGO.App.Messenger.Observers.Base
{
	public abstract class MessageObserver<TMessage> : IMessageObserver where TMessage : MvxMessage
	{
		private readonly bool _shouldSubscribeOnThreadPool;
		private MvxSubscriptionToken _subscriptionToken;

		protected MessageObserver(bool shouldSubscribeOnThreadPool = false)
		{
			_shouldSubscribeOnThreadPool = shouldSubscribeOnThreadPool;
		}


		public void Start(IMvxMessenger messenger)
		{
			IsObserving = true;
			_subscriptionToken = _shouldSubscribeOnThreadPool
				? messenger.SubscribeOnThreadPoolThread<TMessage>(HandleMessage)
				: messenger.SubscribeOnMainThread<TMessage>(HandleMessage);
		}

		public void Stop()
		{
			IsObserving = false;
			_subscriptionToken?.Dispose();
		}

		public virtual bool ShouldStopOnDisappearing()
		{
			return true;
		}

		public bool IsObserving { get; private set; }

		public virtual void Dispose()
		{
			Stop();
		}

		private void HandleMessage(TMessage messageToHandle)
		{
			if (ShouldHandleMessage(messageToHandle))
				OnMessageArrived(messageToHandle);
		}

		protected abstract void OnMessageArrived(TMessage messageToHandle);

		protected virtual bool ShouldHandleMessage(TMessage message)
		{
			return true;
		}
	}
}