﻿using System;
using LottoGO.App.Messenger.Base;

namespace LottoGO.App.Messenger.Observers.Base
{
	public interface IMessageObserver : IDisposable
	{
		bool IsObserving { get; }
		void Start(IMvxMessenger messenger);
		void Stop();
		bool ShouldStopOnDisappearing();
	}
}