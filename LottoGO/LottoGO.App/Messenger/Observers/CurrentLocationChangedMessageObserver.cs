﻿using LottoGO.App.Messenger.Messages;
using LottoGO.App.Messenger.Observers.Base;
using LottoGO.App.ViewModels;

namespace LottoGO.App.Messenger.Observers
{
    public class CurrentLocationChangedMessageObserver : MessageObserver<CurrentLocationChangedMessage>
    {
        private readonly MapViewModel _mapViewModel;

        public CurrentLocationChangedMessageObserver(MapViewModel mapViewModel)
        {
            _mapViewModel = mapViewModel;
        }

        protected override void OnMessageArrived(CurrentLocationChangedMessage messageToHandle)
        {
            _mapViewModel.CurrentPosition = messageToHandle.Position;
            _mapViewModel.SetClosestLocationCommand.Execute(null);
        }
    }
}